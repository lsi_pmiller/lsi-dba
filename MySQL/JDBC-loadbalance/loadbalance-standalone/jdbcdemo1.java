/*
 * compile with
 *   javac -classpath mysql-connector-java-5.1.32-bin.jar jdbcdemo1.java
 * run with
 *  java -classpath mysql-connector-java-5.1.32-bin.jar:.  jdbcdemo1 
 *
 */

import com.mysql.jdbc.Driver;
 
import java.sql.ResultSet;
import java.util.Properties;
import java.sql.Connection;
//import java.sql.Statement;
import java.sql.SQLException;
//import java.sql.DriverManager;
 
public class jdbcdemo1 {
  public static void main(String[] args) throws Exception {
    Driver driver = new Driver();
    Properties props = new Properties();
    ResultSet rs;
    String variable_value;
    Connection conn = null;
    String JDBC_URL = "jdbc:mysql:replication://" +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=slave)" +
        "/test";
 
    props.put("replicationConnectionGroup", "lray");
    props.put("replicationEnableJMX", "true");
 
    props.put("user", "test_user");
    props.put("password", "test_pass");
    props.put("loadBalanceStrategy", "com.mysql.jdbc.PrimaryBalanceStrategy");
    props.put("loadBalanceBlacklistTimeout", "1000");

 
    System.out.println("\n------------ MySQL Connector/J and MySQL Replication Testing ------------\n");
 
    try {
    	Class.forName("com.mysql.jdbc.ReplicationDriver");
    }
    catch (ClassNotFoundException e) {
    	System.out.println("Where is your MySQL JDBC Driver?");
        e.printStackTrace();
        return;
    }
 
    System.out.println("MySQL Connector/J driver loaded");
 
    System.out.println("Trying connection...");
 
    try {
		conn = driver.connect(JDBC_URL, props);
	}
	catch (SQLException e) {
		System.out.println("Connection Failed! Check output console");
		//e.printStackTrace();
		System.out.println("Error cause: "+e.getCause());
		System.out.println("Error message: "+e.getMessage());
		return;
	}
 
    System.out.println("Connection established...");
 
    int loopCnt = 50;
    for(int i=1; i <= loopCnt; i++) {
    	System.out.println("\nQuery " + i + "/" + loopCnt + ":");
    	// Fake Read Write SQL statement (just to see where it goes)
    	System.out.println("Read Write query...");
    	try {
    		conn.setReadOnly(false);
    	}
    	catch (SQLException e) {
    		System.out.println("Connection read write property set has failed...");
    	}
 
    	try {
    		//rs = conn.createStatement().executeQuery("SELECT variable_value FROM information_schema.global_variables where variable_name='port'");
    		rs = conn.createStatement().executeQuery("SELECT GROUP_CONCAT(variable_value ORDER BY variable_name SEPARATOR ':') as variable_value FROM information_schema.global_variables where variable_name in ('hostname', 'port')");
    		while (rs.next()) {
    			variable_value = rs.getString("variable_value");
    			System.out.println("variable_value : " + variable_value);
    		}
	    }
    	catch (SQLException e) {
    		System.out.println("Read/write query has failed...");
    	}
 
    	// Read Only statement
    	System.out.println("Read Only query...");
    	try {
    		conn.setReadOnly(true);	
    	}
    	catch (SQLException e) {
    		System.out.println("Connection read only property set has has failed...");
    	}
 
    	try {
    		// rs = conn.createStatement().executeQuery("SELECT variable_value FROM information_schema.global_variables where variable_name='port'");
    		rs = conn.createStatement().executeQuery("SELECT GROUP_CONCAT(variable_value ORDER BY variable_name SEPARATOR ':') as variable_value FROM information_schema.global_variables where variable_name in ('hostname', 'port')");

    		while (rs.next()) {
    			variable_value = rs.getString("variable_value");
    			System.out.println("variable_value : " + variable_value);
    		}
    		}
    	catch (SQLException e) {
    		System.out.println("Read only query has failed...");
    	}
 
    	Thread.sleep(2000);
    }
  }
}
