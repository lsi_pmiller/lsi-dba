/*
  Copyright (c) 2007, 2014, Oracle and/or its affiliates. All rights reserved.

  The MySQL Connector/J is licensed under the terms of the GPLv2
  <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>, like most MySQL Connectors.
  There are special exceptions to the terms and conditions of the GPLv2 as it is applied to
  this software, see the FLOSS License Exception
  <http://www.mysql.com/about/legal/licensing/foss-exception.html>.

  This program is free software; you can redistribute it and/or modify it under the terms
  of the GNU General Public License as published by the Free Software Foundation; version 2
  of the License.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth
  Floor, Boston, MA 02110-1301  USA

 */

package com.mysql.jdbc;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * A balancing strategy that starts at the first host and will stay on that host 
 * until a connection error occurs.
 * When a connection error occurs it advances to the next host in the list (wrapping around)
 * and will stay on that host until a connection error occurs.
 * 
 * The initial point selection, and subsequent point selections are
 * blacklist-aware.
 *
 * If you want blacklist aware selections (and you do!) you must have
 * loadBalanceBlacklistTimeout set to something other then the default of 0 (zero)
 * http://dev.mysql.com/doc/connector-j/en/connector-j-reference-configuration-properties.html
 *
 * This class was derived from SequentialBalanceStrategy
 */
public class PrimaryBalanceStrategy implements BalanceStrategy {
	private int currentHostIndex = -1;
	
	public PrimaryBalanceStrategy() {
	}

	public void destroy() {
		// we don't have anything to clean up
	}

	public void init(Connection conn, Properties props) throws SQLException {
		// we don't have anything to initialize
	}

	public ConnectionImpl pickConnection(LoadBalancingConnectionProxy proxy,
			List<String> configuredHosts, Map<String, ConnectionImpl> liveConnections, long[] responseTimes,
			int numRetries) throws SQLException {
		int numHosts = configuredHosts.size();

		SQLException ex = null;

		Map<String, Long> blackList = proxy.getGlobalBlacklist();


        //System.out.println("PrimaryBalanceStrategy - numHosts : " + numHosts);
		for (int attempts = 0; attempts < numRetries; attempts++) {

            //System.out.println("PrimaryBalanceStrategy - attempts/numRetries : " + attempts + "/" + numRetries);
            //System.out.println("PrimaryBalanceStrategy - currentHostIndex : " + currentHostIndex);
            //System.out.println("PrimaryBalanceStrategy - blackList.size() : " + blackList.size());
            if (numHosts == 1) {
				currentHostIndex = 0; // pathological case

			} else  if (currentHostIndex == -1) {
				for (int i = 0; i < numHosts; i++) {
					if (!blackList.containsKey(configuredHosts.get(i))) {
						currentHostIndex = i; 
						break;
					}
				}
				
				if (currentHostIndex == -1) {
					blackList = proxy.getGlobalBlacklist(); // it may have changed
								// and the proxy returns a copy
					
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}

					continue; // retry
				}
			} else {

				
				int i = currentHostIndex;
				boolean foundGoodHost = false;
				
				for (; i < numHosts; i++) {
					if (!blackList.containsKey(configuredHosts.get(i))) {
						currentHostIndex = i;
						foundGoodHost = true;
						break;
					}
				}

				if (!foundGoodHost) {
					for (i = 0; i < currentHostIndex; i++) {
						if (!blackList.containsKey(configuredHosts.get(i))) {
							currentHostIndex = i;
							foundGoodHost = true;
							break;
						}
					}
				}
			
				if (!foundGoodHost) {
					blackList = proxy.getGlobalBlacklist(); // it may have changed
					// and the proxy returns a copy
		
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
					}
			
					continue; // retry
				}
			}

			String hostPortSpec = configuredHosts.get(currentHostIndex);
            //System.out.println("PrimaryBalanceStrategy - 1 liveConnections.get(hostPortSpec) : " + hostPortSpec);
			ConnectionImpl conn = liveConnections.get(hostPortSpec);
            //System.out.println("PrimaryBalanceStrategy - 2 liveConnections.get(hostPortSpec) : " + hostPortSpec);

			if (conn == null) {	
                //System.out.println("PrimaryBalanceStrategy - conn == null");
				try {
                    //System.out.println("PrimaryBalanceStrategy - proxy.createConnectionForHost start");
					conn = proxy.createConnectionForHost(hostPortSpec);
                    //System.out.println("PrimaryBalanceStrategy - proxy.createConnectionForHost finish");
                } catch (SQLException sqlEx) {
					ex = sqlEx;

                    System.out.println("PrimaryBalanceStrategy - SQLException : " + sqlEx);


					if (sqlEx instanceof CommunicationsException
                        || "08S01".equals(sqlEx.getSQLState())) {
                        
                        //System.out.println("PrimaryBalanceStrategy -  addToGlobalBlacklist : " + hostPortSpec);
                        //System.out.println("PrimaryBalanceStrategy - 1 blackList.size() : " + blackList.size());
						proxy.addToGlobalBlacklist( hostPortSpec );
                        blackList = proxy.getGlobalBlacklist(); 
                        //System.out.println("PrimaryBalanceStrategy - 2 blackList.size() : " + blackList.size());
                        // the call getGlobalBlacklist is not in SequentialBalanceStrategy
                        // needed here in case the first host on the list is down.

						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
						}
						
						continue;
                    }
                    throw sqlEx;
				}
			}
            //System.out.println("PrimaryBalanceStrategy - conn != null ...  return conn");			
			return conn;
		}

		if (ex != null) {
			throw ex;
		}

		return null; // we won't get here, compiler can't tell
	}

}
