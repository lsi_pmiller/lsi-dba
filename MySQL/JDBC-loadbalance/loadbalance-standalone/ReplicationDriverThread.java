/*
 * code taken from 
 * http://dev.mysql.com/doc/connector-j/en/connector-j-master-slave-replication-connection.html
 *
 * compile with
 *   javac -classpath mysql-connector-java-5.1.32-bin.jar ReplicationDriverThread.java
 * run with
 *  java -classpath mysql-connector-java-5.1.32-bin.jar:.  ReplicationDriverThread 
 *
 */

import java.sql.*;
import java.util.*;

import com.mysql.jdbc.ReplicationDriver;

public class ReplicationDriverThread {
    private static int numberOfThreads = 4;


    // Looks like a normal MySQL JDBC url, with a
    // comma-separated list of hosts, the first
    // being the 'master', the rest being any number
    // of slaves that the driver will load balance against
    //


/*
    // use master/slave connection for both read and write
    private static String read_URL = "jdbc:mysql:replication://" +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=slave)," + 
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=slave)/test";


    // write_URL can be master or master+slave
    // can not be only slave
    private static String write_URL = "jdbc:mysql:replication://" +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=master)," +
        "address=(protocol=tcp)(host=localhost)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=localhost)(port=3307)(type=slave)/test";
*/

    // use master/slave connection for both read and write
    private static String read_URL = "jdbc:mysql:replication://" +
        "address=(protocol=tcp)(host=test-ha-db1)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=test-ha-db2)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=test-ha-db3)(port=3306)(type=master)," + 
        "address=(protocol=tcp)(host=test-ha-db1)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=test-ha-db2)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=test-ha-db3)(port=3306)(type=slave)/test";


    // write_URL can be master or master+slave
    // can not be only slave
    private static String write_URL = "jdbc:mysql:replication://" +
        "address=(protocol=tcp)(host=test-ha-db1)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=test-ha-db2)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=test-ha-db3)(port=3306)(type=master)," +
        "address=(protocol=tcp)(host=test-ha-db1)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=test-ha-db2)(port=3306)(type=slave)," +
        "address=(protocol=tcp)(host=test-ha-db3)(port=3306)(type=slave)/test";

    /*
    // use the muti host version for the read connection
    private static String read_URL = "jdbc:mysql://" +
        "test-ha-db1,test-ha-db2,test-ha-db3/test?" +
        "failoverReadOnly=false&" +
        "allowMasterDownConnections=true";
    */

    /*
    // this does not do failover, do not use
    private static String write_URL = "jdbc:mysql://" +
        "test-ha-db1,test-ha-db2,test-ha-db3/test?" +
        "failoverReadOnly=false&" +
        "allowMasterDownConnections=true";
    */


    private static Properties read_props = new Properties();
    private static Properties write_props = new Properties();
    static {
        read_props.put("user", "test_user");
        read_props.put("password", "test_pass");

        write_props.put("user", "test_user");
        write_props.put("password", "test_pass");

        // 10000ms == 10s timeout
        read_props.put("connectTimeout",  "10000");
        write_props.put("connectTimeout", "10000");

        // 10000ms == 10s timeout
        read_props.put("socketTimeout",  "10000");
        write_props.put("socketTimeout", "10000");

        // must be a non-zero value
        // 300000ms == 300s == 5m
        read_props.put("loadBalanceBlacklistTimeout",  "300000");
        write_props.put("loadBalanceBlacklistTimeout", "300000");

        write_props.put("loadBalanceStrategy", 
                        "com.mysql.jdbc.PrimaryBalanceStrategy");


        // read connection can be any of these 4
        // my limited test shows no differences besides what is noted
        // Also one can have none of the below read_props set

        /*
         * autoReconnect is slowest when one host is down 
         * 4 threads & 20 loop
         * both host up  : 0m11.610s
         * host one down : 1m41.583s
         * host two down : 1m41.606s
         *
         * note that with "autoReconnect", "false"
         * read connection will occur to db started while this program is running 
         * not sure what autoReconnect adds?
         */
        // We want this for failover on the slaves
        // read_props.put("autoReconnect", "true");  // not sure is needed
        // We want to load balance between the slaves
        //read_props.put("roundRobinLoadBalance", "true");
        
        /*        
        // 2)
        read_props.put("loadBalanceStrategy", 
                       "com.mysql.jdbc.BestResponseTimeBalanceStrategy");
        // 3)
        read_props.put("loadBalanceStrategy", 
                       "com.mysql.jdbc.RandomBalanceStrategy");
        // 4)
        read_props.put("loadBalanceStrategy", 
                       "com.mysql.jdbc.SequentialBalanceStrategy");
        */
    }
        

    public static void main(String[] args) throws Exception {
        ReplicationDriverThread lb = new ReplicationDriverThread();
        lb.doit();
    }

    public void doit() {
        Vector <Repeater> threadList = new Vector<Repeater>();
        for (int i=0; i<numberOfThreads; i++) {
            Repeater thread = new Repeater(Integer.toString(i));
            thread.start();
            threadList.add(thread);
        }

        // give the threads a chance to get ready
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // and off we go!
        for (int i=0; i<threadList.size(); i++) {
            Repeater thread = (Repeater)threadList.elementAt(i);
            thread.wake();
        }

        waitForThreadsToFinish(threadList);
    }

    static Connection getNewConnectionRead() throws SQLException, ClassNotFoundException {
        ReplicationDriver driver = new ReplicationDriver();
        Connection conn = driver.connect(read_URL, read_props);
        conn.setReadOnly(true);
        conn.setAutoCommit(false);
        return conn;
    }

    static Connection getNewConnectionWrite() throws SQLException, ClassNotFoundException {
        ReplicationDriver driver = new ReplicationDriver();
        Connection conn = driver.connect(write_URL, write_props);
        conn.setReadOnly(false);
        conn.setAutoCommit(false);
        return conn;
    }


    static String executeSimpleWrite(Connection conn) {
        String hostPort = "unknown";

        try {
            //
            // Perform read/write work on the master
            // by setting the read-only flag to "false"
            //
            ResultSet rs = conn.createStatement().executeQuery("SELECT @@HOSTNAME, @@PORT");
            while (rs.next()) {
                hostPort = rs.getString(1) + ":" + rs.getInt(2);
            }

            String updateStatement = "insert into pmiller (ts, hostport) values (now(), ?)";
            PreparedStatement update = conn.prepareStatement(updateStatement);
            update.setString(1, hostPort);
            update.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            System.out.println("  got SQLException in executeSimpleWrite");
            e.printStackTrace();

        } catch (Exception e) {
            System.out.println("  got Exception in executeSimpleWrite");
            e.printStackTrace();
        }

        return hostPort;
    }

    static String executeSimpleRead(Connection conn, int cnt){
        String hostPort = "unknown";
        try {
            //
            // do a query from a slave, the driver automatically picks one
            // from the list
            //
            ResultSet rs = conn.createStatement().executeQuery("SELECT @@HOSTNAME, @@PORT /* Connection cnt: " + cnt + " */");
            while (rs.next()) {
                hostPort = rs.getString(1) + ":" + rs.getInt(2);
            }
        } catch (SQLException e) {
            System.out.println("got SQLException in executeSimpleRead");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("got Exception in executeSimpleRead");
            e.printStackTrace();
        }
        return hostPort;
    }

    private void waitForThreadsToFinish(Vector list) {
        System.out.println("Running");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	
        int i = 0;
        while (i < list.size()) {
            Repeater thread = (Repeater)list.elementAt(i);
            if (thread.finished()) {
                i++;
            } else {
                // give the thread a chance to finish
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        for (i=0; i<list.size(); i++) {
            Repeater thread = (Repeater)list.elementAt(i);
            thread.printResults();
            System.out.println();
        }

    }



    public static class Repeater extends Thread {
        private boolean finished = false;
        private TreeMap<String, Integer> readCounts
            = new TreeMap<String, Integer>();
        private TreeMap<String, Integer> writeCounts
            = new TreeMap<String, Integer>();

        public Repeater(String id) {
            super(id);
        }

        synchronized void wake() {
            this.notify();
        }

        public boolean finished() {
            return finished;
        }

        public void printResults() {
            System.out.println("thread: " + this.getName());
            for(String hostPort: readCounts.keySet()){
                System.out.println("    read:" +  hostPort + 
                                   " count:" + readCounts.get(hostPort));
            }
            for(String hostPort: writeCounts.keySet()){
                System.out.println("    write:" +  hostPort + 
                                   " count:" + writeCounts.get(hostPort));
            }
        }
            

        synchronized public void run() {
            try {
                // wait for the notify
                wait();
            } catch (InterruptedException e) {
                System.out.println("Got an InterruptedException");
                e.printStackTrace();
            }

            String readHostPort;
            String writeHostPort;
            Integer readCnt;
            Integer writeCnt;
            Connection c;
            int loop = 20;
            for(int i=1; i < loop; i++) {
                System.out.println("thread: " + this.getName() + " loop: " + i + "/" + loop);

                // writes
                c = null;
                try {
                    c = getNewConnectionWrite();
                    for(int j=0; j < 10; j++){
                        writeHostPort = executeSimpleWrite(c);
                        writeCnt = writeCounts.get(writeHostPort);
                        if (writeCnt == null) {
                            writeCnt = 1;
                        } else {
                            writeCnt++;
                        }
                        writeCounts.put(writeHostPort, writeCnt);
                    }
                } catch (SQLException e) {
                    System.out.println("  Got an write SQLException in run");
                    //e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    System.out.println("  Got a write ClassNotFoundException in run");
                    //e.printStackTrace();
                } finally { 
                    try {
                        if (c != null) c.close();
                    } catch (Exception e) {
                    } 
                }

                // reads
                try {
                    c = getNewConnectionRead();
                    for(int j=0; j < 10; j++){
                        readHostPort = executeSimpleRead(c, j);
                        readCnt = readCounts.get(readHostPort);
                        if (readCnt == null) {
                            readCnt = 1;
                        } else {
                            readCnt++;
                        }
                        readCounts.put(readHostPort, readCnt);
                    }
                    c.close();
                } catch (SQLException e) {
                    System.out.println("  Got an read SQLException in run");
                    //e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    System.out.println("  Got a read ClassNotFoundException in run");
                    //e.printStackTrace();
                }
            }
            finished = true;
        }
    }
}
