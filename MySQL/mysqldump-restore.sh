#!/bin/sh

FILE=$1
if [ "$FILE" = "" ]
then
    echo "must give mysql dump file"
    echo "$0 file databasename"
    exit
fi

DATABASE=$2
if [ "$DATABASE" = "" ]
then
    echo "must give a database name"
    echo "$0 file databasename"
    exit
fi


START_TICKS=`date +%s`
zcat $1 |  mysql $DATABASE
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`
echo "finished in : ${TOTAL_TIME}s"
