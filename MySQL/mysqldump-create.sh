#!/bin/sh

DATABASE=$1
if [ "$DATABASE" = "" ]
then
    echo "must give a database name"
    echo "$0 databasename"
    exit
fi
START_TICKS=`date +%s`
DATE=`date '+%F_%T'`
mysqldump $DATABASE | gzip > /var/tmp/mysqldump-${DATABASE}-${DATE}.sql.gz
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`
echo "finished in : ${TOTAL_TIME}s"

