#!/usr/bin/perl

use strict;
use warnings FATAL => 'all';

use Time::Local;
use Time::HiRes qw( gettimeofday tv_interval );
use File::Copy;
use POSIX qw(strftime);
use Sys::Hostname;
use Data::Dumper;

$Data::Dumper::Sortkeys = 1;

$| = 1;

my $dry_run = 0;

# Unsafe but good enough to prevent running as root
my $current_user = $ENV{'USER'};
die "You should run this only as user 'mysql'..." if $current_user ne 'mysql';

my %config = (

	'backupdb' => {
		'alias' => 'db1',
		'databases' => {
			'mysql' => {},
			'prod' => {
				'expire_days' => days_to_seconds(7),
				'archive' => 1,
			},
#			'gl_invoicing' => {},
		},
	},
	'drrtdb' => {
		'alias' => 'rtdb',
		'databases' => {
			'rt3' => {},
		},
	},
	'drbcdb1' => {
		'alias' => 'bcdb1',
		'databases' => {
			'prod' => {}, 
		},
	},
    'drlpiddb' => {
         'alias' => 'lpiddb',
         'databases' => {
             'prod_lpid' => {},
         },
    },
	# This is remote MySQL 5.0.51 host on Windows OS (London)
	# Connect via remote desktop using your LSI account for administration
	'ldcgoiappdb01.goioperations.com' => {
		'alias' => 'ldcgoiappdb01',
		'host' => 'ldcgoiappdb01.goioperations.com',
		'user' => 'backupdb_user',
		'pass' => '0ranges!!',
        # adding max_allowed_packet=67108864 did not correct
	    # mysqldump: Error 2013: Lost connection to MySQL server during query
		'options' => '--single-transaction --max_allowed_packet=67108864',
		'dump_slave' => 0,
		'databases' => {
			'appruser' => {
				'expire_days' => days_to_seconds(14),
            },
			'mysql' => {},
		},
	},	
);

my $hostname = shift || Sys::Hostname::hostname();

# Check for valid backup hosname
if (!grep { /^${hostname}$/i } keys %config) {

	printf "Invalid backup hostname: %s!\n", $hostname;
	printf "Valid hostnames: [%s]\n", join(', ', keys %config);

	exit;
}

my $mysqldump = '/usr/bin/mysqldump';

# slave stop/start now handled by mysqldump --dump-slave
#my $mysqladmin  = '/usr/bin/mysqladmin';
#my $slave_stop  = sprintf('%s -u root stop-slave',  $mysqladmin);
#my $slave_start = sprintf('%s -u root start-slave', $mysqladmin);

my $backup_host  = $config{$hostname}{alias} || $hostname;
my $backup_dir   = sprintf('/backups/database/dumps/%s', $backup_host);
my $snapshot_dir = sprintf('/backups/database/archive/%s', $backup_host);

my $backup_time = strftime("%Y-%m-%d", localtime);
my $backup_file = "%s_${backup_time}.sql.gz";
my $backup_log  = "%s_${backup_time}.log";

my $dow  = (localtime)[6];
my $mday = (localtime)[3];

my $now = time;

## Stop slave
#my_system($slave_stop, "Stopping slave...");

my $host_config = $config{$hostname};

# Backup DB's
my $t0 = [gettimeofday];
foreach my $db ( keys %{$host_config->{databases}} ) {

	printf "Dumping database [%s] at [%s]\n", $db, scalar localtime();
	mysql_dump($db);

	my $archive_flag = $host_config->{databases}{$db}{archive} || 0;

	if ($archive_flag) {
		printf "Archiving database %s...\n", $db;
		archive_database($db);
	}

	printf "Cleaning up old dumps of database %s...\n", $db;
	delete_old_dumps($db);
}

# Start slave
#my_system($slave_start, "Starting slave...");

printf "\nDone at [%s] in [%ss]\n\n", scalar localtime(), (tv_interval ( $t0 ));

# Dump database
sub mysql_dump {

	my $db = shift;

	my $file = sprintf($backup_file, $db);
	my $file_log = sprintf($backup_log, $db);

	my $host = exists $host_config->{host} ? "-h $host_config->{host}"  : '';
	my $user = exists $host_config->{user} ? "-u $host_config->{user}"  : '-u root';
	my $pass = exists $host_config->{pass} ? "-p'$host_config->{pass}'" : '';

	my $options = exists $host_config->{options} ? $host_config->{options} : '';

	my $dump_slave = exists $host_config->{dump_slave} && !$host_config->{dump_slave} ? '' : '--dump-slave=2'; # Dump slave info by default

	my $cmd = "$mysqldump $options $host $user $pass --verbose $dump_slave $db 2> $backup_dir/$file_log | /bin/gzip > $backup_dir/$file";

	my $t0 = [gettimeofday];
	my_system($cmd);
	print "elapsed time [" . tv_interval ( $t0 ) . "s]\n";
}

# Copy a snapshot
sub archive_database {

	my $db = shift;

	#if ($dow == 6) {
	if ($mday == 1) {
		my $file = sprintf($backup_file, $db);
		printf("Archiving dump file '%s'...\n", $file);
		mkdir($snapshot_dir) if !-d $snapshot_dir;
		copy("$backup_dir/$file", "$snapshot_dir/$file") unless $dry_run;
	}
}

# Delete old backup files
sub delete_old_dumps {

	my $db = shift;

	my $expire_threshold = $config{$hostname}{databases}{$db}{expire_days} || days_to_seconds(7);

	printf "Threshold: %d (Now: $now)\n", $expire_threshold;

	my @delete_files;

	opendir(DBDIR, $backup_dir ) or die "Cannot open dir '$backup_dir': $!";
	while ( defined( my $name = readdir DBDIR ) ) {

		next if $name =~ /^\.\.?$/;

		if ( ($name =~ /^${db}[_-](\d{4})-(\d{2})-(\d{2}).sql.gz$/) ||
             ($name =~ /^${db}[_-](\d{4})-(\d{2})-(\d{2}).log/) ) {
			my $file_time = timelocal(0,0,0,$3,$2-1,$1);

			if ($file_time && $file_time < ($now - $expire_threshold) ) {
				push (@delete_files, "$backup_dir/$name");
			}
		}
	}
	closedir(DBDIR);

	if (scalar @delete_files) {

		@delete_files = sort @delete_files;

		print "Unlinking " . scalar @delete_files . " file(s)...\n";
		print "Unlinking: \n" . join("\n", @delete_files), "\n";

		unlink(@delete_files) unless $dry_run;

	} else {
		print "  There were no old DB backup files found.\n";
	}
}

sub my_system {

	my $cmd = shift;
	my $msg = shift;

	print "$msg\n" if defined $msg;

	print "CMD: $cmd\n";

	system($cmd) unless $dry_run;
}

sub days_to_seconds {

	my $days = shift;

	return $days * 24 * 60 * 60;
}
