#!/bin/sh

# checks a localhosts slave replication status
# used by PRTG
# See 
#  https://ashsprtg01.lsi.local/help/ssh_script_advanced_sensor.htm
#  https://ashsprtg01.lsi.local/api.htm?tabid=7

PORT=$1
if [ "$PORT" == "" ]
then
    PORT=3306
fi

SLAVE_STATUS=`mysql --user prtg --password='prtg' --protocol=tcp --port $PORT -e "show slave status\G" 2>&1`

if [[ $SLAVE_STATUS = ERROR* ]]
then
    cat <<EOD
<prtg>
  <error>1</error>
  <text>$SLAVE_STATUS</text>
</prtg>
EOD
    exit 2
fi

if [ "$SLAVE_STATUS" = "" ]
then
    # host is not a slave
    SECONDS_BEHIND_MASTER=0
    LAST_ERRNO=0
else
    SECONDS_BEHIND_MASTER=`echo "$SLAVE_STATUS" | grep Seconds_Behind_Master | awk '{print $2}'`
    LAST_ERRNO=`echo "$SLAVE_STATUS" | grep Last_Errno | awk '{print $2}'`
fi

if [ "$SECONDS_BEHIND_MASTER" = "NULL" ] && [ $LAST_ERRNO -gt 0 ]
then
    # check seconds_behind_master to zero if there is an error number
    SECONDS_BEHIND_MASTER=0
elif [ "$SECONDS_BEHIND_MASTER" = "NULL" ] && [ $LAST_ERRNO -eq 0 ]
then
    # this should not happen, but if it does set the LAST_ERRNO to
    # Error: 1590 SQLSTATE: HY000 (ER_SLAVE_INCIDENT) 
    SECONDS_BEHIND_MASTER=0
    LAST_ERRNO=1590
fi

cat <<EOD
<prtg>
  <result>
    <channel>Seconds Behind Master</channel>
    <value>$SECONDS_BEHIND_MASTER</value>
  </result>
  <result>
    <channel>Last Error Number</channel>
    <value>$LAST_ERRNO</value>
  </result>
</prtg>
EOD

