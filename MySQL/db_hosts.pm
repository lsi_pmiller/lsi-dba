package db_hosts;

use strict;


##
## GRANT SELECT, PROCESS, SUPER, REPLICATION CLIENT ON *.* TO 'repchk'@'%' IDENTIFIED BY 'pr0stak';
##

our $DB_USER = 'repchk';
our $DB_PASS = 'pr0stak';
our $DB_NAME = '';

our $DB = {

	## Ashburn ------------------------

	'db1-vm' => {
        host => '10.62.32.11',
#		master => 'sparedb-vm',
        query_warn => 90,
        query_crit => 360,
        kill_slow => 10,  # Kill only when there are 10 locked queries
        kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
	},

	'db2-vm' => {
		host => '10.62.32.21',
		master => 'db1-vm',
		max_delay => 480,
	},

	'db3-vm' => {
		host => '10.62.32.30',
		master => 'db1-vm',
		max_delay => 480,
#       downtime_start => '0:00',
#       downtime_end => '23:59',
	},

	'crondb1-vm' => {
		host => '10.62.32.16',
		master => 'db1-vm',
#		query_warn => 360,
        query_crit => 4800,
#		max_delay => 300,
		max_delay => 3600,
        downtime_start => '3:00',
        downtime_end => '5:30',
	},

	'admdb1-vm' => {
		host => '10.62.32.51',
		master => 'db1-vm',
		max_delay => 60,
	},

	'reportdb-vm' => {
		host => '10.62.32.61',
		master => 'db1-vm',
		max_delay => 1800,
		query_warn => 300,
		query_crit => 3600,
		kill_slow => 0,  # Kill any slow queries
		kill_slow_time => 300,  # seconds
		kill_slow_host => ['adm'],
        downtime_start => '0:01',
        downtime_end => '5:30',
	},

	'sparedb-vm' => {
		host => '10.62.32.71',
		master => 'db1-vm',
		max_delay => 1200,
#		downtime_start => '0:00',
#		downtime_end => '23:59',

# testing pmiller
       query_warn => 60,
       query_crit => 100,
		kill_slow => 0,  # Kill any slow queries
		kill_slow_time => 120,  # seconds
		kill_slow_host => ['vpndc.+'],
	},

	dbrpl => {
		host => '10.62.66.84',
		master => 'db1-vm',
		max_delay => 1200,
        query_warn => 300,
        query_crit => 600,
		downtime_start => '0:50',
		downtime_end => '4:00',
	},

	gwdb1 => {
		master	=> 'gwdb1-vm',
		max_delay => 30,
	},

	'gwdb1-vm' => {
		host => '10.62.32.41',
		master => undef,
		max_delay => 30,
	},

	gwdbrpl => {
		host => '10.62.66.70',
		master => 'gwdb1-vm',
		downtime_start => '0:50',
		downtime_end => '4:00',
	},

	bcdb1 => {
		master => 'bcdb1-vm',
	},

	'bcdb1-vm' => {
		host => '10.62.32.91',
		master => undef,
	},

	bcdbrpl => {
		host => '10.62.66.73',
		master => 'bcdb1-vm',
		downtime_start => '0:50',
		downtime_end => '4:00',
	},

	'rtdb-vm' => {
		host => '10.62.37.51',
		master => undef,
		query_warn => 200,
		query_crit => 300,
		kill_slow => 0,  # Kill any slow queries
		kill_slow_host => ['rt3'],
	},

	extdb1 => {
		host => '64.94.17.166',
		master => undef,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
	},

	cnet => {
		host => '10.62.32.101',
		master => undef,
		query_warn => 18000,
		query_crit => 28000,
        downtime_start => '0:00',
        downtime_end => '9:00',
	},

	dyscerndb => {
		host => '10.0.21.244',
		master => undef,
	},

	rapdb01 => {
#		host => 'rapdb01.ash.liquidation.com',
		host => '10.62.32.151',
		master => undef,
	},

	rapdbrpl => {
#		host => 'rapdbrpl.ash.liquidation.com',
		host => '10.62.66.91',
		master => 'rapdb01',
        downtime_start => '0:50',
        downtime_end => '4:00',
	},

    opsdb01 => {
        host => 'opsdb01.ash.liquidation.com',
        master => 'dbrpl',
		max_delay => 900,
		query_warn => 2800,
        query_crit => 28800,
        downtime_start => '0:01',
        downtime_end => '8:00',
    },

        opslpiddb01 => {
            host => 'opslpiddb01.ash.liquidation.com',
            master => 'lpiddbrpl',
#            downtime_start => '0:50',
#            downtime_end => '4:00',
        },

	## GALERA - production ------------------------------------------

    'lraydb01' => {
        host => 'lraydb01.ash.liquidation.com',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'lraydb02' => {
        host => '10.62.32.32',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'lraydb03' => {
        host => '10.62.32.33',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'stage-lraydb01' => {
        host => 'stage-lraydb01',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'stage-lraydb02' => {
        host => 'stage-lraydb02',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'stage-lraydb03' => {
        host => 'stage-lraydb03',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

    'lraydbbackup01' => {
        host => '10.62.32.201',
        query_warn => 90,
        query_crit => 360,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm'],
#       downtime_start => '0:01',
#       downtime_end => '2:30',
    },

	## DEV  ----------------------
#	wickedwitch => {
#		host => 'devdb',
#		master	=> 'db1',
#		max_delay => 1200,
#	},

	devdb => {
		host => 'devdb.internal.liquidation.com',
#		master  => 'devdbhub',
		max_delay => 4800,
#		query_warn => 30,
#		query_crit => 30,
		kill_slow => 1,
		kill_slow_host => ['vpnadmin.+'],
#       downtime_start => '0:00',
#       downtime_end => '25:59',
	},

#	devgwdb1 => {
#		host => 'devdb',
#		port => 3307,
#		master => 'gwdb1-vm',
#		max_delay => 1200,
#	},

	devbcdb1 => {
		host => 'devdb.internal.liquidation.com',
		port => 3308,
		master => 'bcdb1-vm',
		max_delay => 2000,
	},

	build => {
		host => 'devdb.internal.liquidation.com',
		port => 3309,
		master => 'devdbhub',
		max_delay => 1200,
	},

	devdbhub => {
		host => '10.65.88.17',
		port => 3306,
		master => 'dbrpl',
		max_delay => 1200,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
	},

#	devdb2 => {
#		host => 'devdb2.internal.liquidation.com',
#		port => 3306,
#		master => 'devdbhub',
#		max_delay => 4800,
#                query_warn => 1200,
#                query_crit => 4800,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
#	},

	devdb3 => {
		host => 'devdb3.internal.liquidation.com',
		port => 3306,
		master => 'devdbhub',
		max_delay => 1200,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
	},

	devcnet => {
		host => 'devdb2.internal.liquidation.com',
		port => 3307,
		master => 'cnet',
		max_delay => 86400,
                downtime_start => '0:00',
                downtime_end => '23:59',
	},

	devdyscern => {
		host => 'devdb2.internal.liquidation.com',
		port => 3308,
		master => '10.0.21.244',  ## dyscern.liquidation.com
		max_delay => 120,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	devlpidhub => {
		host => 'devdb.internal.liquidation.com',
		port => 3312,
		master => '10.62.66.63',
		max_delay => 3600,
		downtime_start => '0:00',
		downtime_end => '23:59',
	},

	devlpiddb => {
		host => 'devdb.internal.liquidation.com',
		port => 3311,
		master => 'devlpidhub',
		max_delay => 3600,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
	},

#	devlpiddb2 => {
	devdb2 => {
		host => 'devdb2.internal.liquidation.com',
		port => 3309,
		master => 'devlpidhub',
		max_delay => 3600,
#		downtime_start => '0:00',
#		downtime_end => '23:59',
	},

	# LPID dbs -----------------------

	lpiddb => {
		host => '10.62.32.111',
		port => 3306,
		master => undef,
#       downtime_start => '1:00',
#       downtime_end => '2:30',
	},
	lpiddbrpl => {
		host => '10.62.66.63',
		port => 3306,
		master => 'lpiddb',
		max_delay => 3600,
		downtime_start => '0:50',
        downtime_end => '4:00',
	},

	## DR Phoenix ---------------------

	## Frisco (DR) ------------------------

	'drdb1-vm' => {
		host => '10.67.181.25',
		master => 'db1-vm',
		query_warn => 1800,
		query_crit => 3600,
#		kill_slow => 10,  # Kill only when there are 10 locked queries
#		kill_slow_host => ['adm.'],
#       downtime_start => '0:01',
#       downtime_end => '23:59',
	},

	'drdb2-vm' => {
		host => '10.162.32.21',
		master => 'drdb1-vm',
		max_delay => 30,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	'drdb3-vm' => {
		host => '10.162.32.30',
		master => 'drdb1-vm',
		max_delay => 30,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	'drcrondb1-vm' => {
		host => '10.162.32.16',
		master => 'drdb1-vm',
		max_delay => 300,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	'dradmdb1-vm' => {
		host => '10.162.32.51',
		master => 'drdb1-vm',
		max_delay => 30,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	'drreportdb-vm' => {
		host => '10.162.32.61',
		master => 'drdb1-vm',
		max_delay => 15,
#		query_warn => 1800,
#		query_crit => 3600,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	'drsparedb-vm' => {
		host => '10.162.32.71',
#       query_warn => 40,
#       query_crit => 55,
#       kill_slow => 10,  # Kill only when there are 10 locked queries
#       kill_slow_host => ['adm.'],
		master => 'drdb1-vm',
		max_delay => 1200,
		downtime_start => '0:00',
		downtime_end => '23:59',
	},

# commented out
# pmiller Mon Sep 15 14:17:13 EDT 2014
#	'drdbrpl-vm' => {
#		host => '10.162.66.84',
#		master => 'drdb1-vm',
#		max_delay => 1200,
#		query_warn => 300,
#		query_crit => 600,
#		downtime_start => '1:00',
#		downtime_end => '1:30',
#                downtime_start => '23:55',
#                downtime_end => '23:59',
#	},

	# BC

	'drbcdb1-vm' => {
		host => '10.67.181.24',
		master => 'bcdb1-vm',
        downtime_start => '7:59',
        downtime_end => '10:00',
	},

	'drbcdbrpl-vm' => {
		host => '10.162.66.73',
		master => 'drbcdb1-vm',
		downtime_start => '1:00',
		downtime_end => '1:30',
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	# RT

	'drrtdb-vm' => {
		host => '10.162.37.51',
		master => 'rtdb-vm',
#		kill_slow => 60,  # Kill only when there are 10 locked queries
#		kill_slow_host => ['rt3.'],
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

#	drextdb1 => {
#		master => undef,
#	},

	# CNET

	'drcnet' => {
		host => '10.162.32.101',
		master => 'cnet',
		query_warn => 1800,
		query_crit => 3600,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},

	# DR LPID

	'drlpiddb-vm' => {
		host => '10.67.181.22',
		master => 'lpiddb',
        downtime_start => '23:50',
        downtime_end => '1:00',
	},

# commented out
# pmiller Mon Sep 15 14:18:16 EDT 2014
#	'drlpiddbrpl-vm' => {
#		host => '10.162.66.63',
#		master => 'drlpiddb-vm',
#		max_delay => 3600,
#		downtime_start => '1:00',
#		downtime_end => '1:30',
#                downtime_start => '23:55',
#                downtime_end => '23:59',
#	},

	# BACKUP

	'backupdb' => {
		host => '10.67.181.23',
		master => 'drdb1-vm',
		max_delay => 3600,
		query_warn => 1800,
		query_crit => 3600,
        downtime_start => '6:59',
        downtime_end => '20:00',
	},

	'backupdb-test' => {
		host => '10.162.32.103',
		master => undef,
		query_warn => 1800,
		query_crit => 3600,
        downtime_start => '0:00',
        downtime_end => '23:59',
	},
};

