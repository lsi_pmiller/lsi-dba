#!/usr/bin/env python

import argparse
import boto
import boto.ec2
import boto.rds2
import sys
from pprint import pprint
import time

#boto.set_stream_logger('boto')

parser = argparse.ArgumentParser(
    description="starts ec2 server(s)",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--aws_access_key_id",
    help="aws access key",
)
parser.add_argument(
    "--aws_secret_access_key",
    help="aws secret key",
)
parser.add_argument(
    "--aws_region",
    help="aws region",
    default="us-east-1"
)
parser.add_argument(
    "--aws_account",
    help="aws account number",
)
parser.add_argument(
    "--key_name",
    help="key name",
    default="owner"
)
parser.add_argument(
    "--key_value",
    help="key value",
    default="paul.miller"
)
parser.add_argument(
    "action",
    help="action",
    nargs='?',
    choices=['start', 'status', 'stop'],
    default="status",
)
args = parser.parse_args()


if (args.aws_access_key_id):
    aws_access_key_id=args.aws_access_key_id
else:
    aws_access_key_id='XXXX'

if (args.aws_secret_access_key):
    aws_secret_access_key=args.aws_secret_access_key
else:
    aws_secret_access_key='XXXX'

if (args.aws_account):
    aws_account=args.aws_account
else:
    aws_account='XXXX'

aws_region=args.aws_region


def get_ec2_connection():
    conn = boto.ec2.connect_to_region(aws_region,
                                      aws_access_key_id=aws_access_key_id,
                                      aws_secret_access_key=aws_secret_access_key)

    return conn


def get_rds2_connection():
    conn = boto.rds2.connect_to_region(aws_region,
                                       aws_access_key_id=aws_access_key_id,
                                       aws_secret_access_key=aws_secret_access_key)

    return conn



def get_instances_via_tag(conn):
    instances = conn.get_only_instances(filters={"tag:{}".format(args.key_name) : "{}".format(args.key_value)})

    return instances

def get_rds_instances_via_tag(conn):
    filter='"tag:{}" : "{}"'.format(args.key_name, args.key_value)
    instances = conn.describe_db_instances()['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances'];
#    pprint(instances)

    matching_instances = []
    for i in instances:
        # an Amazon Resource Name (ARN) for an RDS is in the form of
        #   arn:aws:rds:<region>:<account number>:db:<dbinstance name> 
        arn="arn:aws:rds:" + args.aws_region + ":" + aws_account + ":db:" + i['DBInstanceIdentifier']
        tags = conn.list_tags_for_resource(arn)['ListTagsForResourceResponse']['ListTagsForResourceResult']['TagList']
#        pprint(tags)
        for t in tags:
            if ((t['Key'] == args.key_name) and (t['Value'] == args.key_value)):
                matching_instances.append(i)          

    return matching_instances


def print_ec2_instance_info(instance):
#    pprint(instance.__dict__)
#    print "=========="

    if (instance.ip_address):
        print "  id:{:10s} name:{:30s} state:{:10s} ip_address:{:15s}".format(instance.id,
                                                              instance.tags['Name'],
                                                              instance.state, 
                                                              instance.ip_address)
    elif (instance.private_ip_address):
        print "  id:{:10s} name:{:30s} state:{:10s} private_ip_address:{:15s}".format(instance.id,
                                                                      instance.tags['Name'],
                                                                      instance.state, 
                                                                      instance.private_ip_address)
    else:
        print "  id:{:10s} name:{:30s} state:{:10s}".format(instance.id,
                                                            instance.tags['Name'],
                                                            instance.state)

def print_rds_instance_info(instance):
    if (instance['Endpoint']):
        print "  id:{:20s} status:{:10s} Endpoint:{:60s}".format(instance['DBInstanceIdentifier'],
                                                                 instance['DBInstanceStatus'],
                                                                 instance['Endpoint']['Address'])
    else:
        print "  id:{:20s} status:{:10s}".format(instance['DBInstanceIdentifier'],
                                                 instance['DBInstanceStatus'])


def show_instances_via_tag():
    conn = get_ec2_connection()
    instances = get_instances_via_tag(conn)
    filter='{} : {}'.format(args.key_name, args.key_value)
    print "ECS instances with tag [{}]".format(filter)
    for i in instances:
        print_ec2_instance_info(i)

    conn = get_rds2_connection()
    instances = get_rds_instances_via_tag(conn)
    filter='{} : {}'.format(args.key_name, args.key_value)
    print "RDS instances with tag [{}]".format(filter)
    for i in instances:
        print_rds_instance_info(i)


def start_instances_via_tag():
    conn = get_ec2_connection()
    instances = get_instances_via_tag(conn)

    for i in instances:
        print "  id:{} name:{} state:{}".format(i.id,
                                                i.tags['Name'],
                                                i.state)

    instance_ids = [i.id for i in instances]
    started_instances = conn.start_instances(instance_ids)
    for i in started_instances:
        print "Instance: {}".format(i)
        while i.state != u'running':
            print "    State: {}".format(i.state)
            time.sleep(10)
            i.update()

    for i in started_instances:
        print_ec2_instance_info(i)


def stop_instances_via_tag():
    conn = get_ec2_connection()
    instances = get_instances_via_tag(conn)
    for i in instances:
        print "  id: {} state: {}".format(i.id, i.state)

    instance_ids = [i.id for i in instances]
    stopped_instances = conn.stop_instances(instance_ids)
    for i in stopped_instances:
        print "Instance: {}".format(i)
        while i.state != u'stopped':
            print "    State: {}".format(i.state)
            time.sleep(10)
            i.update()

    for i in stopped_instances:
        print_ec2_instance_info(i)



if (args.action == 'start'):
    start_instances_via_tag()
elif (args.action == 'stop'):
    stop_instances_via_tag()
else:
    show_instances_via_tag()    

print
print "AWS_ID=$HOME/LSI/AWS/$USER-eu.pem"
print "rsync -aP -e \"ssh -i $AWS_ID\" $SOURCE  ec2-user@${IP}:$DESTINATION"
print "ssh -i $AWS_ID ec2-user@${IP}"
