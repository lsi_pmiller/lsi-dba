#!/usr/bin/env python

import argparse
import boto
import boto.ec2
import boto.rds2
import sys
from pprint import pprint
import time

#boto.set_stream_logger('boto')

# 10m9.262s t1-micro    5G
# 10m38.413s m3-2xlarge 5G



parser = argparse.ArgumentParser(
    description="starts ec2 server(s)",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--aws_access_key_id",
    help="aws access key",
)
parser.add_argument(
    "--aws_secret_access_key",
    help="aws secret key",
)
parser.add_argument(
    "--aws_region",
    help="aws region",
    default="us-east-1"
)
parser.add_argument(
    "--aws_account",
    help="aws account number",
)
parser.add_argument(
    "--db_snapshot_identifier",
    help="db_snapshot_identifier",
    default="pmiller-snap-" + time.strftime("%Y-%m-%d-%H%M%S")
)
parser.add_argument(
    "--db_instance_identifier",
    help="db_instance_identifier",
    default="pmiller-restore-" + time.strftime("%Y-%m-%d-%H%M%S")
)
parser.add_argument(
    "--db_instance_class",
    help="db_instance_class",
    choices=['db.t1.micro','db.m1.small','db.m1.medium','db.m1.large','db.m1.xlarge','db.m2.2xlarge','db.m2.4xlarge','db.m3.2xlarge'],
    default="db.t1.micro",
)
parser.add_argument(
    "--db_parameter_group_name",
    help="db_parameter_group_name",
    default="dev-mysql5-5"
)
parser.add_argument(
    "--wait",
    help="wait (loop) until the rds database state is available",
    action="store_true",
)
parser.add_argument(
    "action",
    help="action",
    nargs='?',
    choices=['restore', 'status', 'modify', 'restore_wait_modify', 'list'],
    default="status",
)
args = parser.parse_args()


if (args.aws_access_key_id):
    aws_access_key_id=args.aws_access_key_id
else:
    aws_access_key_id='AKIAJZFD73A5TNWUFVWQ'
    
if (args.aws_secret_access_key):
    aws_secret_access_key=args.aws_secret_access_key
else:
    aws_secret_access_key='gTTmEv2r9jnk92Sjwep7kuepP/Xxmg0IJnYz9DFT'
    
if (args.aws_account):
    aws_account=args.aws_account
else:
    aws_account='202244129857'

aws_region=args.aws_region


def get_rds2_connection():
    conn = boto.rds2.connect_to_region(aws_region,
                                       aws_access_key_id=aws_access_key_id,
                                       aws_secret_access_key=aws_secret_access_key)

    return conn



def get_rds_instance_via_name(conn, db_instance_identifier):
    instance = conn.describe_db_instances(db_instance_identifier=db_instance_identifier)['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances'][0];
    #pprint(instance)
    return instance


def do_restore(db_instance_identifier, db_snapshot_identifier):    
    conn = get_rds2_connection()

    print "creating db_instance:{} from db_snapshot:{}".format(db_instance_identifier,
                                                               db_snapshot_identifier)
    conn.restore_db_instance_from_db_snapshot(db_instance_identifier, 
                                              db_snapshot_identifier, 
                                              db_instance_class=args.db_instance_class,
                                              db_subnet_group_name="use-dev-db", 
                                              multi_az=False, 
                                              tags=[('owner', 'paul.miller')])


def do_modify(db_instance_identifier):    
    print "modifying db_instance:{} setting db_parameter_group_name to {}".format(db_instance_identifier,
                                                                                  args.db_parameter_group_name)
    conn = get_rds2_connection()
    conn.modify_db_instance(db_instance_identifier, 
                            apply_immediately=True,
                            db_parameter_group_name=args.db_parameter_group_name)


def do_reboot(db_instance_identifier):
    conn = get_rds2_connection()
    conn.reboot_db_instance(db_instance_identifier)


def print_rds_instance_info(instance):
    if (instance['Endpoint']):
        print "  id:{:20s} status:{:10s} Endpoint:{:60s}".format(instance['DBInstanceIdentifier'],
                                                                 instance['DBInstanceStatus'],
                                                                 instance['Endpoint']['Address'])
    else:
        print "  id:{:20s} status:{:10s}".format(instance['DBInstanceIdentifier'],
                                                 instance['DBInstanceStatus'])


def show_dbsnapshots(db_instance_identifier):
    conn = get_rds2_connection()
    snapshots = conn.describe_db_snapshots(db_instance_identifier)['DescribeDBSnapshotsResponse']['DescribeDBSnapshotsResult']['DBSnapshots']
    for s in snapshots:    
        #pprint(s)
        print "  DBSnapshotIdentifier {:40s} SnapshotCreateTime {}".format(s['DBSnapshotIdentifier'],
                                                                           s['SnapshotCreateTime'])

        
        
def show_instance_via_name(db_instance_identifier):
    conn = get_rds2_connection()
    instance = get_rds_instance_via_name(conn,db_instance_identifier)
    print "RDS instances:[{}]".format(db_instance_identifier)

    if (args.wait):
        sleep_time = 30
        while instance['DBInstanceStatus'] != u'available':
            print "    State: {:15s} - sleeping for {}s".format(instance['DBInstanceStatus'], sleep_time)
            time.sleep(sleep_time)
            #instance.update()   # does not work ?
            instance = get_rds_instance_via_name(conn,db_instance_identifier)
        print "RDS instances:[{}] is available".format(db_instance_identifier)

    print_rds_instance_info(instance)



if (args.action == 'restore'):
    do_restore(args.db_instance_identifier, args.db_snapshot_identifier)
elif (args.action == 'modify'):
    do_modify(args.db_instance_identifier)
    do_reboot(args.db_instance_identifier)
elif (args.action == 'restore_wait_modify'):
    args.wait = True
    do_restore(args.db_instance_identifier, args.db_snapshot_identifier)
    show_instance_via_name(args.db_instance_identifier)
    do_modify(args.db_instance_identifier)
    do_reboot(args.db_instance_identifier)
elif (args.action == 'list'):
    show_dbsnapshots(args.db_instance_identifier)

show_instance_via_name(args.db_instance_identifier)

