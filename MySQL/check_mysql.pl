#!/usr/bin/perl -w

###
# A replacement for the nagios check_db script
# with all the functionality of the orginal check_db script
#
# reads from the same db config file /usr/local/nagios/libexec/db_hosts.pm
# (moved into /home/mysql/bin)
# designed to run outside of nagios and kill long running queries
# via the option [--check_name processlist]
#
# writes to syslog which is indexed by the splunk server
###

use strict;
use warnings FATAL => 'all';

use DBI;
use Data::Dumper;
use Getopt::Long;
use HTML::Entities;
use Mail::Sendmail;
use Sys::Syslog;

use lib '/home/mysql/bin';
use db_hosts;

$Data::Dumper::Indent    = 1;
$Data::Dumper::Sortkeys  = 1;
$Data::Dumper::Quotekeys = 0;

$| = 1;

my $debug = $ENV{DEBUG} || 0;
my ($help, @db_server_names, @checks);
my $syslog = 0;
my $KILL_SLOW = 0;
my $sendEmail = 0;
my $emailAddress = 'al.dba@lqdt.com';

my ($program_name) = ($0 =~ m|/([^/]+)\..+$|);
my $lockfile = '/tmp/' . $program_name . '.lock';

GetOptions (
    'db_name=s'      => \@db_server_names,
    'check_name=s'   => \@checks,
    'kill_slow'      => \$KILL_SLOW,

    'syslog'         => \$syslog,

    'sendEmail'      => \$sendEmail,
    'emailAddress=s' => \$emailAddress,

    'debug'        => \$debug,
    'help'         => \$help,
    );
@db_server_names = split(/\s*,\s*/,join(',',@db_server_names));
@checks = split(/\s*,\s*/,join(',',@checks));


## GLOBALS ------------------------------------------------------

my %ERRORS  = ('OK' => 0, 'WARNING' => 1, 'CRITICAL' => 2, 'UNKNOWN' => 3, 'DEPENDENT' => 4);
my $state   = "OK";
my $message = "OK";

my @check_names = qw( all processlist slave tables grants );

my %RESULT = ();

my $DB      = $db_hosts::DB;
my $DB_USER = $db_hosts::DB_USER;
my $DB_PASS = $db_hosts::DB_PASS;
my $DB_NAME = $db_hosts::DB_NAME;

## FOR EMAIL -----------------------------------------------------
my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );

## OVERRIDE ------------------------------------------------------

# These settings will override any config settings

# Attempt to start slave
my $RESTART_SLAVE = 0; 


## TODAY ---------------------------------------------------------
my ($year, $month, $day, $hour, $min) = (localtime)[5,4,3,2,1];
$year += 1900;
$month++;

my $today = sprintf("%4d%02d%02d", $year, $month, $day);

## DISPLAY -------------------------------------------------------
my $max_queries = 20;
my $max_info_length = 300;
my $max_info_length_huge = 100000;  # for email and logging

## THROTTLE ------------------------------------------------------

my $max_attempts = 3;
my $seconds_between_attempts = 5;

## QUERY WATCHDOG ------------------------------------------------

## Default thresholds
my $query_time_threshold          = 1200;
my $critical_query_time_threshold = 3200;

## REPLICATION ---------------------------------------------------

## Between 0 and 6 the replication threshhold is higher
my $replication_threshold = ($hour >= 0 && $hour <= 8) ? 1800 : 60;

#	## Do not bother alarming us between 2 and 6 in the morning! ;-)
#	$replication_threshold = (8 * 60) if ($hour >= 2 and $hour <= 6);

## THREAD COUNT --------------------------------------------------

## Running threads
my $default_running_threads_threshold_warning  = 400;
my $default_running_threads_threshold_critical = 600;

## Total threads
my $default_thread_threshold_warning  = 600;
my $default_thread_threshold_critical = 1300;

## TABLES --------------------------------------------------------

my $full_table_threshold = 80; # Percent full

## ---------------------------------------------------------------

## Parameters check ----------------------------------------------

if ($help) {
    usage();
    exit;
}

my %check = map { $_, 1 } @checks;

if ( !  defined $db_server_names[0] ) {
    usage();
    die "\n--db_name not specified!\n\n";
}

my @targets;
if ( grep { 'ALL' eq $_} @db_server_names ) {
    my %hosts = map {
        my $tmp = defined($DB->{$_}{host}) ? $DB->{$_}{host} : $_;
        $tmp => 1;
    } keys %$DB;
    @db_server_names = qw( ALL );
    @targets = sort keys %hosts;
} else {
    foreach my $db (@db_server_names) {
        if ( ! grep { $db eq $_ } keys %$DB ) {
            usage();
            die "\nInvalid db_name '$db'!\n\n";
        }
    }
    @targets = sort( uniq( @db_server_names ) );
}
        
    
foreach my $check_name (keys %check) {
    if ( ! grep { $check_name eq $_ } @check_names ) {
        usage();
        die "\nInvalid check name '$check_name'!\n\n";
    }
}

%check = map { $_, 1 } @check_names if $check{all};

print Dumper(\%check) if $debug;

if ($syslog) {
    openlog($program_name, '', 'user');
}

if (! -e $lockfile ) {
    print "Creating lockfile: $lockfile\n" if $debug;
    open (LOCK, ">$lockfile") or
        die "Could not create $lockfile: $!";
    close LOCK;
} else {
    my $mtime = (stat($lockfile))[9];
    my $timestamp = time();
    my $age = $timestamp - $mtime;

    my $msg_line = "FATAL: Exiting, localhost lock file [$lockfile] age ${age}s exists! \n";
    $msg_line   .= "Either; another instance of $program_name is running \n";
    $msg_line   .= "or an earlier run did not exit cleanly. \n";
    $msg_line   .= "Please correct\n";

    my $hostname = `hostname -s`;
    chop $hostname;
    print STDERR $msg_line;
    if ($age > 300) {  # 5 minutes
        sendEmail($hostname, "lock file exists", ('Localhost' => $hostname, 'Error' => $msg_line));
    }
    $msg_line =~ s/\n//g;
    log_syslog($hostname, "Main", $msg_line);
    die "existing lockfile [$lockfile]: $!";
}



## Targets -------------------------------------------------------

foreach my $target (@targets) {

    ## MySQL instances per host --------------------------------------

    print "Reading mysql instances for host '$target'\n" if $debug;

#    my @mysql_instances = get_mysql_instances($target);
#
#    my $instance_count = scalar @mysql_instances || 0;
#
#    map { $DB->{$_}{'instance_count'} = $instance_count } @mysql_instances;
#
#    print "Instances found ($instance_count): " . join(', ', @mysql_instances) . "\n" if $debug;
#
#    foreach my $instance (@mysql_instances) {
#
#        print "Checking $instance [" if $debug;
#        check_database($instance);
#
#        print "]\n" if $debug;
#    }

    print "Checking $target [" if $debug;
    check_database($target);    
    print "]\n" if $debug;

} # targets

## Process results -----------------------------------------------

print "RESULT: " if $debug;

#print Dumper \%RESULT;

if (exists($RESULT{'CRITICAL'})) {
	
    my $msg = join("\n---\n", @{$RESULT{'CRITICAL'}});
	
    ## Append warnings, if any
    if (exists($RESULT{'WARNING'})) {
        $msg .= "\n---\nWARNING: " . join("\n---\n", @{$RESULT{'WARNING'}});
    }
	
    my_die('CRITICAL', $msg); 
	
}
elsif (exists($RESULT{'WARNING'})) {
	
    my $msg = join("\n---\n", @{$RESULT{'WARNING'}});
	
    my_die('WARNING', $msg);
	
}
elsif (exists($RESULT{'OK'})) {
	
    my $msg;
    my %instances;
	
    foreach my $m (@{$RESULT{'OK'}}) {
        my ($key, $value) = $m =~ m!^(\[.+\])\s+(.+)$!;
        next if !defined $key;
        push @{$instances{$key}}, $value;
    }

    if (keys %instances) {
        foreach my $i (sort keys %instances) {
            $msg .= $i . ' ' . join(',', @{$instances{$i}}) . "\n";
        }
    }
    else {
        $msg = join(',', @{$RESULT{'OK'}});
    }

    my_die('OK', $msg);
	
}
else {
    my_die("OK");
}

## END -----------------------------------------------------------

print "\nHuh?\n";

## Print usage ---------------------------------------------------

sub usage {

    my $all_dbs =  join(', ', sort keys %$DB);
    my $all_checks = join(', ', @check_names);

	print <<__EOD__;
USAGE: $0 --db_name=s [--db_name=s,s,s] [--check_name=s] [--check_name=s,s,s] [--kill_slow] [--syslog] ...


  --db_name        : database name as defined in db_hosts.pm
                     option can be repeated or a comma seperated list 
  --check_name     : database check to perform
                     option can be repeated or a comma seperated list 
  --kill_slow      : if set will terminat the top slow query
                     defaults to [$KILL_SLOW]
  --syslog         : if set syslog for splunk reporting is turned on
                     note that noc.ash.liquidation.com syslog is at
                     /var/log/HOSTS/noc/YYYY/MM/DD/usernocYYYYMMDD
                     or read the logs via splunk
                     defaults to [$syslog]
  --sendEmail      : if set will send email to --emailAddress
                     email is sent on FAILURE/CRITICAL only
                     defaults to [$sendEmail]
  --emailAddress   : defaults to [$emailAddress]
  --debug          : debug flag defaults to [$debug]
  --help           : this page

Supported databases: ALL, $all_dbs

Available checks: $all_checks

See /home/mysql/bin/db_hosts.pm for per db_name options and overrides

__EOD__
}

sub log_syslog {
    my $instance = shift;
    my $check = shift;
    my $message = shift;

    syslog('info', '%s', "$instance: $check: $message") if ($syslog);
}    

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

## MySQL instances -----------------------------------------------

sub get_mysql_instances {
    # sub is no longer used
	my $name = shift;

	my $db = $DB->{$name};

	my $host = defined($db->{host}) ? $db->{host} : $name;

	my @instances = grep {
		defined($DB->{$_}{host}) && $DB->{$_}{host} eq $host
		|| $_ eq $name
	} keys %$DB;

	return @instances; 
}
## ---------------------------------------------------------------

## DATABASE ------------------------------------------------------
sub check_database {

	my $name = shift;

	my $db = $DB->{$name};

	my $host = $db->{host} ? $db->{host} : $name;

	# Handle downtime
	if (exists($db->{downtime_start}) && exists($db->{downtime_end})) {

		my ($dhs, $dms) = $db->{downtime_start} =~ m!(\d+):?(\d*)!;
		my ($dhe, $dme) = $db->{downtime_end}   =~ m!(\d+):?(\d*)!;

		$dms ||= 0;
		$dme ||= 0;

		my $now = $hour * 60 + $min;
		my $ds = $dhs * 60 + $dms;
		my $de = $dhe * 60 + $dme;

		if ($ds > $de) {
			$now += 1440;
			$de += 1440;
		}

		if ( $now >= $ds && $now <= $de ) {
			printf("DOWNTIME (%02d:%02d - %02d:%02d)", $dhs, $dms, $dhe, $dme) if $debug;
            log_syslog($name, "Main", sprintf("DOWNTIME (%02d:%02d - %02d:%02d)", $dhs, $dms, $dhe, $dme));
			return;
		}
	} 

	my $dsn = ";host=$host";

	$dsn .= ";port=$db->{port}" if $db->{port};

	my $dbh;

	print "connection: " if $debug;

	## Create db handle
	eval {
		$dbh = DBI->connect(
			"DBI:mysql:$dsn",
			$DB_USER,
			$DB_PASS,
			{ RaiseError => 1, PrintError => 0 }
		);
	};

	if ($@) {
		push @{$RESULT{'CRITICAL'}},
			"$name: Cannot connect to db: " . DBI->errstr();
		print "CRITICAL" if $debug;
        log_syslog($name, "check_connection", "connection CRITICAL");
		return 0;
	}

	print "OK" if $debug;
    log_syslog($name, "check_connection", "OK");

	$db->{dbh} = $dbh;
	$db->{name} = $name;
#	$db->{instance} =
#		($db->{'instance_count'} > 1 || $db_server_names[0] eq 'ALL')
#		? "[$db->{name}] " : '';

	my $result;

	if ($check{processlist}) {

		print " processlist: " if $debug;
		$result = check_processlist($db);

		print "OK" if $result && $debug;
	}

	if ($check{slave}) {

		print " slave: " if $debug;

		if (defined $db->{master}) {
			$result = check_slave($db);
			print "OK" if $result && $debug;            
		}
		else {
			push @{$RESULT{'OK'}}, 'Not slave';
			print "SKIP" if $debug;
            log_syslog($name, "check_slave", "OK (Not a slave)");
		}
	}

	if ($check{tables}) {

		print " tables: " if $debug;

		$result = check_tables($db);

		print "OK" if $result && $debug;
	}

	if ($check{grants}) {

		print " grants: " if $debug;

		$result = check_grants($db);

		print "OK" if $result && $debug;
	}
}
## ---------------------------------------------------------------

## SLAVE ---------------------------------------------------------
sub check_slave {

	my $db = shift;

	my $dbh = $db->{dbh};

	my $instance = $db->{'name'};

	## Check replication

	my $sth = $dbh->prepare("SHOW SLAVE STATUS");
    $sth->execute();
	my $slave_status = $sth->fetchrow_hashref();
    $sth->finish();

#	print STDERR Dumper $slave_status;
    if (! defined $slave_status->{Slave_SQL_Running}) {
        print "SHOW SLAVE STATUS is empty" if $debug;
        push @{$RESULT{'WARNING'}}, 'SHOW SLAVE STATUS is empty';
        log_syslog($db->{'name'}, "check_slave", "WARNING SHOW SLAVE STATUS is empty");
        return 0;
    }

	if ($slave_status->{Slave_SQL_Running} eq 'No'
		|| $slave_status->{Slave_IO_Running} eq 'No') {

		# Bypass temporary replication issues
#		if ($db->{name} =~ /^(devdb|devdb3)$/) {
#			push @{$RESULT{'WARNING'}}, sprintf('%s%s', $instance, "bypass");
#			print 'WARNING' if $debug;
#			return 0;
#		}
		# /Bypass

		my $error = $slave_status->{Last_Error}
			? $slave_status->{Last_Error}
			: "Replication stopped";

		if($RESTART_SLAVE) {
			$dbh->do("SLAVE STOP");
			$dbh->do("SLAVE START");
			push @{$RESULT{'CRITICAL'}}, $instance . "Attempted to restart slave. ($error)";
			print "CRITICAL" if $debug;
            log_syslog($instance, "check_slave", "Attempted to restart slave. ($error)");
			return 0;
		}
		else {
			push @{$RESULT{'CRITICAL'}}, $instance . "Slave is not replicating! ($error)";
			print "CRITICAL" if $debug;
            log_syslog($instance, "check_slave", "Slave is not replicating! ($error)");
			return 0;
		}
	}

	my $time = $slave_status->{'Seconds_Behind_Master'} || 0;

	my $threshold = ( defined($db->{max_delay}) && $db->{max_delay} >= $replication_threshold )
		? $db->{max_delay}
		: $replication_threshold;

	if ($time > $threshold) {

		## Is it critical?
		my $result_type = ($time > $threshold * 2) ? 'CRITICAL' : 'WARNING';

		push @{$RESULT{$result_type}},
			$instance . "Slow replication: " . secs_to_time($time) . ' (' . secs_to_time($threshold) .')';	
        log_syslog($instance, "check_slave",  "Slow replication: " . secs_to_time($time) . ' (' . secs_to_time($threshold) .')');
		print $result_type if $debug;

		# Add some information
#		my $pl = get_processlist($dbh, User => 'system user');
		my $pl = get_processlist($dbh); # Get all processes
		my $info = format_processlist($pl);
		push @{$RESULT{$result_type}}, $info;
        log_syslog($instance, "check_slave", "Slow replication: " . secs_to_time($time) . ' (' . secs_to_time($threshold) .')');


		return 0;
	}

	push @{$RESULT{'OK'}}, sprintf('%s%s', $instance, secs_to_time($time));

	return 1;
}
## ---------------------------------------------------------------

## PROCESSLIST ---------------------------------------------------
sub check_processlist {

	my $db = shift;

	my $dbh = $db->{dbh};

	my $instance = $db->{'name'};

	my $processlist;

	my $attempt_count = 0;
	my $threads_ok = 0;
	my $header = '';

	## Custom thread threshold
	my $threads_warning  = defined($db->{threads_warning})
		? $db->{threads_warning}
		: $default_thread_threshold_warning;
	my $threads_critical = defined($db->{threads_critical})
		? $db->{threads_critical}
		: $default_thread_threshold_critical;

	## Custom running threads threshold
	my $threads_running_warning  = defined($db->{threads_running_warning})
		? $db->{threads_running_warning}
		: $default_running_threads_threshold_warning;
	my $threads_running_critical = defined($db->{threads_running_critical})
		? $db->{threads_running_critical}
		: $default_running_threads_threshold_critical;

	## Check $max_attempts
	PLIST:
	while($attempt_count < $max_attempts && !$threads_ok) {

		$attempt_count++;

		$processlist = get_processlist($dbh);

		## Check number of threads running ---------------------------------------

        my $total_threads = scalar keys %$processlist;
        my $threads_running = grep {
#				$processlist->{$_}{'User'} ne 'system user'
#				&& $processlist->{$_}{'Command'} !~ m!^(Sleep|Binlog Dump|Delayed insert)$!
            $processlist->{$_}{'Command'} !~ m!^Sleep$!
        } keys %$processlist;

        #print Dumper( map { $processlist->{$_} } grep { $processlist->{$_}{'Command'} !~ m!^(Sleep|Binlog Dump|Delayed insert)$! } keys %$processlist);

        printf("(%d/%d) ", $threads_running, $total_threads ) if $debug;
	
        $header = sprintf(
            "Running threads: %d(%d) of %d(%d) ",
            $threads_running, $threads_running_critical,
            $total_threads, $threads_critical
			);

        if ($threads_running >= $threads_running_critical
            || $total_threads >= $threads_critical) {

            ## Format processlist
            my $plist = format_processlist($processlist, $max_queries);

            ## Send critical message
            push @{$RESULT{'CRITICAL'}}, $instance . $header;
            push @{$RESULT{'CRITICAL'}}, $plist;
            print "CRITICAL" if $debug;
            log_syslog($db->{name}, "check_processlist", "CRITICAL $header $plist");
            log_syslog($db->{name}, "check_processlist", "CRITICAL $plist");

            last PLIST;
        }
        elsif ($threads_running >= $threads_running_warning
               || $total_threads >= $threads_warning) {
            
            ## Try again?
            if($attempt_count < $max_attempts) {
                print "S${seconds_between_attempts}s " if $debug;
                sleep($seconds_between_attempts);
                next PLIST;
            }

            ## Format processlist
            my $plist = format_processlist($processlist, $max_queries);
            
            ## Send warning message
            push @{$RESULT{'WARNING'}}, $instance . $header;
            push @{$RESULT{'WARNING'}}, $plist;
            print "WARNING" if $debug;
            log_syslog($db->{name}, "check_processlist", "WARNING $header $plist");
            log_syslog($db->{name}, "check_processlist", "WARNING $plist");

        }
        else {
            ## Threads look OK
            push @{$RESULT{'OK'}}, sprintf('%s%d/%d', $instance, $threads_running, $total_threads);
            log_syslog($db->{name}, "check_processlist", 
                       "OK threads running:$threads_running total:$total_threads");

            $threads_ok = 1;
        }
        
	} ## End attempts

	## Check slow queries ----------------------------------------------------

	## Custom Thresholds
	my $warn_query_time = defined($db->{query_warn}) ? $db->{query_warn} : $query_time_threshold;
	my $crit_query_time = defined($db->{query_crit}) ? $db->{query_crit} : $critical_query_time_threshold;

	my @slow_queries;
	my $filler_queries = {};
	my %locked;

	## Analyze queries
	foreach my $pid (
		# Sort by Time DESC, State ne 'Locked' DESC
		# Basically if Time is equal, put non-locked queries on the top
		sort {
			$processlist->{$b}{Time} <=> $processlist->{$a}{Time}
			|| (
				defined($processlist->{$b}{State})
				&& ($processlist->{$b}{State} ne 'Locked'
					|| $processlist->{$b}{State} ne 'Waiting for table level lock'
				)
			)
		}
		grep { defined($processlist->{$_}{Time}) } keys %$processlist) {

		my $p = $processlist->{$pid};

		next if $p->{Command} ne 'Query';

		next if defined($p->{Info}) && $p->{Info} =~ /SHOW FULL PROCESSLIST/i;

		## Ignore long "use_mysql_result" queries
		next if defined($p->{State}) && $p->{State} =~ /Writing To Net/i;

		next if defined($p->{Info}) &&  $p->{Info} =~ /!40001 SQL_NO_CACHE/;

#		print Dumper($p);

		## Is the query slow?
		if ($p->{Time} >= $warn_query_time) {
			push @slow_queries, $pid;
		}
		else {
			$filler_queries->{$pid} = $p;
		}

		## Is the query locked?
		if (defined($p->{State})
			&& ($p->{State} eq 'Locked' || $p->{State} eq 'Waiting for table level lock')
		) {
			$locked{$pid}++;
		}
	}

	my $slow_count = scalar @slow_queries || 0;
	my $lock_count = keys %locked || 0;

	## Decide on what to do
	if (!$slow_count) {
		push @{$RESULT{'OK'}}, sprintf('%sS%dL%d', $instance, $slow_count, $lock_count);
        log_syslog($db->{name}, "slow_queries", sprintf("OK Slow:%d Lock:%d", $slow_count, $lock_count));
	}
	else {

		my $worst_time = 0;
		my $count = 0;

		# Already ordered by time
		foreach my $pid (@slow_queries) {

			$count++;

			if ($count > $max_queries) {
				push @{$RESULT{'WARNING'}}, sprintf('Skipping next %d', $slow_count - $count + 1);
                log_syslog($db->{name}, "slow_queries", sprintf("WARNING Skipping next %d",
                                                $slow_count - $count + 1));
				last;
			}

			my $p = $processlist->{$pid};

			$p->{State} ||= '';

			my ($slow_host) = $p->{Host} =~ /^([^.:]+)/;

			my $info = no_ctrl_char($p->{Info});
			$info =~ s/\n/ /g;
			$info =~ s/\s+/ /g;

            # Temporary disable annoying critical
            return 1 if $count == 1 && $info=~ m!^INSERT INTO auction_coi_approved!;
            return 1 if $count == 1 && $info=~ m!^SELECT auction_id, close_time, current_bid, number_of_bids, close_flag FROM auction_search_tmp!;

			my $time = $p->{Time};

			# Remember worst query time
			$worst_time = $time if !$worst_time;

			my $msg_line = sprintf(
				"SLOW Time: %s %s [%s]\n%s",
				secs_to_time($time),
				$slow_host,
				$p->{State},
				length($info) > $max_info_length
					? substr($info, 0, int($max_info_length / 2)) . ' ... ' . substr($info, - int($max_info_length / 2))
					: $info
			);

			## Is it critical?
			# Even if it's critical, if there are no locked queries, let it run and send a warning
			# unless kill_slow == 0 (kill any slow queries)
			my $kill_slow_time = defined($db->{kill_slow_time}) ? $db->{kill_slow_time} : $crit_query_time;

			if ($time < $kill_slow_time
				|| ($lock_count == 0 && (defined($db->{kill_slow}) && $db->{kill_slow} > 0))
			) {

				## No. Send a warning
				push @{$RESULT{'WARNING'}}, $msg_line;
                $msg_line =~ s/\n//g;
                log_syslog($db->{name}, "slow_queries", "WARNING $msg_line");

				print "W" if $debug;
			}
			else {

				## Yes. Send alert and try to kill it

				# If this is the top query and it's SELECT, kill it!
				if (
					$count == 1
					&& $info =~ /^SELECT/i  # Select queries only
					&& !defined($locked{$pid})  # Do not kill locked queries
					&& (defined($db->{kill_slow})
						&& $lock_count >= $db->{kill_slow})  # Locked queries are >= threshold
					&& (defined($db->{kill_slow_host})
						&& grep { $slow_host =~ /^$_/ } @{$db->{kill_slow_host}})  # Slow host
				) {

					if (!$KILL_SLOW) {
						$msg_line = "*** KILL $pid *** " . $msg_line;
                        log_syslog($db->{name}, "check_processlist", "SLOW QUERY NOT KILLED: " . trim_huge($info));
                        #sendEmail($db->{name}, "SLOW QUERY NOT KILLED", %{$p});
					}
					else {
						$msg_line = "*** $pid KILLED *** " . $msg_line;
						$dbh->do("KILL $pid") if $KILL_SLOW;
                        log_syslog($db->{name}, "check_processlist", "SLOW QUERY KILLED: " . trim_huge($info));
                        sendEmail($db->{name}, "SLOW QUERY KILLED", %{$p});
					}
                }

				push @{$RESULT{'WARNING'}}, $msg_line;
                $msg_line =~ s/\n//g;
                log_syslog($db->{name}, "slow_queries", "WARNING $msg_line");
				print "W" if $debug;
			}
		}

		# Add more locked queries if we haven't reached max_queries
		if ($slow_count < $max_queries && $lock_count) {
			## Format processlist
			my $plist = format_processlist($filler_queries, $max_queries - $slow_count);

			## Send warning message
			push @{$RESULT{'WARNING'}}, $plist if $plist;
            log_syslog($db->{name}, "slow_queries", "WARNING $plist") if $plist;

		}

		# Add header
		$header .= sprintf(
			"Slow: %d Worst: %s (%s) Locked: %d",
			$slow_count, secs_to_time($worst_time), secs_to_time($crit_query_time), $lock_count
		);

		# Add Instance
		$header = $instance . $header;

		if (defined($RESULT{'CRITICAL'})) {
			unshift @{$RESULT{'CRITICAL'}}, $header;
            log_syslog($db->{name}, "slow_queries", "CRITICAL $header");
		}
		else {
			unshift @{$RESULT{'WARNING'}}, $header;
            log_syslog($db->{name}, "slow_queries", "WARNING $header");
		}

	}

	return ( exists($RESULT{'CRITICAL'}) || exists($RESULT{'WARNING'}) ) ? 0 : 1;
}

sub get_processlist {

	my $dbh = shift;
	my %args = @_;

	my $pl = $dbh->selectall_hashref("SHOW FULL PROCESSLIST", 'Id');

#		next if $p->{Command} =~ /^(Sleep|Binlog Dump|Delayed insert)$/;
#		next if $p->{User} eq 'system user';

	foreach my $key (keys %args) {

		my ($sign, $col) = $key =~ /^(-)?(.+)$/;

		$sign ||= '+';

		foreach my $pid (keys %$pl) {
			if ($sign eq '-') {
				delete $pl->{$pid} if $pl->{$pid}{$col} =~ /^$args{$key}$/;
			}
			else {
				delete $pl->{$pid} if $pl->{$pid}{$col} !~ /^$args{$key}$/;
			}
		}			
	}

	return $pl;
}

sub format_processlist {

	my $plist = shift;
	my $max_queries = shift;

	my @buffer;
	my $count = 0;
	my $total = scalar keys %$plist;

	return '' if $total == 0;

	foreach my $pid (
		sort {
			$plist->{$b}{Time} <=> $plist->{$a}{Time}
			|| (defined($plist->{$b}{Status})
				&& ($plist->{$b}{Status} ne 'Locked'
					|| $plist->{$b}{Status} ne 'Waiting for table level lock')
				)
		}
		grep { defined($plist->{$_}{Time}) } keys %$plist) {

		my $p = $plist->{$pid};

		next if $p->{Command} =~ /^(Sleep|Binlog Dump|Delayed insert)$/;
#		next if $p->{User} eq 'system user';

		my $host = $p->{Host};
		$host =~ s/^([^.:]+).*?$/$1/;

		my $info = defined($p->{Info}) ? no_ctrl_char($p->{Info}) : '';
		$info =~ s/\n/ /g;
		$info =~ s/\s+/ /g;

		next if $info eq 'SHOW FULL PROCESSLIST';

		my $state = defined($p->{State}) ? $p->{State} : '';

#print Dumper($p);

		push @buffer, sprintf(
			"ID: %d Time: %s Host: %s State: [%s] Info: %s",
			$pid,
			secs_to_time($p->{Time}),
			$host,
			$state,
			length($info) > $max_info_length
				? substr($info, 0, int($max_info_length / 2)) . ' ... ' . substr($info, - int($max_info_length / 2))
				: $info
		);

		$count++;

		if (defined($max_queries) && $count > $max_queries) {
			push @buffer, sprintf('Skipping next %d', $total - $count + 1);
			last;
		}
	}

	return join("\n---\n", @buffer);
}

sub get_running_queries {

	my $db = shift;
	my $collect_seconds = shift || 5;

	my $db_host = $db->{host} || $db->{name};
	my $db_port = $db->{port} || 3306;

	## Collect processlist profile information
	require FileHandle;
	my $mk_processlist_cmd = sprintf(
		'perl /usr/local/nagios/libexec/maatkit/mk-query-digest' .
		' --report-histogram none --nofor-explain --run-time %ds' .
		' --limit 100%% --processlist h=%s --port %d --user %s --password %s',
		$collect_seconds, $db_host, $db_port, $DB_USER, $DB_PASS
	);

	print "\nCMD: $mk_processlist_cmd\n" if $debug;

	my $fh = FileHandle->new("$mk_processlist_cmd |");
	defined($fh) or die "Failed to open $mk_processlist_cmd: $!";
	$fh->autoflush(1);
	my $output;
	while(<$fh>) {
		$output .= $_;
	}
	$fh->close();

	return $output;
}
## ---------------------------------------------------------------

## TABLES --------------------------------------------------------
sub check_tables {

	my $db = shift;

	## Disabled - too slow ------

	push @{$RESULT{'OK'}}, "Disabled";
    log_syslog($db->{'name'}, "check_tables", "OK (Disabled)");
	return 1;

	## --------------------------

	my $dbh = $db->{dbh};

	my $instance = $db->{'name'};

	## Check tables

	my $databases = $dbh->selectcol_arrayref("SHOW DATABASES");

	my @full_tables;

	foreach my $database (@$databases) {

		my $status = $dbh->selectall_hashref("SHOW TABLE STATUS FROM `$database`", 'Name');

		foreach my $table (keys %$status) {

			my $table_status = $status->{$table};

			my $max_data_length = $table_status->{'Max_data_length'};
			my $data_length     = $table_status->{'Data_length'};

			## Ignore tables w/o Max_data_length (i.e. MERGE tables)
			next if not defined($max_data_length);
			next if $max_data_length == 0;

			my $table_full = int(($data_length / $max_data_length) * 100);

			if ($table_full >= $full_table_threshold) {

				## Do not include archived tables
				next if $table =~ /_\d+$/;

				## Maybe it should ignore tables that were not updated for certain time
				## Todo.

				## Push bad apples
				push @full_tables, "$database.$table ($table_full%)";
			}
		}
	}

	if (@full_tables) {
		push @{$RESULT{'WARNING'}}, $instance . "Full table(s): " . join(', ', @full_tables);
        log_syslog($db->{'name'}, "check_tables", "Full table(s): " . join(', ', @full_tables));
		print "WARNING" if $debug;
		return 0;
	}

    log_syslog($db->{'name'}, "check_tables", "OK");
	return 1;
}

## ---------------------------------------------------------------


## GRANTS --------------------------------------------------------

sub check_grants {

	my $db = shift;

	my $noc_db_user = 'check_db';
	my $noc_db_pass = 'oH7F3ToCNqPv63K';

	my $dbh = $db->{dbh};
	my $noc_dbh = DBI->connect("dbi:mysql:host=127.0.0.1", $noc_db_user, $noc_db_pass);

	my $users = $dbh->selectcol_arrayref(q~SELECT DISTINCT CONCAT("'",user,"'@'",host,"'") FROM mysql.user ORDER BY user, host~);
	
	my @db_grants;

	foreach my $user (@$users) {

#		print "user: $user\n";
		my $grants;
		eval {
			$grants = $dbh->selectcol_arrayref("SHOW GRANTS FOR $user");
		};
		next unless $grants;

		push @db_grants, @$grants;
	}

	my $db_grants_text = join("\n", @db_grants);

	print "\n$db_grants_text\n" if $debug;

	my ($old_grants) = $noc_dbh->selectrow_array("SELECT grants FROM mysql_grants.grants_data WHERE instance = '$db->{name}'");

	if (!$old_grants) {
		my $grants_insert = $noc_dbh->prepare("
			INSERT
				mysql_grants.grants_data
			SET
				instance = ?,
				grants = ?
		");

		$grants_insert->execute($db->{name}, $db_grants_text);
	}
	else {

		use Digest::MD5 qw(md5_hex);

		my $old_md5 = md5_hex($old_grants);
		my $new_md5 = md5_hex($db_grants_text);

		printf("OLD: %s\nNEW: %s\n", $old_md5, $new_md5) if $debug;

		if ($old_md5 ne $new_md5) {

			my $grants_update = $noc_dbh->prepare("
				UPDATE
					mysql_grants.grants_data
				SET
					grants = ?
				WHERE
					instance = ?
			");

			$grants_update->execute($db_grants_text, $db->{name});

			my $diff = diff($old_grants, $db_grants_text);

			# Sanitize output
			$diff =~ s/PASSWORD '.+'/PASSWORD '***'/g;

			push @{$RESULT{'WARNING'}}, $db->{name} . "Database grants change detected!\n" . $diff;
			print "WARNING" if $debug;
            log_syslog($db->{name}, "check_grants",
                       "WARNING Database grants change detected!");
			return 0;
		}
	}

    log_syslog($db->{name}, "check_grants", "OK");
	return 1;
}

sub diff {

	my $str1 = shift;
	my $str2 = shift;

	my $file1 = sprintf("/tmp/1_%s.txt", md5_hex($str1));
	my $file2 = sprintf("/tmp/2_%s.txt", md5_hex($str2));

	open(my $fh1, ">$file1") or die "Can't open file $file1 for writing!";
	print $fh1 $str1;
	close($fh1);

	open(my $fh2, ">$file2") or die "Can't open file $file2 for writing!";;
	print $fh2 $str2;
	close($fh2);

	my $diff = `/usr/bin/diff -u $file1 $file2`;

	unlink($file1, $file2);

	return $diff;
}

## ---------------------------------------------------------------


sub my_die {

	my $state   = shift || 'OK';
	my $message = shift || '';

	if ($message) {
		$message = $state . ': '. $message if $state eq 'OK';
	}
	else { 
		$message = $state;
	}

	print "$message\n";
    closelog() if ($syslog);

    unlink($lockfile);

	exit $ERRORS{$state};
}

sub secs_to_time {
	my ( $secs, $fmt ) = @_;
	$secs ||= 0;
	
	# If the inbound value has a decimal point, then format the seconds with milliseconds.
	my $hires = $secs =~ m/\./ ? '%06.3f' : '%02d';
	
	if ( !$secs ) {
		return sprintf("00:$hires", $secs);
	}
	
	# Decide what format to use, if not given
	$fmt ||= $secs >= 86_400 ? 'd'
		   : $secs >= 3_600  ? 'h'
		   :                   'm';
	
	return
		$fmt eq 'd' ? sprintf(
			"%d+%02d:%02d:$hires",
			int($secs / 86_400),
			int(($secs % 86_400) / 3_600),
			int(($secs % 3_600) / 60),
			$secs % 60 + ($secs - int($secs)))
		: $fmt eq 'h' ? sprintf(
			"%02d:%02d:$hires",
			int(($secs % 86_400) / 3_600),
			int(($secs % 3_600) / 60),
			$secs % 60 + ($secs - int($secs)))
		: sprintf(
			"%02d:$hires",
			int(($secs % 3_600) / 60),
			$secs % 60 + ($secs - int($secs)));
}

sub no_ctrl_char {

	my $text = shift;

	return '' unless defined $text;

	$text =~ s/
		("(?:(?!(?<!\\)").)*"
		|'(?:(?!(?<!\\)').)*')
		/$1 =~ m#[^\040-\176]# ? "[BINARY]" : $1/egx; #"

	return $text;
}

sub sortRank {
    my $word = shift;
    if ($word eq 'User') {
        return '0';
    } elsif ($word eq 'Host') {
        return '00';
    } elsif ($word eq 'db') {
        return '000';
    } elsif ($word eq 'Time') {
        return '0000';
    } elsif ($word eq 'Id') {
        return 'zzzzzzzzzzzzzz';
    } else {
        return $word;
    }
}

sub trim_huge {
    my $string = shift;

    if (length($string) > $max_info_length_huge) {
        $string = substr($string, 0, int($max_info_length_huge / 2)) . 
            " <SNIP> " . 
            substr($string, - int($max_info_length_huge / 2));
    }

    return $string;
}

sub sendEmail {
    my $instance = shift;
    my $issue = shift;
    my %hash = @_;

    return if (! $sendEmail);
    print "Sending email to [$emailAddress]\n" if $debug;

    my $hostname = `hostname -f`;
    chop $hostname;

    my ($month, $day, $hour, $min, $sec) = (localtime)[4,3,2,1,0];
    my $date_string = sprintf("%s %02d %02d:%02d:%02d",
                              $months[$month],$day,$hour,$min,$sec);

    my $html = <<END_HTML;
<p><strong><font size="5">$program_name encountered an issue with mysql instance "$instance"</font></strong>

<p>
<p>
<table width='600px'>
  <tr>
    <th width='180px' class='even'>Notification Type:</th>
    <td bgcolor="#FF8080">PROBLEM</td>
  </tr>
  <tr>
    <th class="odd">Service Name:</th>
    <td>MySQL Database</td>
  </tr>
  <tr>
    <th class='even'>Monitoring Hostname:</th><td class='even'>$hostname</td>
  </tr>
  <tr>
    <th class='odd'>Date:</th><td class='odd'>$date_string</td>
  </tr>
  <tr>
    <th class='even'>Full Monitor Program:</th><td class='even'>$0</td>
  </tr>
  <tr>
    <th class='odd'>MySQL Instance:</th><td class='odd'>$instance</td>
  </tr>
  <tr>
    <th class='even'>Issue:</th><td bgcolor="#FF8080">$issue</td>
  </tr>
END_HTML

    my $i = 0;
    my $class;
    $hash{'Info'} = trim_huge($hash{'Info'}) if (defined $hash{'Info'});
	foreach my $key (sort {sortRank($a) cmp sortRank($b)} keys %hash) {
        $class = (++$i % 2) ? 'odd' : 'even';
        $html .= "  <tr>\n";
        $html .= "    <th class='$class'>$key:</th>\n";
        $html .= "    <td class='$class'><pre>" . encode_entities($hash{$key}) . "</pre</td>\n";
        $html .= "  </tr>\n";
    }
  
    my %mail = (
        from => 'mysql_dba@liquidityservices.com',
        to => $emailAddress,
        subject => "PROBLEM alert - Database on $instance is CRITICAL",
        'content-type' => 'text/html; charset="iso-8859-1"',
        );

    $mail{body} = <<END_OF_BODY;
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><style type="text/css">body {text-align: center; font-family: Verdana, sans-serif; font-size: 10pt;}
img.logo {float: left; margin: 10px 10px 10px; vertical-align: middle}
span {font-family: Verdana, sans-serif; font-size: 12pt;}
table {text-align:center; margin-left: auto; margin-right: auto;}
th {white-space: nowrap;}
th.even {background-color: #D9D9D9;}
td.even {background-color: #F2F2F2;}
th.odd {background-color: #F2F2F2;}
td.odd {background-color: #FFFFFF;}
th,td {font-family: Verdana, sans-serif; font-size: 10pt; text-align:left;}
</style></head><body>

$html

</html>
END_OF_BODY

    sendmail(%mail) || print "Error: $Mail::Sendmail::error\n";
}
    
