#!/usr/bin/perl

use strict;
use DBI;
use Date::Manip;
use Getopt::Long;
use Mail::Sendmail;

my $dbUser = 'pmiller';
my $dbPwd  = 'XXXX';
my @dbHosts = ();
my @dbHostsDefault = qw(
    test-galeradb01.test.liquidation.com
    test-galeradb02.test.liquidation.com
    test-lrayopsdb01.test.liquidation.com
    stage-lraydb01.test.liquidation.com
    stage-lraydb02.test.liquidation.com
    stage-lraydb03.test.liquidation.com
    stage-lrayopsdb01.test.liquidation.com
);



my $emailAddress = 'Paul.Miller@liquidityservices.com,Mihail.Manolov@liquidityservices.com';
my $sendEmail = 0;
my $debug = 0;
my $help;

my $max_Seconds_Behind_Master = 300;
GetOptions (
    'dbUser=s'   => \$dbUser,
    'dbPwd=s'    => \$dbPwd,
    'dbHost=s'   => \@dbHosts,
    'emailAddress=s' => \$emailAddress,
    'sendEmail'      => \$sendEmail,
    'debug=i'        => \$debug,
    'help'           => \$help,
    );

sub usage {
    print <<__EOD__;
usage $0

  Checks that mysql is up and running on the give dbHosts

  --dbUser     defaults to [$dbUser]
  --dbPwd      defaults to [$dbPwd]
  --dbHost     defaults to 
               [@dbHostsDefault]
               host can be passed via 
               --dbHost db1,db2,db3 or --dbHost db1 --dbHost db2 --dbHost db3

  --sendEmail     if set sends an email to [$emailAddress]
  --emailAddress  defaults to [$emailAddress]
  --debug         value 1-10
  --help          this page
__EOD__

    exit;
}
&usage if $help;

 
if (@dbHosts) {
    @dbHosts = split(/\s*,\s*/,join(',',@dbHosts));
} else {
    @dbHosts = @dbHostsDefault;
}

my ($error_message, @error_message);
my ($dbh);
foreach my $dbHost (@dbHosts) {
    print "connecting to $dbHost\n" if ($debug);
    $dbh = DBI->connect("DBI:mysql:mysql:$dbHost:3306,mysql_connect_timeout=10", $dbUser, $dbPwd);
    if (! defined $dbh) {
        $error_message = "dbHost :$dbHost\n";
        $error_message .= "can not connect to mysql:mysql:$dbHost:3306 as user $dbUser\n";
        push(@error_message, $error_message);
        print STDERR $error_message;
        next;
    }

    my $wsrep_cluster_status = &fetchDbRow($dbh, 
                                           "show status like 'wsrep_cluster_status'", 
                                           'value');
    print "  wsrep_cluster_status [$wsrep_cluster_status]\n" if ($debug);
    if (defined $wsrep_cluster_status) {
        if ($wsrep_cluster_status ne 'Primary') {
            $error_message = "dbHost :$dbHost\n";
            $error_message .= "mysql status wsrep_cluster_status is not Primary [$wsrep_cluster_status]\n\n";
            push(@error_message, $error_message);
            print STDERR $error_message;
            next;
        }
    } else {
        print "  wsrep_cluster_status is not defined\n" if $debug;
    }

    my $slave_io_running = &fetchDbRow($dbh, 'show slave status',
                                       'slave_io_running');
    print "  slave_io_running [$slave_io_running]\n" if ($debug);
    my $seconds_behind_master = &fetchDbRow($dbh, 'show slave status',
                                            'seconds_behind_master');
    print "  seconds_behind_master [$seconds_behind_master]\n" if ($debug);
    if (defined $slave_io_running) {
        if (!defined $seconds_behind_master) {
            $error_message = "dbHost :$dbHost\n";
            $error_message .= "mysql seconds_behind_master is NULL\n";
            push(@error_message, $error_message);
            print STDERR $error_message;
            next;
        }

        if ($seconds_behind_master > $max_Seconds_Behind_Master) {
            $error_message = "dbHost :$dbHost\n";
            $error_message .= "mysql seconds_behind_master > $max_Seconds_Behind_Master [$seconds_behind_master]\n";
            push(@error_message, $error_message);
            print STDERR $error_message;
            next;
        }
    } else {
        print "  seconds_behind_master is not defined\n" if $debug;
    }

    $dbh->disconnect() || die 'Cannot disconnect: ' . $dbh->errstr;
    print "\n" if $debug;
}

print "Number of errors : " . ($#error_message  + 1) . "\n" if ($debug);

if ($sendEmail && ($#error_message > -1)) {
    print "sending email\n" if ($debug);

    chomp(my $hostname = `hostname -f`);
    my $email_message = "Error message from script $0\n";
    $email_message .= "  from : $hostname\n";
    $email_message .= "  at   : " . localtime() . "\n\n";

    my $cnt = 1;
    foreach (@error_message) {
        $email_message .= "$cnt)\n";
        $email_message .= "=======\n";
        $email_message .= $_;
        $cnt++;
    }

    my %mail = ( To       =>  $emailAddress,
                 From     =>  'dba@lqdt.com',
                 Subject  =>  "script $0 reports an error",
                 Message  =>  $email_message,
        );
        
    sendmail(%mail) or die "Can't send mail: " . $Mail::Sendmail::error;
}



    
sub fetchDbRow {
    my $dbh = shift;
    my $sql = shift;
    my $index = shift;

    $index = 0 if (! defined $index);
    my $returnValue;

    print "  sql [$sql]\n" if ($debug > 1);
    my $sth = $dbh->prepare($sql) or die "preparing: ", $dbh->errstr;
    $sth->execute() or die "executing: ", $dbh->errstr;

    my %row;
    $sth->bind_columns( \( @row{ @{$sth->{NAME_lc} } } ));
 
    while ($sth->fetch) {
        $returnValue = $row{$index};

#        foreach my $col (@{$sth->{NAME_lc}}) {
#            print "TEST $col : [$row{$col}]\n";
#        }
    }
    return $returnValue;
}



    
