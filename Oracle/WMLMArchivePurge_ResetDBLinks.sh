#!/bin/bash

v_wm_user=$1
v_wm_pass=$2
v_wm_db=$3
v_arch_user=$4
v_arch_pass=$5
v_arch_db=$6

vlogf=${0}_`date +%Y%m%d_%H%M%S`.log

function ShowCopyright {
    echo "-------------------------------------------------------------------------"
    echo "Copyright © 2013 Manhattan Associates, Inc.  All Rights Reserved."
    echo "Confidential, Proprietary and Trade Secrets Notice"
    echo ""
    echo "Use of this software is governed by a license agreement. This software"
    echo "contains confidential, proprietary and trade secret information of"
    echo "Manhattan Associates, Inc. and is protected under United States and"
    echo "international copyright and other intellectual property laws. Use, disclosure,"
    echo "reproduction, modification, distribution, or storage in a retrieval system in"
    echo "any form or by any means is prohibited without the prior express written"
    echo "permission of Manhattan Associates, Inc."
    echo ""
    echo "Manhattan Associates, Inc."
    echo "2300 Windy Ridge Parkway, 10th Floor"
    echo "Atlanta, GA 30339 USA"
    echo "-------------------------------------------------------------------------"
}

function ShowUsage {
    echo "============================================================"
    echo "                 WAREHOUSE MANAGEMENT SYSTEM"
    echo "            WM-LM Archive/Purge Re-Link Utility"
    echo "         ---------------------------------------------"
    echo ""
    echo "    INSTRUCTIONS:"
    echo ""
    echo "    Use this script to reset the database link in the WM schema"
    echo "    that is required for the WMLM archive/purge component."
    echo ""
    echo "    PLATFORM: Oracle"
    echo ""
    echo "    PRE-REQUISITES: WM Product Schema with Archive/Purge installed,"
    echo "               a valid archive schema and Oracle connection details."
    echo ""
    echo "    Usage:"
    echo "        $0 <wm_user> <wm_pswd> <wm_oracle_sid> <arch_user> <arch_pswd> <arch_oracle_sid>"
    echo ""
    echo "        Parameters"
    echo "        ----------"
    echo "        wm_user         => the WM schema name"
    echo "        wm_pswd         => the WM schema password"
    echo "        wm_oracle_sid   => the WM schema database name"
    echo "        arch_user       => the Archive schema name"
    echo "        arch_pswd       => the Archive schema password"
    echo "        arch_oracle_sid => the Archive schema database name"
    echo ""
    echo ""
    echo "            []$ ./WMLMArchivePurge_ResetDBLinks.sh"
    echo ""
    echo "                     ~ Ctrl+C to Quit ~"
    echo "============================================================"
    echo ""
}

function CreateDBMaxVersionView {
    lvSchemaName=$1
    lvSchemaPswd=$2
    orasid=$3

    echo "create or replace view vw_rstlink_db_max_version as \
          select version_label \
              ,regexp_substr(version_label,'[^_]+',1,1) layer \
              ,to_number(regexp_substr(fullver,'[^.]+',1,1)) major \
              ,to_number(regexp_substr(fullver,'[^.]+',1,2)) minor \
              ,to_number(regexp_replace(regexp_substr(fullver,'[^.]+',1,3),'[A-Za-z]*','')*1) rev1 \
              ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,4),'0')) rev2 \
              ,to_number(coalesce(regexp_substr(fullver,'[^.]+',1,5),'0')) rev3 \
              ,start_dttm \
          from (select version_label \
                      ,regexp_substr(version_label,'[^_]+',1,2) fullver \
                      ,start_dttm \
                from db_build_history \
                where version_label like 'LMARCH%' \
                or (version_label like 'WMARCH%'))  \
          order by 2 desc,3 desc,4 desc,5 desc NULLS LAST,6 desc NULLS LAST,7 desc nulls last, 1 desc;" | sqlplus -s $lvSchemaName/$lvSchemaPswd@$orasid >/dev/null
}

function DropDBMaxVersionView {
    lvSchemaName=$1
    lvSchemaPswd=$2
    orasid=$3
    echo "drop view vw_rstlink_db_max_version;" | sqlplus -s $lvSchemaName/$lvSchemaPswd@$orasid >/dev/null
}

ShowCopyright | tee $vlogf

if [ $# -ne 6 ]; then
    ShowUsage  | tee -a $vlogf

else
    # check the logins
    echo "Checking the WM schema connection for ${v_wm_user} on ${v_wm_db} ..." | tee -a $vlogf
    vloginchk=`sqlplus -L -s $v_wm_user/$v_wm_pass@$v_wm_db<<!
        set echo off feedback off pages 0 trims on
        select '1' from dual;
        quit
!`

    echo "Checking the Archive schema connection for ${v_arch_user} on ${v_arch_db} ..." | tee -a $vlogf
    vloginchka=`sqlplus -L -s $v_arch_user/$v_arch_pass@$v_arch_db<<!
        set echo off feedback off pages 0 trims on
        select '1' from dual;
        quit
!`
    if [[ "$vloginchk" != "1" ]]; then
        echo "[ERROR]: Login Check Failed for ${v_wm_user}.  Check the password and try again." | tee -a $vlogf
        exit 1
    elif [[ "$vloginchka" != "1" ]]; then
        echo "[ERROR]: Login Check Failed for ${v_arch_user}.  Check the password and try again." | tee -a $vlogf
        exit 1
    fi

    CreateDBMaxVersionView $v_wm_user $v_wm_pass $v_wm_db
    
    echo "Checking the archive/purge version ..." | tee -a $vlogf
    # get db link name to use
    v_db_link_name=`sqlplus -L -s $v_wm_user/$v_wm_pass@$v_wm_db<<!
              set echo off feedback off trims on pages 0
              select decode(max(major),3,'WMSARCH_DBLINK','WMSADMIN_DBLINK') link_name
              from VW_RSTLINK_DB_MAX_VERSION
              where layer='WMARCHIVE' group by layer;
              quit;
!`
   

    case $v_db_link_name in
        'WMSARCH_DBLINK'|'WMSADMIN_DBLINK' )
            echo "[OK]" | tee -a $vlogf
            echo "Using database link $v_db_link_name ..." | tee -a $vlogf

            # drop the database link, create it, and update the archive synonyms
            sqlplus -L -s $v_wm_user/$v_wm_pass@$v_wm_db>>$vlogf<<!
                set echo on
                drop database link ${v_db_link_name};

                create database link ${v_db_link_name} connect to ${v_arch_user} identified by $v_arch_pass using '${v_arch_db}';

                -- update the archive synonyms
                set serveroutput on
                declare
                  cursor cArchSyn is
                    select 'create or replace synonym '||synonym_name||' for '||synonym_name||'@${v_db_link_name}' ddl_stmt
                    from user_synonyms
                    where synonym_name like 'ARCH%';
                begin
                  for crec in cArchSyn loop
                      dbms_output.put_line('Executing --> '||crec.ddl_stmt);
                      execute immediate crec.ddl_stmt;
                  end loop;
                end;
                /
                quit;
!
            echo "Reset DBLink Complete." | tee -a $vlogf
            echo "See $vlogf for details."
            ;;
        * )
            echo "[ERROR]:  Failed to detect the archive/purge version and database link name." | tee -a $vlogf
            echo "          Action" | tee -a $vlogf
            echo "          ------" | tee -a $vlogf
            echo "          - Confirm that archive/purge is installed in the WM schema." | tee -a $vlogf
            echo "          - Confirm that the correct WM schema name is being used." | tee -a $vlogf
            echo "" | tee -a $vlogf
            echo "          If the problem persists, contact DBASupport." | tee -a $vlogf
            echo "" | tee -a $vlogf
            exit 1
            ;;
    esac
    
    DropDBMaxVersionView $v_wm_user $v_wm_pass $v_wm_db
fi

