#!/bin/sh
#===============================================================================================================
#       Name    :  execArchivePurge.sh
#       Author  :  Michael Jesko
#       Date    :  Thursday, June 18, 2009
#       Purpose :  Execute procedure wm_archive_pkg.ui_purge() to start a WM archive/purge process.
#       Notes   :  This script is written for and intended to be executed from the database server hosting the
#               :  target schema being purged and optionally archived.
#               :
#       Usage   :  []$ sh execArchivePurge.sh <username> <password> <oracle_sid> <purge_config_id>
#               :
#               :  Note - Purge_config_id equals the value of column purge_config_id from table purge_config
#                         than corresponds to the archive/purge job which needs to be run.
#
#      Version  :  3.0.6a
#      Platform :  AIX/Linux
#      DB Version:  Oracle 10g, 11g
#      Revision History/Notes:
#
#===============================================================================================================

runtime=`date +%Y%m%d_%H%M%S%Z`

if [[ $# -ne 5 ]]; then
    echo "" 
    echo "--> Purge userid, password, oracle sid, and purge code are required."
    echo "    Usage: []$ . execArchivePurge.sh <username> <password> <oracle_sid> <purge_config_id> <logdirpath>"
    echo "" 

else
    v_username=$1
    v_userpass=$2
    v_orasid=$3
    v_purge_cfg_id=$4
    v_logdir=$5

    # if the archive_purge log directory doesn't exist in the home directory, create it now
    if [ -d ${v_logdir} ]; then
        v_logf=${v_logdir}/execArchivePurge.log
    else
        echo "ERROR (execArchivePurge.sh): Log directory ${v_logdir} not found. Unable to continue."
        exit 1
    fi

    echo "" >>  $v_logf
    echo "===============================================================" >>  $v_logf
    echo "                    Manhattan Associates" >>  $v_logf
    echo "                 WAREHOUSE MANAGEMENT SYSTEM" >>  $v_logf
    echo "                    Oracle Archive/Purge" >>  $v_logf
    echo "         ---------------------------------------------" >>  $v_logf
    echo "" >>  $v_logf
    echo "         Run Date/Time:   ${runtime}" >>  $v_logf
    echo "         Purge Config ID: ${v_purge_cfg_id}" >>  $v_logf
    echo "         Oracle SID:      ${v_orasid}" >>  $v_logf
    echo "         Oracle Schema:   ${v_username}" >>  $v_logf
    echo "" >>  $v_logf
    echo "===============================================================" >>  $v_logf
    echo "" >>  $v_logf
    echo "" >>  $v_logf
    echo "---------------------------------------------------------------" >>  $v_logf
    
    archrslt=`sqlplus -L -s $v_username/$v_userpass@${v_orasid}<<EOF
set serveroutput on feedback off heading off verify off
declare
begin
    dbms_output.put_line('Starting the Archive/Purge Process ...');
    wm_archive_pkg.ui_purge('${v_purge_cfg_id}');
end;
/
quit;
EOF`

    echo "${archrslt}">>  $v_logf
    echo "Done." >>  $v_logf
    endtime=`date +%Y%m%d_%H%M%S%Z`
    echo "Archive/Purge activity completed at ${endtime}" >> $v_logf
    echo "---------------------------------------------------------------" >>  $v_logf
fi

