#!/bin/ksh
#!/bin/ksh
#Name: schema_level_datapump.ksh
#Purpose: This script takes a logical dump of the specified oracle schema. No object is excluded. 
#               If you need to exclude any type of objects
#               you must use a command line custom expdp command.
#Date:  8/15/2012
#Author: Jagan Reddy
#Revision information:
#
###############################################################################################

set_DATA_PUMP_DIR () {

export DATA_PUMP_DIR=`sqlplus -s -n / as sysdba <<EOF
set heading off;
select directory_path from dba_directories where directory_name = 'DATA_PUMP_DIR';
exit;
EOF`

echo -e "in set_DATA_PUMP_DIR: data_pump_dir=${DATA_PUMP_DIR}"

if [ X"${DATA_PUMP_DIR}" = X"" ]; then
	send_email_alert "Invalid DATA_PUMP_DIR fetched from DB for ${ORACLE_SID}"
	return 1
fi

}

get_password() {
	export PASSWORD=`grep ${USER} ${USERENVFILE}  | awk '{ print $2} '`  #tab or space delimited list expected
} #end get_password

take_dump(){

#	sed s/TIMESTAMP/${timestamp}/g ${EXPORT_STG_WMS_MSF_PARFILE} > ${ORAADMIN}/scripts/eximport/logs/${ORACLE_SID}_WMS_MSF_SCHEMAS.par
	
	ps -ef | grep pmon | grep -i ${ORACLE_SID} > /dev/null
	
	if [ $? -eq 0 ]; then
	  expdp  ${PARFILE_ARGS} dumpfile=${dumpfile} logfile=${logfile}
	else
		print "\n\n`date`\tSorry. The database ${ORACLE_SID} is not running. Can not take full export backup "
		print "`date`\tPlease start database and try again\n\n"
		exit
	fi

	if [  -e ${DATA_PUMP_DIR}/${dumpfile}  ]; then
		return 0
	else
		print "\n\n ERROR: Export did not succeed"
		send_email_alert "${ORACLE_SID}.${SCHEMA} dump failed"
		return 1
	fi
}

send_email_alert() {

message=${1}

mailx -s "DATA Pump export on `hostname`:${0} Failed" `cat ${USERENVDIR}/alerts.email` <<EOF
	${message}
EOF

}

compress_dump() {

	if [ ! -e ${DATA_PUMP_DIR}/${dumpfile} ]; then
		print "Error while taking datapump export .."
		exit
	else
		gzip ${DATA_PUMP_DIR}/${dumpfile}
	fi

}

log_it () {

msg=${1}
export timestamp=`date '+%m%d%Y_%H%M'`
print "${timestamp}\t${msg}" >> ${LOGFILE}

}

cleanup_old_files () {
#remove more than 7 days of datapump files
find ${DATA_PUMP_DIR}  -mtime +2 -exec rm -f {} \;

}

#MAIN 

if [ $# -ne 3 ]; then
	print "\n\n Error. Invalid usage"
	print "You must enter Oracle SID"
	print "Usage ${0} <ORACLE_SID> <SCHEMA> [<filename tag>]\n"
	exit
fi

ORACLE_SID=${1}
SCHEMA=${2}
filename_ext=${3}

running_ORACLE_SID=`ps -ef | grep pmon | grep ${ORACLE_SID} | awk -F'_' '{ print $3 }'`

if [ X"${ORACLE_SID}" != X"${running_ORACLE_SID}" ]; then
	print "The specified ORACLE ${ORACLE_SID} is not running on this server"
	exit
fi

DBADMIN=${HOME}/dbadmin
CONFIGDIR=${DBADMIN}/scripts/tools/cron-jobs/config


SCRIPTBASE=${HOME}/oracle_admin_scripts
BINDIR=${SCRIPTBASE}/bin
SQLDIR=${SCRIPTBASE}/sql
CONFIGDIR=${SCRIPTBASE}/config
TMPDIR=${SCRIPTBASE}/tmp
LOGDIR=${SCRIPTBASE}/logs
USERENVDIR=${HOME}/config
USERENVFILE=${USERENVDIR}/.userEnv


. ${HOME}/${ORACLE_SID}.env
#. ${CONFIGDIR}/system.env

export DEBUG=0

export DATA_PUMP_DIR=""

set_DATA_PUMP_DIR #logs into the ORACLE_SID and sets the DATA_PUMP_DIR value


USER=SYSTEM
PASSWORD=""

get_password "${USER}"

if [ X"${PASSWORD}" = X"" ]; then
	message="Invalid PASSWORD obtained for ${USER} for ${ORACLE_SID}. Can not continue data pump operation. "
	echo $message
	send_email_alert ${message}
	exit 1
fi
if [ $DEBUG -eq 1 ] {
	echo -e "USER: ${USER} \t password: ${PASSWORD} \t data_pump_dir=${DATA_PUMP_DIR}"
}


PARFILE_ARGS=" \
	USERID=system/LSI0ranges@${ORACLE_SID} \
	SCHEMAS=${SCHEMA} \
	flashback_time=systimestamp \
	DIRECTORY=DATA_PUMP_DIR \
"

export timestamp=`date '+%m%d%Y_%H%M'`
export dumpfile=${ORACLE_SID}_${SCHEMA}_${filename_ext}_${timestamp}.dmp 
export logfile=${ORACLE_SID}_${SCHEMA}_${filename_ext}_${timestamp}.log

LOGFILE=${LOGDIR}/${ORACLE_SID}_${SCHEMA}_${timestamp}_dpump.log
log_it "BEGIN datapump export of ${ORACLE_SID} all schemas"
take_dump;
log_it "END datapump export of ${ORACLE_SID} all schemas"
log_it "BEGIN compressing export file"
compress_dump;
log_it "END compressing export file"
log_it "BEGIN Removing old files"
cleanup_old_files
log_it "END Removing old files"
