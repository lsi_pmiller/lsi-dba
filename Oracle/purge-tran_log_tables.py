#!/usr/bin/env python

import argparse
import datetime
import logging
import MySQLdb  # yum install MySQL-python27
import os
import socket
import sys
from timeit import default_timer as timer

# needed for oracle libs
if 'LD_LIBRARY_PATH' not in os.environ:
    os.environ['LD_LIBRARY_PATH'] = '/usr/lib/oracle/12.1/client64/lib'
    try:
        os.execv(sys.argv[0], sys.argv)
    except Exception, exc:
        print 'Failed re-exec:', exc
        sys.exit(1)

import cx_Oracle

logging.basicConfig(format='%(funcName)-30s - %(levelname)-8s - %(message)s')
log = logging.getLogger()

parser = argparse.ArgumentParser(
    description="purge data from tran_log, tran_log_message and tran_log_response_message data older than X days",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

parser.add_argument(
    "--db_oracle_user",
    help="oracle database user",
    default='WMOS_WMS',
)
parser.add_argument(
    "--db_oracle_password",
    help="oracle database password",
    default='See Password State',
)
parser.add_argument(
    "--db_oracle_host",
    help="oracle database host",
    default="usevsWMSora101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com",
)
parser.add_argument(
    "--db_oracle_port",
    type=int,
    help="oracle database port",
    default=1521,
)
parser.add_argument(
    "--db_oracle_sid",
    help="oracle database sid",
    default='ORCL',
)
parser.add_argument(
    "--days_back",
    help="how many days back to delete",
    default=30,
)
parser.add_argument(
    "--limit",
    help="limit on the delete",
    default=2500,
)
parser.add_argument(
    "--status_db_user",
    help="mysql database user",
    default='inventory',
)
parser.add_argument(
    "--status_db_password",
    help="database password",
    default='See Password State',
)
parser.add_argument(
    "--status_db_schema",
    help="database schema",
    default='inventory',
)
parser.add_argument(
    "--status_db_host",
    help="database host",
    default='usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com',
)
parser.add_argument(
    "--status_db_port",
    type=int,
    help="mysql database port",
    default=3306,
)
parser.add_argument(
    "--logLevel",
    nargs='?',
    help="logging level",
    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    default='INFO'
)


args = parser.parse_args()
log.setLevel(getattr(logging, args.logLevel))


###############
## Job Status
##
def updateJobStatus(start_time, success_cnt):
    log.info("writting to job_status db table")
    conn = getMySQLConnection(args.status_db_host, args.status_db_user,
                              args.status_db_port, args.status_db_password,
                              args.status_db_schema)

    sql="""\
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('{}', '{}', '{}', '{}', {}, {}, '{}', {})\
""".format(os.path.basename(sys.argv[0]),
           os.path.dirname(sys.argv[0]),
           socket.gethostname(),
           'DAILY',
           success_cnt,
           0,
           start_time,
           'now()')
    log.info("sql:[{}]".format(sql))
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()

###############
## MySQL stuff
##
def getMySQLConnection(host, user, port, password, schema):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} schema:{}".format(host,
                                                                           port,
                                                                           user,
                                                                           schema))
        conn = MySQLdb.connect(host=host, port=port,
                               user=user, passwd=password,
                               db=schema)
        log.info("Successfully connected to:{} as user:{} password:{}".format(host,
                                                                              user,
                                                                              password))
    except MySQLdb.Error as e:
        log.error("ERROR connecting to:{}".format(host))
        log.error("EXCEPTION:{}".format(e))
        log.exception("Traceback")
    except:
        log.exception("ERROR connecting to {}".format(host))
        log.exception("Traceback")

    return conn

###############
## Oracle stuff
##
def getOracleConnection(host, port, user, password, sid):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} sid:{}".format(host,
                                                                        port,
                                                                        user,
                                                                        sid))
        connstr = user +'/'+ password +'@'+ host +':'+ str(port) +'/'+ sid
        conn = cx_Oracle.connect(connstr)
        
    except cx_Oracle.DatabaseError as e:
        log.error("ERROR with Oracle connection on:{}".format(host))
        log.error("EXCEPTION:{}".format(e))
        log.exception("Traceback")
        sys.exit(1)
    except:
        log.fatal("ERROR connecting to {0}".format(host))
        log.exception("Traceback")
        sys.exit(1)

    return conn

def purge(sql, conn) :
    log.debug("SQL : {0}".format(sql))
    deleteLoop = 0
    rowsDeleted = 1
    rowsDeletedTotal = 0    
    timerTotal = timer()
    while (rowsDeleted > 0) :
        try:
            timerLoop = timer()
            cursor = conn.cursor()
            cursor.execute(sql)
            rowsDeleted = cursor.rowcount
            rowsDeletedTotal += rowsDeleted
            conn.commit()
            deleteLoop += 1
            log.debug("rows deleted:{} in:{:.2f}s loop:{}".format(rowsDeleted,
                                                                  (timer()-timerLoop),
                                                                  deleteLoop))
        except:
            log.exception("ERROR selecting from {}".format(conn))

    log.info("finished in:{:.2f}s deleted:{} loops:{}".format((timer()-timerTotal),
                                                              rowsDeletedTotal,
                                                              deleteLoop))
    return rowsDeletedTotal
    


def purge_TRAN_LOG_RESPONSE_MESSAGE(conn) :
    log.info("running")
    sql="""
DELETE FROM wmos_wms.tran_log_response_message tlrm
WHERE tlrm.message_id IN
    (SELECT tlrm.message_id
     FROM wmos_wms.tran_log tl
     JOIN wmos_wms.tran_log_response_message tlrm 
          ON tlrm.message_id = tl.message_id
     WHERE tl.created_dttm < CURRENT_DATE - {})
AND ROWNUM < {}""".format(args.days_back, args.limit)
    return purge(sql, conn)



def purge_TRAN_LOG_MESSAGE(conn) :
    log.info("running")
    sql="""
DELETE FROM wmos_wms.tran_log_message tlm
WHERE tlm.tran_log_id IN
    (SELECT tlm.tran_log_id
     FROM wmos_wms.tran_log tl
     JOIN wmos_wms.tran_log_message tlm 
          ON tlm.tran_log_id = tl.tran_log_id
     WHERE tl.created_dttm < CURRENT_DATE - {})
AND ROWNUM < {}""".format(args.days_back, args.limit)
    return purge(sql, conn)


def purge_TRAN_LOG(conn) :
    log.info("running")
    sql="""
DELETE FROM wmos_wms.tran_log tl
WHERE tl.created_dttm < CURRENT_DATE - {}
AND ROWNUM < {}""".format(args.days_back, args.limit)
    return purge(sql, conn)

    
###############
## main
##
start_time = datetime.datetime.now()
oracleConn = getOracleConnection(args.db_oracle_host, args.db_oracle_port,
                                 args.db_oracle_user, args.db_oracle_password,
                                 args.db_oracle_sid)

success_cnt = 0
success_cnt += purge_TRAN_LOG_RESPONSE_MESSAGE(oracleConn)
success_cnt += purge_TRAN_LOG_MESSAGE(oracleConn)
success_cnt += purge_TRAN_LOG(oracleConn)

# if we made it this far assume that the purge happened
# update the job_status db table
updateJobStatus(start_time, success_cnt)
