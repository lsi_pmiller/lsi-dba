#!/bin/sh

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/db/oracle/app/oracle/product/11.2.0/client_1/instantclient:/usr/lib/oracle/12.1/client64/lib
if [ -z "$ORACLE_HOME" ]
then
    export ORACLE_HOME=/usr/lib/oracle/12.1/client64
fi

BIN_PATH=/home/oracle/bin

usage()
{
    echo
    echo "Usage: $0 username password host sid [logdirpath]"
    echo "  Calls execArchivePurge.sh script with a purge_config_id"
    echo
    echo "  file logs to logdirpath/execArchivePurge-DOW.log"
    echo "  database logs to WMOS_WMS.purge_hist and WMOS_WMS.msg_log"
    echo "    select * from WMOS_WMS.msg_log where LOG_DATE_TIME>trunc(sysdate) and MSG like '%Exception%'\G"
    exit
}

updateJobStatus() {
    start_time=$1
    success_cnt=$2

    name=`basename $0`
    path=`dirname $0`
    hostname=`hostname -f`
    frequency="DAILY"

    echo "insert success into job_status"
    mysql --host usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com --user inventory --password='See PWS' --database inventory <<EOF
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('$name', '$path', '$hostname', '$frequency', $success_cnt, 0, '$start_time', now())
EOF
}

if [ $# -lt 4 ]; then
	echo "Invalid usage"
    usage
fi

username=$1
password=$2
server_name=$3
database_name=$4
logdirpath=$5

if [ "$logdirpath" = "" ]
then
    logdirpath=/home/oracle/bin/logs
fi

ORACLE_HEADER='
set heading off
set feedback off
set pages 0
'

check_connection()
{
    USERNAME=$1
    PASSWORD=$2
    SERVER_NAME=$3
    DATABASE_NAME=$4

    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"

    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT 'connected' from dual;
EOF
}


get_purge_config_ids()
{
    USERNAME=$1
    PASSWORD=$2
    SERVER_NAME=$3
    DATABASE_NAME=$4

    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"

    $SQLPLUS <<EOF
$ORACLE_HEADER
select PURGE_CONFIG_ID from WMOS_WMS.PURGE_CONFIG order by PURGE_CONFIG_ID;
EOF
}

############
### main ###
############
START_TIME=`date +'%F %T'`

echo "LOG> START TIME='$START_TIME'"
START_TICKS=`date +%s`

connection=$(check_connection $username $password $server_name $database_name)

if [ "$connection" != "connected" ]
then
    echo "unable to connect to oracle db [sqlplus $username/****@$server_name/$database_name]" >&2
    echo "Exiting" >&2
    usage
fi

if [ ! -x  "$BIN_PATH/execArchivePurge.sh" ]
then
    echo "Error file [$BIN_PATH/execArchivePurge.sh]"
    echo "Does not exist or is not executable"
    echo "Exiting"
    usage
fi
    
   

purge_config_ids=$(get_purge_config_ids $username $password $server_name $database_name)

success_cnt=0
for purge_config_id in $purge_config_ids
do    
    echo "LOG> calling $BIN_PATH/execArchivePurge.sh with purge_config_id: ${purge_config_id} at '"`date +'%F %T'`"'"
    $BIN_PATH/execArchivePurge.sh $username $password ${server_name}/${database_name} $purge_config_id $logdirpath
    ((success_cnt++))
done

DOW=`date +%a`
mv $logdirpath/execArchivePurge.log $logdirpath/execArchivePurge-${DOW}.log

END_TICKS=`date +%s`
TOTAL_TIME=`expr $END_TICKS - $START_TICKS`
echo "LOG> END TIME='"`date +'%F %T'`"' (${TOTAL_TIME}s)"

updateJobStatus "$START_TIME" $success_cnt
