#!/bin/sh

usage()
{
    echo
    echo "Usage: $0 ENV dumpfile [dry_run]"
    echo "  dumpfile requires full path"
    echo "  examples"
    echo "  DEV"
    echo "    DEV00   OOC1/DEV00/QA3"
    echo "      nohup $0 DEV00   /var/tmp/PROD_WMSTOOLS_PROD_\`date +%m%d%Y\`_????.dmp > DEV00_WMSTOOLS_output.txt 2>&1 &"
    echo "    DEV01   TEST1/DEV01/QA1"
    echo "      nohup $0 DEV01  /var/tmp/PROD_WMSTOOLS_PROD_\`date +%m%d%Y\`_????.dmp > DEV01_WMSTOOLS_output.txt 2>&1 &"
    echo "    DEV02   TEST10/DEV02/QA2"
    echo "      nohup $0 DEV02 /var/tmp/PROD_WMSTOOLS_PROD_\`date +%m%d%Y\`_????.dmp > DEV02_WMSTOOLS_output.txt 2>&1 &"
    echo "    STAGE"
    echo "      nohup $0 STAGE  /var/tmp/PROD_WMSTOOLS_PROD_\`date +%m%d%Y\`_????.dmp > STAGE_WMSTOOLS_output.txt 2>&1 &"
    exit
}

if [ $# -lt 2 ]
then
   echo "missing parameters"
   usage
fi

ENV=$1
DUMPFILE_PATH=$2
DRY_RUN=$3

TARGET_SCHEMA_WMSTOOLS="NOT_SET"

if [ "$ENV" == "DEV00" ]
then
    TARGET_SCHEMA_WMSTOOLS="WMSTOOLS"

# TEST1/DEV01/QA1
elif [  "$ENV" == "DEV01" ]
then
    TARGET_SCHEMA_WMSTOOLS="WMSTDEV01"

# TEST10/DEV02/QA2
elif [  "$ENV" == "DEV02" ]
then 
    TARGET_SCHEMA_WMSTOOLS="WMSTOOLSQA1"   

# STAGE
elif [  "$ENV" == "STAGE" ]
then 
    TARGET_SCHEMA_WMSTOOLS="WMSTOOLS_STAGE"   
else    
    echo "unknown ENV [$ENV]"
    usage
fi
    
    
TARGET_SCHEMA_WMSTOOLS_PASSWORD=`grep -w ${TARGET_SCHEMA_WMSTOOLS} $HOME/config/.userEnv | awk '{ print $2 }'`
TARGET_SQLPLUS_WMSTOOLS="sqlplus -S $TARGET_SCHEMA_WMSTOOLS/$TARGET_SCHEMA_WMSTOOLS_PASSWORD"
LOCKFILE=/var/tmp/refresh-${TARGET_SCHEMA_WMSTOOLS}.lck


cat<<EOF
TARGET_SCHEMA_WMSTOOLS : $TARGET_SCHEMA_WMSTOOLS
DUMPFILE_PATH : $DUMPFILE_PATH

EOF


#######
# helper functions

check_and_create_lock_file()
{
    if [ -e $LOCKFILE ]
    then
        echo "Error lockfile [$LOCKFILE] exits."
        echo "A Previous refresh may be still going on or might have failed."
        echo "Verify and remove lockfile if needed."
        exit
    fi
    echo "Pid : $$" > $LOCKFILE
    echo "Start :" `date` >> $LOCKFILE
}

ORACLE_HEADER='
set heading off
set feedback off
set pages 0
'

check_connection()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT 'connected' from dual;
EOF
}

get_connection_count()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT count(*) from v\$session where username = '$SCHEMA';
EOF
}

get_table_count()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
select count(*) from user_tables where table_name not like 'SYS%';
EOF
}

get_DATA_PUMP_DIR()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT directory_path FROM dba_directories WHERE directory_name = 'DATA_PUMP_DIR';
EOF
}

drop_objects()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec MANH_DROP_OBJECTS;
commit;
purge recyclebin; 
commit;
EOF
}

compile_objects()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec dbms_utility.compile_schema('${SCHEMA}');
EOF
}

gather_stats()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec dbms_stats.gather_schema_stats( ownname=>'${SCHEMA}', estimate_percent=> 100, DEGREE=>4, GRANULARITY=>'ALL',CASCADE=>TRUE);
EOF
}

create_grant_readonly_role()
{
    SQLPLUS=$1
    SCHEMA=$2

    $SQLPLUS <<EOF > /dev/null
$ORACLE_HEADER
drop role RO_${SCHEMA};
create role RO_${SCHEMA};

SPOOL /tmp/RO_${SCHEMA}.SQL replace;
SELECT 'GRANT SELECT ON ${SCHEMA}.'||OBJECT_NAME || ' TO RO_${SCHEMA};' FROM
DBA_OBJECTS WHERE OBJECT_TYPE IN('TABLE','VIEW') and owner='${SCHEMA}';
SPOOL off;

@/tmp/RO_${SCHEMA}.SQL;

grant RO_${SCHEMA} to mhuser01;
grant RO_${SCHEMA} to sri01;
grant RO_${SCHEMA} to mahesh01;
grant RO_${SCHEMA} to mo01;
grant RO_${SCHEMA} to noellee;
EOF
}

add_oracle_node_to_chef()
{
    if [ "$ENV" == "DEV00" ]
    then
        CHEF_ENV="qa3"
    elif [  "$ENV" == "DEV01" ]
    then
        CHEF_ENV="qa1"
    elif [  "$ENV" == "DEV02" ]
    then 
        CHEF_ENV="qa2"
    elif [  "$ENV" == "STAGE" ]
    then 
        CHEF_ENV="preprod"
    else    
        echo "unknown ENV [$ENV]"
        usage
    fi

    echo "calling ~/bin/add_oracle_to_chef.sh"
    OUTPUT=`~/bin/add_oracle_to_chef.sh $TARGET_SCHEMA_WMSTOOLS WMSTOOLS $CHEF_ENV $DUMPFILE 2>&1`
    echo "output from add_oracle_node_to_chef [$OUTPUT]"
}

clean_up_and_exit()
{
    rm -f $DATA_PUMP_DIR/$DUMPFILE
    rm -f $LOCKFILE
    exit
}
# START
########

START_TICKS_ALL=`date +%s`

## do checks
############
if [ ! -r "$DUMPFILE_PATH" ]
then
    echo "can not read dumpfile [$DUMPFILE_PATH]"
    echo "did you include the path?"
    usage
fi
DUMPFILE=`basename $DUMPFILE_PATH`

connection=$(check_connection "$TARGET_SQLPLUS_WMSTOOLS")
if [ "$connection" != "connected" ]
then
    echo "unable to connect to target oracle db [$TARGET_SQLPLUS_WMSTOOLS]"
    usage
fi

connection_count=$(get_connection_count "$TARGET_SQLPLUS_WMSTOOLS" "$TARGET_SCHEMA_WMSTOOLS")
if [ "$connection_count" -gt 1 ]
then
    echo "$TARGET_SCHEMA_WMSTOOLS users are connected to database ($connection_count)"
    echo "did you stop the application?"
    echo "   SELECT SID,SERIAL#,LOGON_TIME,STATE,STATUS from v\$session where username = '$TARGET_SCHEMA_WMSTOOLS';"
    echo "   ALTER SYSTEM KILL SESSION 'sid,serial#';"
    usage
fi

DATA_PUMP_DIR=$(get_DATA_PUMP_DIR "$TARGET_SQLPLUS_WMSTOOLS")
if [ "$DATA_PUMP_DIR" == "" ]
then
    echo "can not get the DATA_PUMP_DIR"
    usage
fi

if [ "$DRY_RUN" != "" ]
then
    echo "DRY_RUN option encounter, exiting"
    exit
fi

check_and_create_lock_file
# from now on use clean_up_and_exit to exit

ln -f -s $DUMPFILE_PATH $DATA_PUMP_DIR

## start of refresh
###################
echo "exec'ing MANH_DROP_OBJECTS and purge recyclebin on $TARGET_SCHEMA_WMSTOOLS"
echo "monitor via - select count(*) from all_objects where owner='$TARGET_SCHEMA_WMSTOOLS';"
drop_objects "$TARGET_SQLPLUS_WMSTOOLS"


TARGET_SYSTEM_PASSWORD=`grep -w SYSTEM $HOME/config/.userEnv | awk '{ print $2 }'`
TARGET_SQLPLUS_SYSTEM="sqlplus -S SYSTEM/$TARGET_SYSTEM_PASSWORD"

IMPCMD="impdp USERID=SYSTEM/${TARGET_SYSTEM_PASSWORD} \
DIRECTORY=DATA_PUMP_DIR \
DUMPFILE=${DUMPFILE} \
LOGFILE=${DUMPFILE}.log \
REMAP_SCHEMA=WMSTOOLS_PROD:$TARGET_SCHEMA_WMSTOOLS \
REMAP_TABLESPACE=WMSTOOLS_PRD_TS:USERS \
SCHEMAS=WMSTOOLS_PROD \
EXCLUDE=TABLE:\"IN ('FS_FSENTRY','FS_WS_DEFAULT_FSENTRY','GLOBAL_REVISION','JOURNAL','LOCAL_REVISIONS','PM_WS_DEFAULT_BINVAL','PM_WS_DEFAULT_BUNDLE','PM_WS_DEFAULT_NAMES','PM_WS_DEFAULT_REFS','VERSIONING_FS_FSENTRY','VERSIONING_PM_BINVAL','VERSIONING_PM_BUNDLE','VERSIONING_PM_NAMES','VERSIONING_PM_REFS')\" \
VERSION=10.2"

START_TICKS=`date +%s`
echo "running impdp..."
echo "monitor via - tail -f ${DATA_PUMP_DIR}/${DUMPFILE}.log"
`${IMPCMD} > /dev/null 2>&1`
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`
echo "impdp finished in : ${TOTAL_TIME}s"


output=`grep -P "^Job .+ completed with" ${DATA_PUMP_DIR}/${DUMPFILE}.log`
if [ "$output" == "" ]
then
    echo "Error did not find completed in Data Pump Import (impdp) output"
    echo "Please check ${DATA_PUMP_DIR}/${DUMPFILE}.log"
    clean_up_and_exit
fi
echo "impdp output [$output]"

table_count=$(get_table_count "$TARGET_SQLPLUS_WMSTOOLS")
echo "table_count $TARGET_SCHEMA_WMSTOOLS [$table_count]"
if [ "$table_count" -le 45 ]
then
    echo "Refresh failed.. table count for $TARGET_SCHEMA_WMSTOOLS schema is less than 45"
    clean_up_and_exit
fi


## post refresh
###############
echo  "Compiling schemas $TARGET_SCHEMA_WMSTOOLS"
compile_objects "$TARGET_SQLPLUS_WMSTOOLS" "$TARGET_SCHEMA_WMSTOOLS"

echo "Gathering statistics $TARGET_SCHEMA_WMSTOOLS"
echo "monitor via - select distinct trunc(last_analyzed),count(*) from dba_tables where owner='$TARGET_SCHEMA_WMSTOOLS' group by trunc(last_analyzed);"
gather_stats "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMSTOOLS"


echo "create readonly role and do grants [RO_${TARGET_SCHEMA_WMSTOOLS}]"
create_grant_readonly_role "$TARGET_SQLPLUS_SYSTEM" "$TARGET_SCHEMA_WMSTOOLS"
## users in readonly role
# select * from  dba_role_privs where GRANTED_ROLE='RO_${TARGET_SCHEMA_WMSTOOLS}' order by GRANTEE;
## tables in readlonly role
# select * from dba_tab_privs where grantee='RO_${TARGET_SCHEMA_WMSTOOLS}';


END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS_ALL`

add_oracle_node_to_chef

cat<<EOF

ENV : $ENV
TARGET_SCHEMA_WMSTOOLS : $TARGET_SCHEMA_WMSTOOLS
DUMPFILE_PATH : $DUMPFILE_PATH
TOTAL_TIME : ${TOTAL_TIME}s
EOF

clean_up_and_exit      
