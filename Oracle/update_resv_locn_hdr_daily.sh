#!/bin/sh

# POED-1816
# ENG-8405
# Daily update total current volume and weight for Vegas inbound staging locations

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/db/oracle/app/oracle/product/11.2.0/client_1/instantclient:/usr/lib/oracle/12.1/client64/lib
if [ -z "$ORACLE_HOME" ]
then
    export ORACLE_HOME=/usr/lib/oracle/12.1/client64
fi

usage()
{
    echo
    echo "Usage: $0 username password host sid"
    echo "  updates table resv_locn_hdr for purge/clean up"
    echo
    echo "  See  POED-1816, ENG-8405"
    echo "  The intent of this update is to keep Vegas inbound staging locations with low current volume and weight. We have noticed that latency gets worse when moving LPNs as the location's cube and volume grows."
    exit
}


if [ $# -lt 4 ]; then
	echo "Invalid usage"
    usage
fi

username=$1
password=$2
server_name=$3
database_name=$4


updateJobStatus() {
    start_time=$1
    success_cnt=$2

    name=`basename $0`
    path=`dirname $0`
    hostname=`hostname -f`
    frequency="DAILY"

    echo "insert success into job_status"
    mysql --host usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com --user inventory --password='See PWS' --database inventory <<EOF
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('$name', '$path', '$hostname', '$frequency', $success_cnt, 0, '$start_time', now())
EOF
}

ORACLE_HEADER='
set heading off
set feedback off
set pages 0
'

check_connection()
{
    USERNAME=$1
    PASSWORD=$2
    SERVER_NAME=$3
    DATABASE_NAME=$4

    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"

    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT 'connected' from dual;
EOF
}


do_update()
{
    USERNAME=$1
    PASSWORD=$2
    SERVER_NAME=$3
    DATABASE_NAME=$4

    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"

    $SQLPLUS <<EOF
$ORACLE_HEADER
    
update resv_locn_hdr set curr_wt=0, curr_vol=0, curr_uom_qty=0, user_id='msells', mod_date_time=sysdate
where locn_id in (
	select locn_id
	from locn_hdr
	where dsp_locn in (
		'LV-PU-00-00-01','LV-PU-00-00-02','LV-PU-00-00-03','LV-PU-00-00-04','LV-PU-00-00-05',
		'LV-PU-BL-00-01','LV-PH-00-00-01','LV-PH-00-00-02','LV-PH-00-00-03','LV-PH-00-00-05',
		'LV-PH-00-00-06',
		'PL-PU-TW-AY-01','PL-PU-TW-AY-02','PL-PU-TW-AY-03','PL-PH-00-00-01','PL-PH-0I-MS-01',
		'PL-PH-OL-RR-01',
		'GL-PH-G3-00-01','GL-PH-G3-00-02','GL-PH-G3-00-03','GL-PU-TW-AY-01','GL-PU-TW-AY-02',
		'GL-PU-TW-AY-03','GL-PU-TW-AY-TV'
	)
);

EOF
}


############
### main ###
############
START_TIME=`date +'%F %T'`
echo "LOG> START TIME='$START_TIME'"
START_TICKS=`date +%s`

connection=$(check_connection $username $password $server_name $database_name)

if [ "$connection" != "connected" ]
then
    echo "unable to connect to oracle db [sqlplus $username/****@$server_name/$database_name]" >&2
    echo "Exiting" >&2
    usage
fi

update_cnt=$(do_update $username $password $server_name $database_name)

END_TICKS=`date +%s`
TOTAL_TIME=`expr $END_TICKS - $START_TICKS`
echo "LOG> END TIME='"`date +'%F %T'`"' (${TOTAL_TIME}s)"

# if we made it this far assume that all is well
# update the job_status db table
updateJobStatus "$START_TIME" $update_cnt
