#!/bin/sh

#
# run on both WMOS_MSF WMOS_WMS
#

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/db/oracle/app/oracle/product/11.2.0/client_1/instantclient:/usr/lib/oracle/12.1/client64/lib
if [ -z "$ORACLE_HOME" ]
then
    export ORACLE_HOME=/usr/lib/oracle/12.1/client64
fi

usage()
{
    echo
    echo "Usage: $0 SCHEMA TYPE USERNAME PASSWORD SERVER_NAME DATABASE_NAME"
    echo "  Where TYPE is 'FULL' or 'STALE'"
    exit
}

updateJobStatus() {
    start_time=$1
    schema=$2
    type=$3

    name=`basename $0`" $schema $type"
    path=`dirname $0`
    hostname=`hostname -f`

    frequency="DAILY"
    if [ "$type" == "FULL" ]
    then
        frequency="WEEKLY"
    fi

    echo "insert success into job_status"
    mysql --host usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com --user inventory --password='See PWS' --database inventory <<EOF
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('$name', '$path', '$hostname', '$frequency', 1, 0, '$start_time', now())
EOF
}


if [ $# -lt 6 ]
then
    echo "missing parameters"
    usage
fi

schema=$1
type=$2
username=$3
password=$4
server_name=$5
database_name=$6



ORACLE_HEADER='
set heading off
set feedback off
set pages 0
'

check_connection()
{
    USERNAME=$1
    PASSWORD=$2
    SERVER_NAME=$3
    DATABASE_NAME=$4

    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"

    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT 'connected' from dual;
EOF
}

do_show_stats()
{
    SCHEMA=$1
    USERNAME=$2
    PASSWORD=$3
    SERVER_NAME=$4
    DATABASE_NAME=$5
    
    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"
    
    $SQLPLUS <<EOF
$ORACLE_HEADER

select distinct trunc(last_analyzed), count(*) 
from dba_tables 
where owner='$SCHEMA'
group by trunc(last_analyzed)
order by trunc(last_analyzed);
EOF
}


do_gather_schema_stats_full()
{
    SCHEMA=$1
    USERNAME=$2
    PASSWORD=$3
    SERVER_NAME=$4
    DATABASE_NAME=$5

    echo "running gather_schema_stats"
    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"
    
    $SQLPLUS <<EOF
$ORACLE_HEADER

exec dbms_stats.gather_schema_stats( \
                   ownname          => '${SCHEMA}', \
                   method_opt       => 'for all columns size auto', \
                   estimate_percent => 100, \
                   cascade          => true, \
                   degree           => DBMS_STATS.DEFAULT_DEGREE, \
                   options          => 'gather');
EOF
}

do_gather_schema_stats_stale()
{
    SCHEMA=$1
    USERNAME=$2
    PASSWORD=$3
    SERVER_NAME=$4
    DATABASE_NAME=$5

    echo "running gather_schema_stats"
    SQLPLUS="sqlplus -S $USERNAME/$PASSWORD@$SERVER_NAME/$DATABASE_NAME"
    
    $SQLPLUS <<EOF
$ORACLE_HEADER

exec dbms_stats.gather_schema_stats( \
                   ownname          => '${SCHEMA}', \
                   method_opt       => 'for all columns size auto', \
                   estimate_percent => dbms_stats.auto_sample_size, \
                   cascade          => true, \
                   options          => 'gather auto');
EOF
}


############
### main ###
############
START_TIME=`date +'%F %T'`

echo "LOG> START TIME='$START_TIME'"
START_TICKS=`date +%s`

connection=$(check_connection $username $password $server_name $database_name)

if [ "$connection" != "connected" ]
then
    echo "unable to connect to oracle db [sqlplus $username/****@$server_name/$database_name]" >&2
    echo "Exiting" >&2
    usage
fi


echo "last_analyzed stats BEFORE"
do_show_stats $schema $username $password \
              $server_name $database_name

echo
echo "Gathering $type statistics for $schema@$server_name"
echo "monitor via - select distinct trunc(last_analyzed),count(*) from dba_tables where owner='$schema' group by trunc(last_analyzed);"
echo

if [ "$type" = "FULL" ]
then
    do_gather_schema_stats_full $schema $username $password \
                                $server_name $database_name
elif [ "$type" = "STALE" ]
then
    do_gather_schema_stats_stale $schema $username $password \
                                 $server_name $database_name
else
    echo "Error wrong TYPE passed in [$type]"
    usage
fi

echo "last_analyzed stats AFTER"
do_show_stats $schema $username $password \
              $server_name $database_name

echo
END_TICKS=`date +%s`
TOTAL_TIME=`expr $END_TICKS - $START_TICKS`
echo "LOG> END TIME='"`date +'%F %T'`"' (${TOTAL_TIME}s)"

updateJobStatus "$START_TIME" $schema $type
