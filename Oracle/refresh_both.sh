#!/bin/sh


pagerAddress='2023292928@vtext.com'

ENV=$1
if [ "$ENV" != "DEV00" ] && [ "$ENV" != "DEV01" ] && [ "$ENV" != "DEV02" ]
then
    echo "env $ENV is not one of [DEV00, DEV01, DEV02]"
    echo "exiting"
    exit
fi

START_TICKS=`date +%s`
./refresh_WMS-MSF_from_prod.sh $ENV /var/tmp/WMOS_PROD_MSF_WMOS_PROD_WMS_`date +%m%d%Y`_????.dmp > ${ENV}_WMS-MSF_output.txt 2>&1
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`

mail -s 'done with refresh_WMS-MSF_from_prod' $pagerAddress<<END_OF_EMAIL
TOTAL_TIME : ${TOTAL_TIME}s
END_OF_EMAIL


START_TICKS=`date +%s`
./refresh_WMSTOOLS_from_prod.sh $ENV /var/tmp/PROD_WMSTOOLS_PROD_`date +%m%d%Y`_????.dmp > ${ENV}_WMSTOOLS_output.txt 2>&1
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`

mail -s 'done with refresh_WMSTOOLS_from_prod' $pagerAddress<<END_OF_EMAIL
TOTAL_TIME : ${TOTAL_TIME}s
END_OF_EMAIL

