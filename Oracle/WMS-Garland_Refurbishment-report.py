#!/usr/bin/env python

import argparse
import datetime
import logging
import MySQLdb  #  yum install MySQL-python
import os
import socket
import subprocess
import sys
import xlsxwriter


from pprint import pprint

# needed for oracle libs
if 'LD_LIBRARY_PATH' not in os.environ:
    os.environ['LD_LIBRARY_PATH'] = '/usr/lib/oracle/12.1/client64/lib'
    try:
        os.execv(sys.argv[0], sys.argv)
    except Exception, exc:
        print 'Failed re-exec:', exc
        sys.exit(1)

import cx_Oracle

logging.basicConfig(format='%(asctime)s %(funcName)-30s - %(levelname)-8s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger()

def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

parser = argparse.ArgumentParser(
    description="daily oracle reports written to the Pdrive",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.register('type','bool',str2bool)

parser.add_argument(
    "--db_oracle_user_WMS",
    help="oracle database user",
    default='WMOS_WMS_RO',
)
parser.add_argument(
    "--db_oracle_password_WMS",
    help="oracle database password",
    default='See PWS',
)
parser.add_argument(
    "--db_oracle_host_WMS",
    help="oracle database host",
    default="usevsWMSora101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com",
)
parser.add_argument(
    "--db_oracle_port",
    type=int,
    help="oracle database port",
    default=1521    
)
parser.add_argument(
    "--db_oracle_sid",
    help="oracle database sid",
    default='ORCL'
)
parser.add_argument(
    "--max_column_width",
    help="max width in the execl column",
    default=40
)
parser.add_argument(
    "--write_to_share",
    type=str2bool,
    help="Set to true write report to the --smb_Pdrive",
    default=False,
)
parser.add_argument(
    "--smb_Pdrive",
    help="The share host name",
    default='DC1VSFILE102A.lsi.local'
)
parser.add_argument(
    "--smb_workgroup",
    help="The share workgroup",
    default='LSI'
)
parser.add_argument(
    "--smb_user",
    help="The user that writes to the share",
    default='svc_reporting.wms'
)
parser.add_argument(
    "--smb_password",
    help="The user that writes to the share",
    default='See PWS',
)
parser.add_argument(
    "--status_db_user",
    help="mysql database user",
    default='inventory',
)
parser.add_argument(
    "--status_db_password",
    help="database password",
    default='See PWS',
)
parser.add_argument(
    "--status_db_schema",
    help="database schema",
    default='inventory',
)
parser.add_argument(
    "--status_db_host",
    help="database host",
    default='usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com',
)
parser.add_argument(
    "--status_db_port",
    type=int,
    help="mysql database port",
    default=3306,
)
parser.add_argument(
    "--logLevel",
    nargs='?',
    help="logging level",
    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    default='INFO'
)

args = parser.parse_args()
log.setLevel(getattr(logging, args.logLevel))


###############
## Job Status stuff
##
def updateJobStatus(start_time, success_cnt, failure_cnt):
    log.info("writting to job_status db table")
    conn  = getMysqlConnection(args.status_db_host, args.status_db_port,
                               args.status_db_user, args.status_db_password,
                               args.status_db_schema)

    sql="""\
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('{}', '{}', '{}', '{}', {}, {}, '{}', {})\
""".format(os.path.basename(sys.argv[0]),
           os.path.dirname(sys.argv[0]),
           socket.gethostname(),
           '5X-daily',
           success_cnt,
           failure_cnt,
           start_time,
           'now()')
    log.info("sql:[{}]".format(sql))
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()


def getMysqlConnection(host, port, user, password, schema):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} schema:{}".format(host,
                                                                           port,
                                                                           user,
                                                                           schema))
        conn = MySQLdb.connect(host=host, port=port,
                               user=user, passwd=password,
                               db=schema, charset='utf8', use_unicode=True)

    except MySQLdb.Error as e:
        log.critical("ERROR connecting to:{}".format(host))
        log.critical("EXCEPTION:{}".format(e))
        log.exception("Traceback")
        sys.exit(1)
    except:
        log.fatal("ERROR connecting to {}".format(host))
        log.exception("Traceback")
        sys.exit(1)

    return conn


###############
## Oracle stuff
##
def OutputTypeHandler(cursor, name, defaultType, size,
                      precision, scale):
    # unicode for all
    if defaultType in (cx_Oracle.STRING, cx_Oracle.FIXED_CHAR): 
        return cursor.var(unicode, size, cursor.arraysize)

def getOracleConnection(host, port, user, password, sid):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} sid:{}".format(host,
                                                                        port,
                                                                        user,
                                                                        sid))
        connstr = user +'/'+ password +'@'+ host +':'+ str(port) +'/'+ sid
        conn = cx_Oracle.connect(connstr)
        conn.outputtypehandler = OutputTypeHandler
        
    except cx_Oracle.DatabaseError as e:
        log.error("ERROR with Oracle connection on:{}".format(host))
        log.error("EXCEPTION:{}".format(e))
        log.exception("Traceback")
        sys.exit(1)
    except:
        log.fatal("ERROR connecting to {0}".format(host))
        log.exception("Traceback")
        sys.exit(1)

    return conn

    
############
## SQL stuff
##
def doSelect(conn, sql):
    table = []
    try:
        cursor = conn.cursor()
        log.debug("SQL : {0}".format(sql))
        cursor.execute(sql)
        
        columns = cursor.description
        table.append([row[0] for row in columns])
        
        for row in cursor.fetchall():
            table.append(row)
                    
    except:
        log.exception("ERROR selecting from {}".format(conn))

    return table
    
###############
## reports
##
def isfloat(value):
  try:
    float(value)
    return True
  except:
    return False

def get_column_types(report) :
    typeList = [None] * len(report[0])
    x = -1
    for column in zip(*report[1:]) :
        x += 1
        for item in column :
            if (item is None) :
                continue
            t = type(item)
            newType = t            
            if ((t is unicode) or (t is str)) :
                if (item.isdigit()) :
                    newType = int
                elif (isfloat(item)) :
                    newType = float
                                
            if ((newType != int) and (newType != float)) :
                typeList[x] = newType
                log.debug("called break for column {} newType {}".format(x, newType))
                break
            
            if (typeList[x] is None) :
                typeList[x] = newType
            elif (typeList[x] != newType) :
                typeList[x] = float

    # trust but validate
    for row in report[:1] :
        x = -1
        for item in row :
            x += 1
            log.debug("column {}  {} <-> {}".format(x, item, typeList[x]))    
    return typeList


def add_report_2_worksheet(report, worksheet, date_format) :
    typeList = get_column_types(report)
    
    worksheet.freeze_panes(1, 0)
    
    column_width = []
    x = 0
    y = 0
    for row in report[:1] :
        for item in row :
            column_width.append(0)
            width = len(str(item)) + 2.1
            if (width > column_width[y]) :
                column_width[y] = width
            worksheet.write(x, y, item, bold_format)
            y += 1

    x = 1
    y = 0
    for row in report[1:] :
        for item in row :
            if (item is None) :
                worksheet.write(x, y, item)
            else :
                if (type(item) is datetime.datetime) :
                    width = len(str(date_format.num_format))
                    worksheet.write(x, y, item, date_format)
                else :
                    try :
                        if (typeList[y] == int) :
                            item = int(item)
                        elif (typeList[y] == float) :
                            item = float(item)
                    except :
                        log.warn("issue with cast to {} for [{}] column {}".format(typeList[y],
                                                                               item, y))
                
                    worksheet.write(x, y, item)

                    try :
                        if (type(item) is unicode) :
                            width = len(item) + 1
                        else :
                            width = len(str(item)) + 5
                    except :
                        log.warn("issue with getting length of [{}]".format(item))

            if (width > column_width[y]) :
                column_width[y] = width                
            y += 1
        y = 0
        x += 1

    y = 0
    for column in column_width :
        if (column > args.max_column_width) :
            column = args.max_column_width
            log.debug("at max width for column:{}".format(y))
        worksheet.set_column(y, y, column)
        y += 1


def report_WMS_Garland_Refurb(conn) :
    log.info("running")
    return doSelect(conn,
"""
SELECT 
  wmos_wms.prod_trkg_tran.user_id                     AS "User",
  wmos_msf.locn_hdr.dsp_locn                          AS "Location",
  wmos_wms.prod_trkg_tran.menu_optn_name              AS "Transaction Name",
  Count(DISTINCT( wmos_wms.prod_trkg_tran.cntr_nbr )) AS "Count of LPNs"
FROM   
  wmos_wms.prod_trkg_tran
  left join wmos_msf.locn_hdr
    ON wmos_wms.prod_trkg_tran.to_locn = wmos_msf.locn_hdr.locn_id
WHERE  
  wmos_msf.locn_hdr.dsp_locn LIKE '%GR%'
  AND wmos_wms.prod_trkg_tran.whse = 'DFW'
  AND trunc(wmos_wms.prod_trkg_tran.create_date_time) = trunc(sysdate)
  AND wmos_wms.prod_trkg_tran.menu_optn_name = 'Move iLPN/cLPN'
GROUP BY 
  wmos_wms.prod_trkg_tran.user_id,
  wmos_msf.locn_hdr.dsp_locn,
  wmos_wms.prod_trkg_tran.menu_optn_name
""")


###############
## main
##
start_time = datetime.datetime.now()

connWMS = getOracleConnection(args.db_oracle_host_WMS,
                              args.db_oracle_port,
                              args.db_oracle_user_WMS,
                              args.db_oracle_password_WMS,
                              args.db_oracle_sid)

filename = 'WMS-Garland_Refurbishment-reports_{:%Y-%m-%d_%H}.xlsx'.format(
    datetime.datetime.now())
workbook = xlsxwriter.Workbook('/tmp/' + filename)
date_format = workbook.add_format({'num_format': 'mm/dd/yy hh:mm:ss'})
bold_format = workbook.add_format({'bold': True})

worksheet = workbook.add_worksheet('WMS Garland Moves Refurbishment')
report = report_WMS_Garland_Refurb(connWMS)
add_report_2_worksheet(report, worksheet, date_format)

worksheet = workbook.add_worksheet('Report Info')
report = [
    ['Name', 'Value'],
    ['from host', socket.gethostname()],
    ['from db', args.db_oracle_host_WMS],
    ['from script', sys.argv[0]],
    ['run at', datetime.datetime.now()],
]
add_report_2_worksheet(report, worksheet, date_format)

workbook.close()
if (not args.write_to_share) :
    log.warn("--write_to_share not set, not writting to share")
    log.info("spread sheet written to /tmp/{}".format(filename))
    sys.exit()

#############
## write report to the Pdrive
##
os.chdir('/tmp')

# create MS shortcut
smbPath = '\\\{}\Files\Public\WMS-reports\DFW-Refurb-Prod-5'.format(args.smb_Pdrive)
subprocess.call(['/usr/local/bin/mslink.sh',
                 '-l',
                 '{}\{}'.format(smbPath, filename),
                 '-o',
                 'WMS-Garland_Refurbishment-reports_current.lnk'],
                shell=False)

log.info("calling smbclient for put and dir list")
process = subprocess.Popen(['smbclient',
                            '//' + args.smb_Pdrive + '/files/Public',
                            '--user',     args.smb_user,
                            '--workgroup', args.smb_workgroup,
                            args.smb_password],
                           shell=False,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
process.stdin.write('cd WMS-reports\DFW-Refurb-Prod-5\n')
process.stdin.write('put {}\n'.format(filename))
process.stdin.write('put {}\n'.format('WMS-Garland_Refurbishment-reports_current.lnk'))
(stdout,stderr) = process.communicate('ls WMS-Garland_Refurbishment-reports_*.xlsx')
os.remove('/tmp/' + filename)
os.remove('/tmp/' + 'WMS-Garland_Refurbishment-reports_current.lnk')

dir_list=[]
del_list=[]
for line in stdout.split('\n') :
    line = line.lstrip()
    if (line.startswith('WMS-Garland_Refurbishment-reports_')) :
        f = line.split()[0]
        if (f.endswith('.xlsx')) :
            dir_list.append(f)
            
del_date = '{:%Y-%m-%d}'.format(datetime.datetime.now() +
                                datetime.timedelta(-60))
verified=False
for f in dir_list :
    if (f == filename) :
        verified=True
    file_date = f.split('_')[2]
    if (file_date < del_date) :
        del_list.append(f)

success_cnt=1
failure_cnt=0
if (not verified) :
    log.warn("could not read filename:{} from the P-drive".format(filename))
    success_cnt=0
    failure_cnt=1
    
# if we made it this far assume that the file got written
# to the share correctly, update the job_status db table
updateJobStatus(start_time, success_cnt, failure_cnt)

if (not del_list) :
    log.info("no files older then {}... exiting".format(del_date))
    sys.exit(0)
    
log.info("removing old reports:{}".format(del_list))
log.info("calling smbclient to remove old reports")
process = subprocess.Popen(['smbclient',
                            '//' + args.smb_Pdrive + '/files/Public',
                            '--user',     args.smb_user,
                            '--workgroup', args.smb_workgroup,
                            args.smb_password],
                           shell=False,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
process.stdin.write('cd WMS-reports\DFW-Refurb-Prod-5\n')
for f in del_list :
    log.info("removing old report:{}".format(f))
    process.stdin.write('rm {}\n'.format(f))

(stdout,stderr) = process.communicate() # this Waits for process to terminate 
