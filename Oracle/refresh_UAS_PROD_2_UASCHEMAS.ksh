#!/bin/ksh
if [ $# -ne 3 ]; then
	echo "Invalid Usage"
	echo "Usage: <SOURCE ORACLESID> <TARGET_ORACLESID>  <SOURCE SCHEMA> <TARGET_SCHEMA> "
	exit
fi

PROD_ORACLE_SID=${1}
ORACLE_SID=${2}
SOURCE_SCHEMA=${3}
TARGET_SCHEMA=${4}
export DUMP_DIR=/db/app/orabackups/dailydumps/${ORACLE_SID}
TIMESTAMP="`date +'%m%d%Y'`"

. /home/oracle/${ORACLE_SID}.env

SCRIPTBASE=${HOME}/oracle_admin_scripts
BINDIR=${SCRIPTBASE}/bin
SQLDIR=${SCRIPTBASE}/sql
CONFIGDIR=${SCRIPTBASE}/config
TMPDIR=${SCRIPTBASE}/tmp
LOGDIR=${SCRIPTBASE}/logs
USERENVDIR=${HOME}/config
USERENVFILE=${USERENVDIR}/.userEnv


TARGET_SCHEMA_PASSWORD=`grep ${TARGET_SCHEMA} ${USERENVFILE} | awk '{ print $2 }'`

echo "Copying the dump file to destination..... "

#rm -f ${DUMP_DIR}/PROD_UAS_PROD_${TIMESTAMP}_0300.dmp*

cp /db/app/dumps/${PROD_ORACLE_SID}/PROD_UAS_PROD_${TIMESTAMP}_0300.dmp.gz  ${DUMP_DIR}

DUMPFILE="PROD_UAS_PROD_${TIMESTAMP}_0300.dmp.gz"
INTERACTIVE=99

if [ ! -e ${DUMP_DIR}/${DUMPFILE} ] ; then
        echo "Expected dump file ${DUMPFILE}  not found at ${DUMP_DIR} .. exiting "
        exit 1
fi


file ${DUMP_DIR}/${DUMPFILE} | grep gzip > /dev/null
if [ $? -eq 0 ]; then   # this is gunzip file then uncompress it before import
        gunzip ${DUMP_DIR}/${DUMPFILE}
        DUMPFILE=`echo ${DUMPFILE} | sed -e 's/.gz//'`
else
        DUMPFILE=`echo ${DUMPFILE} | sed -e 's/.gz//'`

fi

echo "Target Schema: ${TARGET_SCHEMA}/TARGET_SCHEMA_PASSWD: Dump file: ${DUMP_DIR}/${DUMPFILE}\n"
#exit

#sqlplus ${TARGET_SCHEMA} @/home/oracle/dbadmin/scripts/UADEV/UADEV_DROP_ALL_OBJECTS_PROC.sql
echo "Dropping objects on ${TARGET_SCHEMA}. Hit enter to continue..."


sqlplus ${TARGET_SCHEMA}/${TARGET_SCHEMA_PASSWORD} <<EOF 
exec UA_DROP_OBJECTS;
EOF

sqlplus -s -N / as sysdba <<EOF
SELECT OBJECT_NAME from dba_objects where OWNER='${TARGET_SCHEMA}';
EOF

echo "DUMPFILE TO BE USED: ${DUMPFILE}"

echo "\nStarting impdp  .. this will take long time. Ignore any grant errors .."
#echo "impdp system/LSI0ranges@wmtest dumpfile=${DUMPFILE} logfile=${dumpfile}.log transform=oid:n remap_schema=${SOURCE_SCHEMA}:${TARGET_SCHEMA} remap_tablespace=UAS_PROD_TS:USERS schemas=${SOURCE_SCHEMA} version=10.2 directory=DATA_PUMP_DIR"

impdp system/LSI0ranges@SLSORA01 dumpfile=${DUMPFILE} logfile=${dumpfile}.log transform=oid:n remap_schema=${SOURCE_SCHEMA}:${TARGET_SCHEMA} remap_tablespace=UAS_PROD_TS:UNIFIEDACCOUNTS_STAGE_TS schemas=${SOURCE_SCHEMA} version=10.2 directory=DATA_PUMP_DIR

sqlplus -s -N / as sysdba <<EOF
REM 'Compiling objects'
exec dbms_utility.compile_schema('${TARGET_SCHEMA}');
select * from dba_objects where status = 'INVALID' and owner = '${TARGET_SCHEMA}' ;
REM 'Computing stats'
exec dbms_stats.gather_schema_stats( ownname=>'${TARGET_SCHEMA}', estimate_percent=> 100, DEGREE=>4, GRANULARITY=>'ALL',CASCADE=>TRUE);
exit;
EOF
