#!/bin/sh

usage()
{
    echo
    echo "Usage: $0 ENV dumpfile [dry_run]"
    echo "  dumpfile requires full path"
    echo "  examples"
    echo "  DEV"
    echo "    DEV00   OOC1/DEV00/QA3"
    echo "    nohup $0 DEV00   /var/tmp/WMOS_PROD_MSF_WMOS_PROD_WMS_\`date +%m%d%Y\`_????.dmp > DEV00_WMS-MSF_output.txt 2>&1 &"
    echo "    DEV01   TEST1/DEV01/QA1"
    echo "      nohup $0 DEV01  /var/tmp/WMOS_PROD_MSF_WMOS_PROD_WMS_\`date +%m%d%Y\`_????.dmp > DEV01_WMS-MSF_output.txt 2>&1 &"
    echo "    DEV02   TEST10/DEV02/QA2"
    echo "      nohup $0 TEST10 /var/tmp/WMOS_PROD_MSF_WMOS_PROD_WMS_\`date +%m%d%Y\`_????.dmp > TEST10_WMS-MSF_output.txt 2>&1 &"
    echo "  STAGE"
    echo "    nohup $0 STAGE    /var/tmp/WMOS_PROD_MSF_WMOS_PROD_WMS_\`date +%m%d%Y\`_????.dmp > STAGE_WMS-MSF_output.txt 2>&1 &"
    exit
}

if [ $# -lt 2 ]
then
   echo "missing parameters"
   usage
fi

ENV=$1
DUMPFILE_PATH=$2
DRY_RUN=$3

TARGET_SCHEMA_WMS="NOT_SET"
TARGET_SCHEMA_MSF="NOT_SET"

PORT="NOT_SET"
DEST_PARAMS_1="NOT_SET"
DEST_PARAMS_2="NOT_SET"
SUBSCRIPTION_NAME="NOT_SET"
PROVIDER_URL="NOT_SET"
MISC_FLAGS="NOT_SET"

# OOC1/DEV00/QA3
if [ "$ENV" == "DEV00" ]
then
    TARGET_SCHEMA_WMS="WMUSER2"
    TARGET_SCHEMA_MSF="WMUSER"

    HOST_NAME="test-wmos1.test.liquidation.com"
    PORT=10501
    DEST_PARAMS_1="localhost:10504"
    DEST_PARAMS_2="localhost:10004"
    SUBSCRIPTION_NAME="test-wmos1:10500"
    PROVIDER_URL="localhost:10504"
    MISC_FLAGS="test-wmtools.test.liquidation.com"
        
# TEST1/DEV01/QA1
elif [  "$ENV" == "DEV01" ]
then
    TARGET_SCHEMA_WMS="WMOS_DEV01_WMS"
    TARGET_SCHEMA_MSF="WMOS_DEV01_MSF"
    
    HOST_NAME="test-wmos1.test.liquidation.com"
    PORT=11501
    DEST_PARAMS_1="localhost:11504"
    DEST_PARAMS_2="localhost:11004"
    SUBSCRIPTION_NAME="test-wmos1:11500"
    PROVIDER_URL="localhost:11504"
    MISC_FLAGS="test-wmtools.test.liquidation.com:9090"

# TEST10/DEV02/QA2
elif [  "$ENV" == "DEV02" ]
then
    TARGET_SCHEMA_WMS="WMOS_DEV02_WMS"
    TARGET_SCHEMA_MSF="WMOS_DEV02_MSF"

    HOST_NAME="test-wmos1.test.liquidation.com"
    PORT=12501
    DEST_PARAMS_1="localhost:12504"
    DEST_PARAMS_2="localhost:12004"
    SUBSCRIPTION_NAME="test-wmos1:12500"
    PROVIDER_URL="localhost:12504"
    MISC_FLAGS="test-wmtools.test.liquidation.com:8080"

elif [  "$ENV" == "STAGE" ]
then
    TARGET_SCHEMA_WMS="WMOS_STAGE_WMS"
    TARGET_SCHEMA_MSF="WMOS_STAGE_MSF"

    HOST_NAME="stage-wm.ash.liquidation.com"
    PORT=10501
    DEST_PARAMS_1="localhost:10504"
    DEST_PARAMS_2="localhost:10004"
    SUBSCRIPTION_NAME="stage-wmos01:10500"
    PROVIDER_URL="localhost:10504"
    MISC_FLAGS="stage-wmtools.ash.liquidation.com:8080"
    
else
    echo "unknown ENV [$ENV]"
    usage
fi
    
    

TARGET_SCHEMA_WMS_PASSWORD=`grep -w ${TARGET_SCHEMA_WMS} $HOME/config/.userEnv | awk '{ print $2 }'`
TARGET_SCHEMA_MSF_PASSWORD=`grep -w ${TARGET_SCHEMA_MSF} $HOME/config/.userEnv | awk '{ print $2 }'`
TARGET_SQLPLUS_WMS="sqlplus -S $TARGET_SCHEMA_WMS/$TARGET_SCHEMA_WMS_PASSWORD"
TARGET_SQLPLUS_MSF="sqlplus -S $TARGET_SCHEMA_MSF/$TARGET_SCHEMA_MSF_PASSWORD"

BASEDIR=/home/oracle/dbadmin/scripts/schemaRefresh
SQLDIR=${BASEDIR}/sql
LOCKFILE=/var/tmp/refresh-${TARGET_SCHEMA_WMS}.lck


cat<<EOF
TARGET_SCHEMA_WMS : $TARGET_SCHEMA_WMS
TARGET_SCHEMA_MSF : $TARGET_SCHEMA_MSF
DUMPFILE_PATH : $DUMPFILE_PATH

Post Refreash params...
PORT : $PORT
DEST_PARAMS_1 : $DEST_PARAMS_1
DEST_PARAMS_2 : $DEST_PARAMS_2
SUBSCRIPTION_NAME : $SUBSCRIPTION_NAME
PROVIDER_URL : $PROVIDER_URL
EOF



#######
# helper functions

check_and_create_lock_file()
{
    if [ -e $LOCKFILE ]
    then
        echo "Error lockfile [$LOCKFILE] exits."
        echo "A Previous refresh may be still going on or might have failed."
        echo "Verify and remove lockfile if needed."
        exit
    fi
    echo "Pid : $$" > $LOCKFILE
    echo "Start :" `date` >> $LOCKFILE
}

ORACLE_HEADER='
set heading off
set feedback off
set pages 0
'

check_connection()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT 'connected' from dual;
EOF
}

get_connection_count()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT count(*) from v\$session where username = '$SCHEMA';
EOF
}

get_table_count()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
select count(*) from user_tables where table_name not like 'SYS%';
EOF
}

get_DATA_PUMP_DIR()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
SELECT directory_path FROM dba_directories WHERE directory_name = 'DATA_PUMP_DIR';
EOF
}

drop_objects()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec MANH_DROP_OBJECTS;
commit;
purge recyclebin; 
commit;
EOF
}

correct_missing_index()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
CREATE INDEX "$SCHEMA"."PE_EMP_PERF_IDX2" ON "$SCHEMA"."E_EMP_PERF_SMRY" ("LOGIN_USER_ID", "WHSE", "ROUND_CLOCK_IN_DATE") PCTFREE 30 INITRANS 40 MAXTRANS 255 NOCOMPRESS LOGGING TABLESPACE "MLMINDX" PARALLEL 1 ;
commit;    
EOF
}

compile_objects()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec dbms_utility.compile_schema('${SCHEMA}');
EOF
}

update_instance_table()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
update app_instance set host_name='$HOST_NAME', port=$PORT where host_name='prod-wm.ash.liquidation.com';
commit;
EOF
}

update_router_table()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
update router set dest_params = replace(dest_params,'localhost:10504','$DEST_PARAMS_1') where dest_params like '%localhost:10504%';
commit;
update router set dest_params = replace(dest_params,'localhost:10004','$DEST_PARAMS_2') where dest_params like '%localhost:10004%';
commit;
EOF
}

update_listener_table()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
update LISTENER set SUBSCRIPTION_NAME=replace(SUBSCRIPTION_NAME,'wm:10500','$SUBSCRIPTION_NAME');
commit;
EOF
}

update_jms_listener_table()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
update JMS_LISTENER set PROVIDER_URL =replace(PROVIDER_URL,'localhost:10504','$PROVIDER_URL');
commit;
EOF
}

resequence_it()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
@${SQLDIR}/SequenceUpdateScript.sql;
EOF
}

gather_stats()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
exec dbms_stats.gather_schema_stats( ownname=>'${SCHEMA}', estimate_percent=> 100, DEGREE=>4, GRANULARITY=>'ALL',CASCADE=>TRUE);
EOF
}

create_grant_readonly_role()
{
    SQLPLUS=$1
    SCHEMA=$2

    $SQLPLUS <<EOF > /dev/null
$ORACLE_HEADER
drop role RO_${SCHEMA};
create role RO_${SCHEMA};

SPOOL /tmp/RO_${SCHEMA}.SQL replace;
SELECT 'GRANT SELECT ON ${SCHEMA}.'||OBJECT_NAME || ' TO RO_${SCHEMA};' FROM
DBA_OBJECTS WHERE OBJECT_TYPE IN('TABLE','VIEW') and owner='${SCHEMA}';
SPOOL off;

@/tmp/RO_${SCHEMA}.SQL;
EOF
}


updateURLs ()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
UPDATE sys_code SET misc_flags = 'http://$MISC_FLAGS/itemsearch' WHERE REC_TYPE= 'C' and CODE_TYPE = 'URL' and CODE_ID = 'E12IS';
UPDATE sys_code SET misc_flags = 'http://$MISC_FLAGS/testingengine' WHERE REC_TYPE= 'C' and CODE_TYPE = 'URL' and CODE_ID = 'E12TQ';
UPDATE sys_code SET misc_flags = 'http://$MISC_FLAGS/sortingengine' WHERE REC_TYPE= 'C' and CODE_TYPE = 'URL' and CODE_ID = 'E02';
commit;
EOF
}

updateLicense ()
{
    SQLPLUS=$1
    SCHEMA=$2
    $SQLPLUS <<EOF
$ORACLE_HEADER
truncate table LICENSE;
commit;
insert into LICENSE select * from DBADMIN.${SCHEMA}_LICENSE;
COMMIT;
EOF
}

do_triggers ()
{
    SQLPLUS=$1
    $SQLPLUS <<EOF
$ORACLE_HEADER   
@/home/oracle/dbadmin/scripts/wms/sql/create_trigger_sdn_117_new_version.sql
EOF
}

add_oracle_node_to_chef()
{
    if [ "$ENV" == "DEV00" ]
    then
        CHEF_ENV="qa3"
        SCHEMA="WMUSER2-WMUSER"
    elif [  "$ENV" == "DEV01" ]
    then
        CHEF_ENV="qa1"
        SCHEMA="WMOS_DEV01_WMS-MSF"
    elif [  "$ENV" == "DEV02" ]
    then 
        CHEF_ENV="qa2"
        SCHEMA="WMOS_DEV02_WMS-MSF"
    elif [  "$ENV" == "STAGE" ]
    then 
        CHEF_ENV="preprod"
        SCHEMA="WMOS_STAGE_WMS-MSF"
    else    
        echo "unknown ENV [$ENV]"
        usage
    fi

    echo "calling ~/bin/add_oracle_to_chef.sh"
    OUTPUT=`~/bin/add_oracle_to_chef.sh $SCHEMA WMS-MSF $CHEF_ENV $DUMPFILE 2>&1`
    echo "output from add_oracle_node_to_chef [$OUTPUT]"
}


clean_up_and_exit()
{
    rm -f $DATA_PUMP_DIR/$DUMPFILE
    rm -f $LOCKFILE
    exit
}
# START
########

START_TICKS_ALL=`date +%s`

## do checks
############
if [ ! -r "$DUMPFILE_PATH" ]
then
    echo "can not read dumpfile [$DUMPFILE_PATH]"
    echo "did you include the path?"
    usage
fi
DUMPFILE=`basename $DUMPFILE_PATH`

connection=$(check_connection "$TARGET_SQLPLUS_WMS")
if [ "$connection" != "connected" ]
then
    echo "unable to connect to target oracle db [$TARGET_SQLPLUS_WMS]"
    usage
fi

connection=$(check_connection "$TARGET_SQLPLUS_MSF")
if [ "$connection" != "connected" ]
then
    echo "unable to connect to target oracle db [$TARGET_SQLPLUS_MSF]"
    usage
fi

connection_count=$(get_connection_count "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS")
if [ "$connection_count" -gt 1 ]
then
    echo "$TARGET_SCHEMA_WMS users are connected to database ($connection_count)"
    echo "did you stop the application?"
    echo "   SELECT SID,SERIAL#,LOGON_TIME,STATE,STATUS from v\$session where username = '$TARGET_SCHEMA_WMS';"
    echo "   ALTER SYSTEM KILL SESSION 'sid,serial#';"
    usage
fi

connection_count=$(get_connection_count "$TARGET_SQLPLUS_MSF" "$TARGET_SCHEMA_MSF")
if [ "$connection_count" -gt 1 ]
then
    echo "$TARGET_SCHEMA_MSF users are connected to database ($connection_count)"
    echo "did you stop the application?"
    echo "   SELECT SID,SERIAL#,LOGON_TIME,STATE,STATUS from v\$session where username = '$TARGET_SCHEMA_MSF';"
    echo "   ALTER SYSTEM KILL SESSION 'sid,serial#';"
    usage
fi

DATA_PUMP_DIR=$(get_DATA_PUMP_DIR "$TARGET_SQLPLUS_WMS")
if [ "$DATA_PUMP_DIR" == "" ]
then
    echo "can not get the DATA_PUMP_DIR"
    usage
fi

if [ "$DRY_RUN" != "" ]
then
    echo "DRY_RUN option encounter, exiting"
    exit
fi

check_and_create_lock_file
# from now on use clean_up_and_exit to exit

ln -f -s $DUMPFILE_PATH $DATA_PUMP_DIR

## start of refresh
###################
echo "exec'ing MANH_DROP_OBJECTS and purge recyclebin on $TARGET_SCHEMA_WMS"
echo "monitor via - select count(*) from all_objects where owner='$TARGET_SCHEMA_WMS';"
drop_objects "$TARGET_SQLPLUS_WMS"

echo "exec'ing MANH_DROP_OBJECTS and purge recyclebin on $TARGET_SCHEMA_MSF"
echo "monitor via - select count(*) from all_objects where owner='$TARGET_SCHEMA_MSF';"
drop_objects "$TARGET_SQLPLUS_MSF"


TARGET_SYSTEM_PASSWORD=`grep -w SYSTEM $HOME/config/.userEnv | awk '{ print $2 }'`
TARGET_SQLPLUS_SYSTEM="sqlplus -S SYSTEM/$TARGET_SYSTEM_PASSWORD"

IMPCMD="impdp USERID=SYSTEM/${TARGET_SYSTEM_PASSWORD} \
DIRECTORY=DATA_PUMP_DIR \
DUMPFILE=${DUMPFILE} \
LOGFILE=${DUMPFILE}.log \
TRANSFORM=oid:n \
REMAP_SCHEMA=WMOS_PROD_MSF:$TARGET_SCHEMA_MSF,WMOS_PROD_WMS:$TARGET_SCHEMA_WMS \
SCHEMAS=WMOS_PROD_MSF,WMOS_PROD_WMS \
VERSION=10.2"

START_TICKS=`date +%s`
echo "running impdp..."
echo "monitor via - tail -f ${DATA_PUMP_DIR}/${DUMPFILE}.log"
`${IMPCMD} > /dev/null 2>&1`
END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS`
echo "impdp finished in : ${TOTAL_TIME}s"


output=`grep -P "^Job .+ completed with" ${DATA_PUMP_DIR}/${DUMPFILE}.log`
if [ "$output" == "" ]
then
    echo "Error did not find completed in Data Pump Import (impdp) output"
    echo "Please check ${DATA_PUMP_DIR}/${DUMPFILE}.log"
    clean_up_and_exit
fi
echo "impdp output [$output]"

table_count=$(get_table_count "$TARGET_SQLPLUS_WMS")
echo "table_count $TARGET_SCHEMA_WMS [$table_count]"
if [ "$table_count" -le 500 ]
then
    echo "Refresh failed.. table count for $TARGET_SCHEMA_WMS schema is less than 500"
    clean_up_and_exit      
fi

table_count=$(get_table_count "$TARGET_SQLPLUS_MSF")
echo "table_count $TARGET_SCHEMA_MSF [$table_count]"
if [ "$table_count" -le 500 ]
then
    echo "Refresh failed.. table count for $TARGET_SCHEMA_MSF schema is less than 500"
    clean_up_and_exit      
fi

## post refresh
###############
echo "Correcting missing index"
correct_missing_index "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo  "Compiling schemas $TARGET_SCHEMA_WMS"
compile_objects "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"
echo  "Compiling schemas $TARGET_SCHEMA_MSF"
compile_objects "$TARGET_SQLPLUS_MSF" "$TARGET_SCHEMA_MSF"

echo "Update instance table in $TARGET_SCHEMA_WMS"
update_instance_table "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "Update router table in $TARGET_SCHEMA_WMS"
update_router_table "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "Update listener table in $TARGET_SCHEMA_WMS"
update_listener_table "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "Update jms_listener table in $TARGET_SCHEMA_WMS"
update_jms_listener_table "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "resequencing $TARGET_SCHEMA_WMS"
resequence_it "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "Gathering statistics $TARGET_SCHEMA_WMS"
echo "monitor via - select distinct trunc(last_analyzed),count(*) from dba_tables where owner='$TARGET_SCHEMA_WMS' group by trunc(last_analyzed);"
gather_stats "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"
echo "Gathering statistics $TARGET_SCHEMA_MSF"
echo "monitor via - select distinct trunc(last_analyzed),count(*) from dba_tables where owner='$TARGET_SCHEMA_MSF' group by trunc(last_analyzed);"
gather_stats "$TARGET_SQLPLUS_MSF" "$TARGET_SCHEMA_MSF"

echo "updateURLs in $TARGET_SCHEMA_WMS"
updateURLs "$TARGET_SQLPLUS_WMS" "$TARGET_SCHEMA_WMS"

echo "updateLicense in $TARGET_SCHEMA_MSF"
updateLicense "$TARGET_SQLPLUS_MSF" "$TARGET_SCHEMA_MSF"

echo "create readonly role and do grants [RO_${TARGET_SCHEMA_WMSTOOLS}]"
create_grant_readonly_role "$TARGET_SQLPLUS_SYSTEM" "$TARGET_SCHEMA_MSF"
create_grant_readonly_role "$TARGET_SQLPLUS_SYSTEM" "$TARGET_SCHEMA_WMS"

# removed the trigger pmiller Wed Mar 18 10:59:06 EDT 2015
#echo "drop and re-create triggers"
#do_triggers "$TARGET_SQLPLUS_WMS" 

END_TICK=`date +%s`
TOTAL_TIME=`expr $END_TICK - $START_TICKS_ALL`

add_oracle_node_to_chef

cat<<EOF

ENV : $ENV
TARGET_SCHEMA_WMS : $TARGET_SCHEMA_WMS
TARGET_SCHEMA_MSF : $TARGET_SCHEMA_MSF
DUMPFILE_PATH : $DUMPFILE_PATH
TOTAL_TIME : ${TOTAL_TIME}s
EOF

clean_up_and_exit      
