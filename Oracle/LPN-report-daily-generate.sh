#!/bin/sh

# 
# report reports on WMS report the output to the P-drive
# See INC0040506
#

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/db/oracle/app/oracle/product/11.2.0/client_1/instantclient:/usr/lib/oracle/12.1/client64/lib
if [ -z "$ORACLE_HOME" ]
then
    export ORACLE_HOME=/usr/lib/oracle/12.1/client64
fi

updateJobStatus() {
    start_time=$1
    success_cnt=$2
    failure_cnt=$3

    name=`basename $0`
    path=`dirname $0`
    hostname=`hostname -f`
    frequency="DAILY"

    echo "insert success:$success_cnt failure:$failure_cnt into job_status"
    mysql --host usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com --user inventory --password='See PWS' --database inventory <<EOF
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('$name', '$path', '$hostname', '$frequency', $success_cnt, $failure_cnt, '$start_time', now())
EOF
}

# stuff for oracle
yasql=/usr/local/bin/yasql
mslink=/usr/local/bin/mslink.sh
oracle_user=WMOS_WMS_RO
oracle_password='See PWS'
oracle_host=usevsWMSora101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com
oracle_sid=ORCL
report_name=LPN-report_`date -d '1 day ago' +%F`.csv

# stuff for samba (to mount the P-drive)
Pdrive=DC1VSFILE102A.lsi.local
workgroup=LSI
smb_user=svc_reporting.wms
smb_password='See PWS'

START_TIME=`date +'%F %T'`
date
echo "creating /tmp/$report_name"
# write the report to a file
$yasql $oracle_user/$oracle_password@$oracle_host/$oracle_sid << __EOD__ | sed '/^\s*$/d' > /tmp/$report_name 
Select
  tc_lpn_id as "LPN",
  ITEM_NAME as "Name",
  c_facility_alias_id as "Warehouse",
  (select dsp_locn from WMOS_MSF.LOCN_HDR where locn_id = curr_sub_locn_id) as "Location",
  TC_PARENT_LPN_ID as "PLT",
  BUSINESS_PARTNER_ID as "Seller",
  trunc(created_dttm) as "Receive Date",
  created_source as "Received By",
  trunc(LAST_UPDATED_DTTM) as "Last Touched",
  LAST_UPDATED_SOURCE as "Last Touched By"
from
  WMOS_WMS.LPN
where
  lpn_facility_status in (10,30,45,50) and
  TC_LPN_ID not like 'P%' and
  TC_LPN_ID not like 'p%' and
  TC_LPN_ID not like '0%' and
  TC_LPN_ID not like '%LT%' and
  inbound_outbound_indicator = 'I'\s
__EOD__

cd /tmp

# create MS shortcut
$mslink -l \\\\$Pdrive\\Files\\Public\\WMS-reports\\$report_name -o LPN-report_current.lnk

# write the file and shorcut to the share
smbclient //$Pdrive/files/Public --user=$smb_user --workgroup=$workgroup $smb_password 2> /dev/null << __EOD__
cd WMS-reports
put $report_name
put LPN-report_current.lnk
exit
__EOD__
rm -f /tmp/$report_name
rm -f /tmp/LPN-report_current.lnk



# remove of files from the share
dir_list=$(smbclient //$Pdrive/files/Public --user=$smb_user --workgroup=$workgroup $smb_password 2> /dev/null << __EOD__
cd WMS-reports
ls LPN-report_*.csv
exit
__EOD__
)

dir_list=`echo "$dir_list" | grep 'LPN-report_.*\.csv' | awk '{print $1'}`
del_date=`date -d "60 days ago" +%F`
del_list=''
verified=False
for file in $dir_list
do
    if [[ "$file" == "$report_name" ]]
    then
        verified=True
    fi
    file_date=`echo $file | awk -F_ '{print $2}' | sed 's/\.csv//'`
    if [[ "$file_date" < "$del_date" ]]
    then
        del_list=`echo -e "rm $file\n$del_list"`
    fi
done

success_cnt=1
failure_cnt=0
if [[ "$verified" != "True" ]]
then
    echo "WARN: could not read filename:$report_name from the P-drive"
    success_cnt=0
    failure_cnt=1
fi

echo "removing old reports via [$del_list]"
smbclient //$Pdrive/files/Public --user=$smb_user --workgroup=$workgroup $smb_password 2> /dev/null << __EOD__
cd WMS-reports
$del_list
exit
__EOD__

# if we made it this far assume that the file got written
# to the share correctly, update the job_status db table
updateJobStatus "$START_TIME" $success_cnt $failure_cnt

echo "done"
