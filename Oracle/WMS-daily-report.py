#!/usr/bin/env python

import argparse
import datetime
import logging
import MySQLdb  #  yum install MySQL-python
import os
import socket
import sys
import subprocess
import xlsxwriter

from pprint import pprint

# needed for oracle libs
if 'LD_LIBRARY_PATH' not in os.environ:
    os.environ['LD_LIBRARY_PATH'] = '/usr/lib/oracle/12.1/client64/lib'
    try:
        os.execv(sys.argv[0], sys.argv)
    except Exception, exc:
        print 'Failed re-exec:', exc
        sys.exit(1)

import cx_Oracle

logging.basicConfig(format='%(asctime)s %(funcName)-30s - %(levelname)-8s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger()

def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

parser = argparse.ArgumentParser(
    description="daily oracle reports written to the Pdrive",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.register('type','bool',str2bool)

parser.add_argument(
    "--db_oracle_user",
    help="oracle database user",
    default='WMOS_WMS_RO',
)
parser.add_argument(
    "--db_oracle_password",
    help="oracle database password",
    default='See PWS',
)
parser.add_argument(
    "--db_oracle_host",
    help="oracle database host",
    default="usevsWMSora101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com",
)
parser.add_argument(
    "--db_oracle_port",
    type=int,
    help="oracle database port",
    default=1521    
)
parser.add_argument(
    "--db_oracle_sid",
    help="oracle database sid",
    default='ORCL'
)
parser.add_argument(
    "--max_column_width",
    help="max width in the execl column",
    default=30
)
parser.add_argument(
    "--write_to_share",
    type=str2bool,
    help="Set to true write report to the --smb_Pdrive",
    default=False,
)
parser.add_argument(
    "--smb_Pdrive",
    help="The share host name",
    default='DC1VSFILE102A.lsi.local'
)
parser.add_argument(
    "--smb_workgroup",
    help="The share workgroup",
    default='LSI'
)
parser.add_argument(
    "--smb_user",
    help="The user that writes to the share",
    default='svc_reporting.wms'
)
parser.add_argument(
    "--smb_password",
    help="The user that writes to the share",
    default='See PWS',
)
parser.add_argument(
    "--status_db_user",
    help="mysql database user",
    default='inventory',
)
parser.add_argument(
    "--status_db_password",
    help="database password",
    default='See PWS',
)
parser.add_argument(
    "--status_db_schema",
    help="database schema",
    default='inventory',
)
parser.add_argument(
    "--status_db_host",
    help="database host",
    default='usevsINVdb101a.cgukppq7uxqu.us-east-1.rds.amazonaws.com',
)
parser.add_argument(
    "--status_db_port",
    type=int,
    help="mysql database port",
    default=3306,
)
parser.add_argument(
    "--logLevel",
    nargs='?',
    help="logging level",
    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
    default='INFO'
)

args = parser.parse_args()
log.setLevel(getattr(logging, args.logLevel))

###############
## Job Status stuff
##
def updateJobStatus(start_time, success_cnt, failure_cnt):
    log.info("writting to job_status db table")
    conn  = getMysqlConnection(args.status_db_host, args.status_db_port,
                               args.status_db_user, args.status_db_password,
                               args.status_db_schema)

    sql="""\
INSERT INTO job_status
(name, path, hostname, frequency, success_cnt, failure_cnt, start_time, end_time)
VALUES
('{}', '{}', '{}', '{}', {}, {}, '{}', {})\
""".format(os.path.basename(sys.argv[0]),
           os.path.dirname(sys.argv[0]),
           socket.gethostname(),
           'DAILY',
           success_cnt,
           failure_cnt,
           start_time,
           'now()')
    log.info("sql:[{}]".format(sql))
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()

def getMysqlConnection(host, port, user, password, schema):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} schema:{}".format(host,
                                                                           port,
                                                                           user,
                                                                           schema))
        conn = MySQLdb.connect(host=host, port=port,
                               user=user, passwd=password,
                               db=schema, charset='utf8', use_unicode=True)

    except MySQLdb.Error as e:
        log.critical("ERROR connecting to:{}".format(host))
        log.critical("EXCEPTION:{}".format(e))
        log.exception("Traceback")
        sys.exit(1)
    except:
        log.fatal("ERROR connecting to {}".format(host))
        log.exception("Traceback")
        sys.exit(1)

    return conn
    
###############
## Oracle stuff
##
def OutputTypeHandler(cursor, name, defaultType, size,
                      precision, scale):
    # unicode for all
    if defaultType in (cx_Oracle.STRING, cx_Oracle.FIXED_CHAR): 
        return cursor.var(unicode, size, cursor.arraysize)

def getOracleConnection(host, port, user, password, sid):
    conn = None
    try:
        log.debug("connecting to host:{} port:{} user:{} sid:{}".format(host,
                                                                        port,
                                                                        user,
                                                                        sid))
        connstr = user +'/'+ password +'@'+ host +':'+ str(port) +'/'+ sid
        conn = cx_Oracle.connect(connstr)
        conn.outputtypehandler = OutputTypeHandler
        
    except cx_Oracle.DatabaseError as e:
        log.error("ERROR with Oracle connection on:{}".format(host))
        log.error("EXCEPTION:{}".format(e))
        log.exception("Traceback")
        sys.exit(1)
    except:
        log.fatal("ERROR connecting to {0}".format(host))
        log.exception("Traceback")
        sys.exit(1)

    return conn

    
############
## SQL stuff
##
def doSelect(conn, sql):
    table = []
    try:
        cursor = conn.cursor()
        log.debug("SQL : {0}".format(sql))
        cursor.execute(sql)
        
        columns = cursor.description
        table.append([row[0] for row in columns])
        
        for row in cursor.fetchall():
            table.append(row)
                    
    except:
        log.exception("ERROR selecting from {}".format(conn))

    return table
    
###############
## reports
##
def isfloat(value):
  try:
    float(value)
    return True
  except:
    return False

def get_column_types(report) :
    typeList = [None] * len(report[0])
    x = -1
    for column in zip(*report[1:]) :
        x += 1
        for item in column :
            if (item is None) :
                continue
            t = type(item)
            newType = t            
            if ((t is unicode) or (t is str)) :
                if (item.isdigit()) :
                    newType = int
                elif (isfloat(item)) :
                    newType = float
                                
            if ((newType != int) and (newType != float)) :
                typeList[x] = newType
                log.debug("called break for column {} newType {}".format(x, newType))
                break
            
            if (typeList[x] is None) :
                typeList[x] = newType
            elif (typeList[x] != newType) :
                typeList[x] = float

    # trust but validate
    for row in report[:1] :
        x = -1
        for item in row :
            x += 1
            log.debug("column {}  {} <-> {}".format(x, item, typeList[x]))    
    return typeList


def add_report_2_worksheet(report, worksheet, date_format) :
    typeList = get_column_types(report)
    
    worksheet.freeze_panes(1, 0)
    
    column_width = []
    x = 0
    y = 0
    for row in report[:1] :
        for item in row :
            column_width.append(0)
            width = len(str(item)) + 2.1
            if (width > column_width[y]) :
                column_width[y] = width
            worksheet.write(x, y, item, bold_format)
            y += 1

    x = 1
    y = 0
    for row in report[1:] :
        for item in row :
            if (item is None) :
                worksheet.write(x, y, item)
            else :
                if (type(item) is datetime.datetime) :
                    width = len(str(date_format.num_format))
                    worksheet.write(x, y, item, date_format)
                else :
                    try :
                        if (typeList[y] == int) :
                            item = int(item)
                        elif (typeList[y] == float) :
                            item = float(item)
                    except :
                        log.warn("issue with cast to {} for [{}] column {}".format(typeList[y],
                                                                               item, y))
                
                    worksheet.write(x, y, item)

                    try :
                        if (type(item) is unicode) :
                            width = len(item) + 1
                        else :
                            width = len(str(item)) + 5
                    except :
                        log.warn("issue with getting length of [{}]".format(item))

            if (width > column_width[y]) :
                column_width[y] = width                
            y += 1
        y = 0
        x += 1

    y = 0
    for column in column_width :
        if (column > args.max_column_width) :
            column = args.max_column_width
            log.debug("at max width for column:{}".format(y))
        worksheet.set_column(y, y, column)
        y += 1


def report_stows(conn) :
    log.info("running")
    return doSelect(conn,
"""
SELECT
pt.whse as "Warehouse",
pt.case_nbr as "LPN",
pt.create_date_time as "Date Stowed",
pt.sys_user_id as "Stower",
(SELECT 
   lfs.description FROM WMOS_WMS.LPN_FACILITY_STATUS lfs 
 WHERE 
   lfs.lpn_facility_status = l.lpn_facility_status 
   AND lfs.inbound_outbound_indicator = l.inbound_outbound_indicator) 
as "LPN Status",
(SELECT dsp_locn FROM WMOS_MSF.LOCN_HDR WHERE locn_id = curr_sub_locn_id) 
as "Bin",
(SELECT slot_type FROM WMOS_MSF.LOCN_HDR WHERE locn_id = curr_sub_locn_id) 
as "Curr Bin Slot Type",
(SELECT dsp_locn FROM WMOS_MSF.LOCN_HDR WHERE locn_id = prev_sub_locn_id) 
as "Prev Bin",
(SELECT slot_type FROM WMOS_MSF.LOCN_HDR WHERE locn_id = prev_sub_locn_id)
as "Prev Bin Slot Type",
(SELECT dsp_locn FROM WMOS_MSF.LOCN_HDR WHERE locn_id = pick_sub_locn_id) 
as "Pick Bin",
(SELECT slot_type FROM WMOS_MSF.LOCN_HDR WHERE locn_id = pick_sub_locn_id)
as "Pick Bin Slot Type"
FROM
  WMOS_WMS.PIX_TRAN pt
  LEFT JOIN WMOS_WMS.LPN l ON pt.case_nbr = l.tc_lpn_id 
WHERE
  pt.tran_type = 'C02' 
  AND pt.create_date_time >= trunc(sysdate-1)
  AND pt.create_date_time <  trunc(sysdate)
ORDER BY 
  pt.whse,pt.create_date_time,l.lpn_facility_status
""")

def report_picks(conn) :
    log.info("running")
    return doSelect(conn,
"""
with picks as (select
th.WHSE as "Warehouse",
td.TASK_TYPE as "Task Type",
th.USER_ID as "Picker",
td.CNTR_NBR as "Item",
td.MOD_DATE_TIME as "When",
(select dsp_locn from WMOS_MSF.LOCN_HDR where pull_locn_id = locn_id) as "Location",
lh.slot_type as "Slot Type"
from
WMOS_WMS.TASK_HDR th, WMOS_WMS.TASK_DTL td, WMOS_MSF.LOCN_HDR lh
where
td.TASK_ID = th.TASK_ID and
td.pull_locn_id = lh.locn_id and
td.QTY_PULLD > 0 and
th.MOD_DATE_TIME >= trunc(sysdate-1) and
th.MOD_DATE_TIME <  trunc(sysdate) and
th.invn_need_type = 2 and 
th.USER_ID not in ('svcWMSLasVegas', 'svcWMSIndy', 'svcWMSGarland', 'svcWMSCranbury')
order by th.WHSE, td.TASK_TYPE, th.MOD_DATE_TIME)

select *
From picks
where rowid in 
      (Select max(rowid) 
        from picks 
        group by "Item")
""")

def report_storage_pallets(conn) :
    log.info("running")
    return doSelect(conn,
"""
SELECT
  trunc(l.created_dttm) as "Received Date",
  (select f.facility_name from WMOS_MSF.FACILITY_ALIAS f where f.facility_id = l.c_facility_id) as "Warehouse",
  (select bp.description from WMOS_MSF.BUSINESS_PARTNER bp 
 where bp.business_partner_id = l.business_partner_id)
  as "Seller",
  count(1) as "Storage Pallets"
FROM
  WMOS_WMS.LPN l
WHERE
  l.item_name = 'stage1'
  AND l.created_dttm >= TRUNC(sysdate-1, 'iw')
  AND l.created_dttm < TRUNC(sysdate)
  AND l.lpn_status not in ('0')
GROUP BY 
  trunc(l.created_dttm), l.c_facility_id, l.business_partner_id
ORDER BY 
  l.c_facility_id, trunc(l.created_dttm)
""")

def report_shipped(conn) :
    log.info("running")
    return doSelect(conn,
"""
SELECT
  distinct(wmos_wms.orders.last_updated_source) as "User",
  wmos_wms.orders.o_facility_alias_id as "Warehouse",
  wmos_wms.orders.ref_field_2 as "Task Group",
  trunc(wmos_wms.lpn.LAST_UPDATED_DTTM, 'HH') as "Hour of the Day",
  count(distinct(wmos_wms.lpn.tc_lpn_id)) as "LPNs Shipped"
FROM
  wmos_wms.orders, wmos_wms.lpn
WHERE
  wmos_wms.orders.tc_order_id = wmos_wms.lpn.tc_order_id
  AND wmos_wms.orders.do_status = '190'
  AND wmos_wms.orders.last_updated_dttm >= trunc(sysdate-1)
  AND wmos_wms.orders.last_updated_dttm <  trunc(sysdate)
GROUP BY 
  wmos_wms.orders.last_updated_source,
  wmos_wms.orders.o_facility_alias_id,
  wmos_wms.orders.ref_field_2,
  trunc(wmos_wms.lpn.LAST_UPDATED_DTTM, 'HH')
""")

###############
## main
##
start_time = datetime.datetime.now()

oracleConn = getOracleConnection(args.db_oracle_host, args.db_oracle_port,
                                 args.db_oracle_user, args.db_oracle_password,
                                 args.db_oracle_sid)

filename = 'WMS-report_{:%Y-%m-%d}.xlsx'.format(datetime.datetime.now() -
                                                datetime.timedelta(days = 1))
workbook = xlsxwriter.Workbook('/tmp/' + filename)
date_format = workbook.add_format({'num_format': 'mm/dd/yy'})
date_hour_format = workbook.add_format({'num_format': 'mm/dd/yy HH'})
bold_format = workbook.add_format({'bold': True})

worksheet = workbook.add_worksheet('Stows')
report = report_stows(oracleConn)
#pprint(report)
add_report_2_worksheet(report, worksheet, date_format)

worksheet = workbook.add_worksheet('Picks')
report = report_picks(oracleConn)
add_report_2_worksheet(report, worksheet, date_format)

worksheet = workbook.add_worksheet('Storage Pallets')
report = report_storage_pallets(oracleConn)
add_report_2_worksheet(report, worksheet, date_format)

worksheet = workbook.add_worksheet('Shipped')
report = report_shipped(oracleConn)
add_report_2_worksheet(report, worksheet, date_hour_format)

worksheet = workbook.add_worksheet('Report Info')
report = [
    ['Name', 'Value'],
    ['from host', socket.gethostname()],
    ['from db', args.db_oracle_host],
    ['from script', sys.argv[0]],
    ['run at', datetime.datetime.now()],
]
add_report_2_worksheet(report, worksheet, date_format)

workbook.close()
if (not args.write_to_share) :
    log.warn("--write_to_share not set, not writting to share")
    log.info("spread sheet written to /tmp/{}".format(filename))
    sys.exit()

#############
## write report to the Pdrive
##
os.chdir('/tmp')

# create MS shortcut
subprocess.call(['/usr/local/bin/mslink.sh',
                 '-l',
                 '\\{}\Files\Public\WMS-reports\pick_stow\{}'.format(args.smb_Pdrive,
                                                                     filename),
                 '-o',
                 'WMS-report_current.lnk'],
                shell=False)

log.info("calling smbclient for put and dir list")
process = subprocess.Popen(['smbclient',
                            '//' + args.smb_Pdrive + '/Files/Public',
                            '--user',     args.smb_user,
                            '--workgroup', args.smb_workgroup,
                            args.smb_password],
                           shell=False,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
process.stdin.write('cd WMS-reports\pick_stow\n')
process.stdin.write('put {}\n'.format(filename))
process.stdin.write('put {}\n'.format('WMS-report_current.lnk'))
(stdout,stderr) = process.communicate('ls WMS-report_*.xlsx')
os.remove('/tmp/' + filename)
os.remove('/tmp/WMS-report_current.lnk')

dir_list=[]
del_list=[]
for line in stdout.split('\n') :
    line = line.lstrip()
    if (line.startswith('WMS-report_')) :
        f = line.split()[0]
        if (filename.endswith('.xlsx')) :
            dir_list.append(f)
            
del_date = '{:%Y-%m-%d}'.format(datetime.datetime.now() +
                                datetime.timedelta(-60))
verified=False
for f in dir_list :
    if (f == filename) :
        verified=True
    file_date = f.split('_')[1][:-5]
    if (file_date < del_date) :
        del_list.append(f)

success_cnt=1
failure_cnt=0
if (not verified) :
    log.warn("could not read filename:{} from the P-drive".format(filename))
    success_cnt=0
    failure_cnt=1

# if we made it this far assume that the file got written
# to the share correctly, update the job_status db table
updateJobStatus(start_time, success_cnt, failure_cnt)

if (not del_list) :
    log.info("no files older then {}... exiting".format(del_date))
    sys.exit(0)
    
log.info("removing old reports:{}".format(del_list))
log.info("calling smbclient to remove old reports")
process = subprocess.Popen(['smbclient',
                            '//' + args.smb_Pdrive + '/files/Public',
                            '--user',     args.smb_user,
                            '--workgroup', args.smb_workgroup,
                            args.smb_password],
                           shell=False,
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
process.stdin.write('cd WMS-reports\pick_stow\n')
for filename in del_list :
    log.info("removing old report:{}".format(filename))
    process.stdin.write('rm {}\n'.format(filename))

(stdout,stderr) = process.communicate() # this Waits for process to terminate 
