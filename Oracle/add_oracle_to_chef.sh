#!/bin/sh

schema=$1
platform=$2
env=$3
dumpfile=$4
hostname=$5
chef_path=$6

usage()
{
    echo
    echo "Usage: $0 schema platform env dumpfile [hostname] [chef_path]"
    echo "  pushes oracle refresh information to chef"
    echo "  examples:"
    echo "    QA1 (TEST1/DEV01)"
    echo "      $0 WMOS_DEV01_WMS-MSF WMS-MSF  qa1 WMOS_PROD_MSF_WMOS_PROD_WMS_02112016_0100.dmp"
    echo "      $0 WMSTDEV01          WMSTOOLS qa1 PROD_WMSTOOLS_PROD_02112016_0100.dmp"
    echo "    QA2 (TEST10/DEV02)"
    echo "      $0 WMOS_DEV02_WMS-MSF WMS-MSF  qa2 WMOS_PROD_MSF_WMOS_PROD_WMS_02152016_0100.dmp"
    echo "      $0 WMSTOOLSQA1        WMSTOOLS qa2 PROD_WMSTOOLS_PROD_02152016_0100.dmp"
    echo "    QA3 (OOC1/DEV00)"
    echo "      $0 WMUSER2-WMUSER     WMS-MSF  qa3 WMOS_PROD_MSF_WMOS_PROD_WMS_MMDDYYYY_HHmm.dmp"
    echo "      $0 WMSTOOLS           WMSTOOLS qa3 PROD_WMSTOOLS_PROD_MMDDYYYY_HHmm.dmp"
    echo "    PREPROD (STAGE)"
    echo "      $0 WMOS_STAGE_WMS-MSF WMS-MSF  preprod WMOS_PROD_MSF_WMOS_PROD_WMS_MMDDYYYY_HHmm.dmp"
    echo "      $0 WMSTOOLS_STAGE     WMSTOOLS preprod PROD_WMSTOOLS_PROD_MMDDYYYY_HHmm.dmp"
    exit 1
}

if [ "$schema" = "" ]
then
    echo "must give a schema"
    echo ""
    usage
fi

if [ "$platform" = "" ]
then
    echo "must give a platform"
    echo ""
    usage
fi
if [ "$env" = "" ]
then
    echo "must give an env"
    echo ""
    usage
fi
if [ "$dumpfile" = "" ]
then
    echo "must give a dumpfile"
    echo ""
    usage
fi
if [ "$hostname" = "" ]
then
    hostname=`hostname -f`
    # expected to be oracle-test[12]
fi
if [ "$chef_path" = "" ]
then
    chef_path=$HOME/.chef
fi


chef_env=`echo $env | sed  's/[[:digit:]]//'`
chef_config=${chef_path}/knife-${chef_env}-admin.rb

if [ ! -e $chef_config ]
then
    echo "file [$chef_config] does not exist"
    echo "pattern is ${chef_path}/knife-\${chef_e}-admin.rb"
    exit 1
fi

# the oracle dbs are not AWS RDS
# so fake it to look like the output of...
#    DB=`aws rds describe-db-instances --output=json --db-instance-identifier $db_instance 2>&1`
date=`date +'%F %T'`
read -r -d '' DB <<EOD
"DBInstances": {
     "hostname":     "$hostname",
     "uploadedDate": "$date",
     "schema":       "$schema",
     "platform":     "$platform",
     "env":          "$env",
     "dumpfile":     "$dumpfile"
}
EOD

# make the oracle dumpfile match the rds snaphotname
# dumpfile = WMOS_PROD_MSF_WMOS_PROD_WMS_02152016_0100.dmp
rdsSnapShotFormat=`echo $dumpfile | perl -ne '@A = (m/(.+)_(\d{2})(\d{2})(\d{4})_(\d{2})(\d{2})\.dmp$/); print "oracle:$A[0]-$A[3]-$A[1]-$A[2]-$A[4]-$A[5]"'`

# the oracle db's are not AWS RDS
# so fake it to look like the output of...
#    TAGS=`aws rds list-tags-for-resource --output json --resource-name $ARN | sed 's/^{$//;s/^}$//'`
read -r -d '' TAGS <<EOD
"TagList": [
    {
        "Value": "$platform",
        "Key": "platform"
    },
    {
        "Value": "$env",
        "Key": "environment"
    },
    {
        "Value": "$rdsSnapShotFormat",
        "Key": "created from snapshot"
    }
]
EOD


(
cat <<__EOD__
{
  "name": "$schema",
  "chef_environment": "$env",
  "run_list": [],
  "normal": {
    "tags": [ "RDS" ]
  },
  "automatic": {
     $DB,
     $TAGS
   }
}
__EOD__
) > /tmp/$$.json

knife node from file /tmp/$$.json --config $chef_config
#echo "/tmp/$$.json"
rm /tmp/$$.json
