#!/bin/sh

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/db/oracle/app/oracle/product/11.2.0/client_1/instantclient:/usr/lib/oracle/12.1/client64/lib
if [ -z "$ORACLE_HOME" ]
then
    export ORACLE_HOME=/usr/lib/oracle/12.1/client64
fi

ARG_START_TIME=1/24
ARG_END_TIME=sysdate
DRY_RUN=FALSE
HOST=usevswmsora101a
REPORT_DIR=${HOME}/public_html

usage()
{
    echo
    echo "Usage: $0 [--start_time] [--end_time] [--dry_run] [--help]"
    echo "  runs oracle AWR and ASH report"
    echo "  note that oracle snapshots are based on system date (sysdate)"
    echo "  and not on the time zone of the current SQL session date (current_date)"
    echo
    echo "  --instance   : dbInstance to run the reports against"
    echo "                 defaults to [$HOST]"
    echo "  --start_time : either an oracle offset from --end_time ('1/24' where 1/24 == 1 hour)"
    echo "                 or a date string ('YYY-MM-DD HH:MM:SS').  See UTC time below"
    echo "                 default to [$ARG_START_TIME]."
    echo "  --end_time   : either an oracle date var (sysdate)"
    echo "                 or a date string ('YYY-MM-DD HH:MM:SS').  See UTC time below"
    echo "  --report_dir : where the reports get written"
    echo "                 default to [$REPORT_DIR]"
    echo "  --dry_run    : do not run the reports"
    echo "  --help       : this page"
    echo
    echo "  UTC time"
    echo "    Note that AWS RDS oracle instances are on UTC time."
    echo "    IE the AWR snapshots (DBA_HIST_SNAPSHOT.END_INTERVAL_TIME) are in UTC."
    echo "    Either use offset/sysdate (works regardless of timezone)"
    echo "      --start_time 1/24       (1 hours back from --end_time)"
    echo "      --end_time sysdate-3/24 (3 hours back from sysdate)"
    echo "    Or use UTC times (only RDS instances are in UTC)"
    echo "      --start_time \"\`date --utc --date '4 hours ago' +'%F %T'\`\""
    echo "      --end_time \"\`date --utc --date '3 hours ago' +'%F %T'\`\""
    echo "    These result in the same time period."
    echo
    echo "  one can convert Eastern time to UTC via"
    echo "    date --date='TZ=\"America/New_York\" 2016-07-12 12:00:00' --utc +'%F %T'"
    echo "    2016-07-12 16:00:00"
    exit
}

for arg in "$@"; do
  shift
  case "$arg" in
    "--instance")   set -- "$@" "-i" ;;
    "--start_time") set -- "$@" "-s" ;;
    "--end_time")   set -- "$@" "-e" ;;
    "--report_dir") set -- "$@" "-r" ;;
    "--dry_run")    set -- "$@" "-d" ;;
    "--help")       set -- "$@" "-h" ;;
    *)        set -- "$@" "$arg"
  esac
done


OPTIND=1
while getopts ":i:s:e:r:d:h" opt
do
  case "$opt" in
    "h") usage;;
    "i") HOST=$OPTARG ;;
    "s") ARG_START_TIME=$OPTARG ;;
    "e") ARG_END_TIME=$OPTARG ;;
    "r") REPORT_DIR=$OPTARG ;;
    "d") DRY_RUN=$OPTARG ;;
    "?") echo "unknown option"
         usage;;
  esac
done
shift $(expr $OPTIND - 1)

DRY_RUN=`echo $DRY_RUN | tr '[a-z]' '[A-Z]'`

echo $ARG_START_TIME | grep -q '/'
if [ $? -eq 1 ] ; then
    # if it does not contain a / assume it a date and quote it
    ARG_START_TIME=\'$ARG_START_TIME\'
fi

echo $ARG_END_TIME | grep -q ^[0-9]
if [ $? -eq 0 ] ; then
    # if start with number assume it is a date and quote it
    ARG_END_TIME=\'$ARG_END_TIME\'
fi


if [ "$HOST" == "usevswmsora101a" ]
then
    USER=dba_oracle
    PASSWORD="look up in pmp"
    SID=ORCL
elif [ "$HOST" == "usevswmtora101a" ]
then
    USER=dba_oracle
    PASSWORD="look up in pmp"
    SID=ORCL
elif [ "$HOST" == "lsiora01.ash.liquidation.com" ]
then
    USER=SYSTEM
    PASSWORD="look up in pmp"
    SID=PLSORA01
elif [ "$HOST" == "manhora01.ash.liquidation.com" ]
then
    USER=SYSTEM
    PASSWORD="look up in pmp"
    SID=PMHORA01
else
    echo "Error: unknown HOST [$HOST]"
    usage
fi
    

echo "argument START_TIME : $ARG_START_TIME"
echo "argument END_TIME : $ARG_END_TIME"
echo "argument DRY_RUN : $DRY_RUN"
echo "argument INSTANCE : $HOST"
echo

if [[ ( "$DRY_RUN" != "TRUE" ) && ( "$DRY_RUN" != "FALSE" ) ]]
then
    echo "DRY_RUN option must be TRUE or FALSE"
    usage
fi
    

SQLPLUS="sqlplus -S $USER/$PASSWORD@$HOST/$SID"

ORACLE_HEADER="
set heading off
set feedback off
set pages 0
ALTER SESSION SET NLS_TIMESTAMP_FORMAT='YYYY-MM-DD HH24:MI:SS.FF';
ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS';
"

get_max_END_INTERVAL_TIME()
{
    sqlplus=$1
    end_time=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select max(END_INTERVAL_TIME) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME<=$end_time;
EOF
}

get_DBID()
{
    sqlplus=$1
    end_interval_time=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select trim(DBID) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME='$end_interval_time';
EOF
}

get_INSTANCE_NUMBER()
{
    sqlplus=$1
    end_interval_time=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select trim(INSTANCE_NUMBER) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME='$end_interval_time';
EOF
}

get_END_SNAP()
{
    sqlplus=$1
    end_interval_time=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select trim(SNAP_ID) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME='$end_interval_time';
EOF
}

get_BEGIN_SNAP_OFFSET()
{
    sqlplus=$1
    end_interval_time=$2
    offset=$3
    # limit? fetch? nope...  subquery
    $SQLPLUS <<EOF
$ORACLE_HEADER
select * from (select trim(SNAP_ID) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME < TO_TIMESTAMP('$end_interval_time', 'YYYY-MM-DD HH24:MI:SS.FF') - ${offset} order by END_INTERVAL_TIME desc) where ROWNUM <= 1;
EOF
}

get_BEGIN_SNAP_DATE()
{
    sqlplus=$1
    date=$2
    # limit? fetch? nope...  subquery
    $SQLPLUS <<EOF
$ORACLE_HEADER
select * from (select trim(SNAP_ID) from DBA_HIST_SNAPSHOT where END_INTERVAL_TIME < ${date} order by END_INTERVAL_TIME desc) where ROWNUM <= 1;
EOF
}

get_SNAP_TIME()
{
    sqlplus=$1
    snap_id=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select END_INTERVAL_TIME from DBA_HIST_SNAPSHOT where SNAP_ID=$snap_id;
EOF
}

convert_date()
{
    sqlplus=$1
    date=$2
    $sqlplus <<EOF
$ORACLE_HEADER
select to_char(to_timestamp('$date', 'YYYY-MM-DD HH24:MI:SS.FF'), 'MM/DD/YY HH24:MI:SS') from dual;
EOF
}

run_AWR()
{
    sqlplus=$1
    instance_number=$2
    dbid=$3
    num_days=$4
    begin_snap=$5
    end_snap=$6
    report_type=$7
    report_name=$8

    echo "running AWR report (/rdbms/admin/awrrpti) with define's"
    echo "define inst_num    =  $instance_number;"
    echo "define dbid        =  $dbid;"
    echo "define num_days    =  $num_days;"
    echo "define begin_snap  =  $begin_snap;"
    echo "define end_snap    =  $end_snap;"
    echo "define report_type =  $report_type;"
    echo "define report_name =  $report_name;"

    if [ "$DRY_RUN" == "TRUE" ]
    then
        echo "DRY_RUN option encounter not running report"
        return
    fi
    
    $SQLPLUS <<EOF > /dev/null
$ORACLE_HEADER
define inst_num    =  $instance_number;
define dbid        =  $dbid;
define num_days    =  $num_days;
define begin_snap  =  $begin_snap;
define end_snap    =  $end_snap;
define report_type =  $report_type;
define report_name =  $report_name;

@?/rdbms/admin/awrrpti 
EOF
}


run_ASH()
{
    sqlplus=$1
    instance_number=$2
    dbid=$3
    duration=$4
    begin_time=$5
    end_time=$6
    report_type=$7
    report_name=$8

    echo "running ASH report (/rdbms/admin/ashrpt) with define's"
    echo "define inst_num    =  $instance_number;"
    echo "define dbid        =  $dbid;"
    echo "define duration    =  '$duration';"
    echo "define begin_time  =  $begin_time;"
    echo "define end_time    =  $end_time;"
    echo "define report_type =  $report_type;"
    echo "define report_name =  $report_name;"
    
    if [ "$DRY_RUN" == "TRUE" ]
    then
        echo "DRY_RUN option encounter not running report"
        return
    fi

    $SQLPLUS <<EOF > /dev/null
$ORACLE_HEADER
define inst_num    =  $instance_number;
define dbid        =  $dbid;
define duration    =  '$duration';
define begin_time  =  '$begin_time';
define end_time    =  '$end_time';
define report_type =  $report_type;
define report_name =  $report_name;

@?/rdbms/admin/ashrpt 
EOF
}

SHORT_HOST=`echo $HOST | sed  's/\..*//'`
ZIP_LIST=''

max_END_INTERVAL_TIME=$(get_max_END_INTERVAL_TIME "$SQLPLUS" "$ARG_END_TIME")
DBID=$(get_DBID "$SQLPLUS" "$max_END_INTERVAL_TIME")
INSTANCE_NUMBER=$(get_INSTANCE_NUMBER "$SQLPLUS" "$max_END_INTERVAL_TIME")
END_SNAP=$(get_END_SNAP "$SQLPLUS" "$max_END_INTERVAL_TIME")

echo $ARG_START_TIME | grep -q '/'
if [ $? -eq 0 ] ; then
    # if contains a '/' assume its an offset (1/24)
    BEGIN_SNAP=$(get_BEGIN_SNAP_OFFSET "$SQLPLUS" "$max_END_INTERVAL_TIME" "$ARG_START_TIME")
else
    BEGIN_SNAP=$(get_BEGIN_SNAP_DATE "$SQLPLUS" "$ARG_START_TIME")
fi


BEGIN_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$BEGIN_SNAP")
END_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$END_SNAP")
echo "AWR report loop from '$BEGIN_SNAP_TIME'($BEGIN_SNAP) '$END_SNAP_TIME'($END_SNAP)"

for SNAP_E in $(seq $BEGIN_SNAP $END_SNAP)
do
    SNAP_B=$SNAP_E
    let SNAP_B--
    
    BEGIN_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$SNAP_B")
    END_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$SNAP_E")

    START_TIME=`echo $BEGIN_SNAP_TIME | tr " " - | sed  's/\..*//'`
    END_TIME=`echo $END_SNAP_TIME | tr " " - | sed  's/\..*//'`
    REPORT_NAME=${REPORT_DIR}/awr_report_${SHORT_HOST}_${START_TIME}_${END_TIME}.html

    echo "===== AWR report ====="
    run_AWR "$SQLPLUS" "$INSTANCE_NUMBER" "$DBID" "0" "$SNAP_B" "$SNAP_E" "html" "$REPORT_NAME"
    ZIP_LIST="${ZIP_LIST} $REPORT_NAME"


    BEGIN_TIME_CON=$(convert_date "$SQLPLUS" "$BEGIN_SNAP_TIME")
    END_TIME_CON=$(convert_date "$SQLPLUS" "$END_SNAP_TIME")
    REPORT_NAME=${REPORT_DIR}/ash_report_${SHORT_HOST}_${START_TIME}_${END_TIME}.html

    echo "===== ASH report ====="
    run_ASH "$SQLPLUS" "$INSTANCE_NUMBER" "$DBID" "" "$BEGIN_TIME_CON" "$END_TIME_CON" "html" "$REPORT_NAME"
    ZIP_LIST="${ZIP_LIST} $REPORT_NAME"
done


##################################
echo "===== zip the reports ====="

BEGIN_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$BEGIN_SNAP")
END_SNAP_TIME=$(get_SNAP_TIME "$SQLPLUS" "$END_SNAP")

START_TIME=`echo $BEGIN_SNAP_TIME | tr " " - | sed  's/\..*//'`
END_TIME=`echo $END_SNAP_TIME | tr " " - | sed  's/\..*//'`

ZIP_NAME=${REPORT_DIR}/awr-ash_report_${SHORT_HOST}_${START_TIME}_${END_TIME}.zip

if [ "$DRY_RUN" != "TRUE" ]
then
    zip -r --junk-paths ${ZIP_NAME} $ZIP_LIST
else 
    echo "DRY_RUN option encounter no doing zip"
    echo "zip -r --junk-paths ${ZIP_NAME} $ZIP_LIST"
fi
